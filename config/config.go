package config

import (
	"database/sql"
	"fmt"
	"net/url"
	"os"
	"strconv"
)

const (
	defaultAccountRemoveDays       int    = 30
	defaultApplicationName         string = "mashighlight"
	defaultDBHost                  string = "localhost"
	defaultDBScheme                string = "postgres"
	defaultDomain                  string = "localhost"
	defaultHighlightScoreThreshold int32  = 6
	defaultMastodonHost            string = "localhost"
	defaultPort                    uint16 = 3000
	defaultStatusRemoveDays        int    = 7
)

type Config struct {
	Domain                  string
	Port                    uint16
	MastodonHost            string
	DatabaseURL             url.URL
	ApplicationName         string
	HighlightScoreThreshold int32
	StatusRemoveDays        int
	AccountRemoveDays       int
}

func contains(arr []string, str string) bool {
	for _, v := range arr {
		if v == str {
			return true
		}
	}
	return false
}

func New() (*Config, error) {
	c := &Config{}

	if domain, ok := os.LookupEnv("DOMAIN"); ok {
		c.Domain = domain
	} else {
		c.Domain = defaultDomain
	}

	if portStr, ok := os.LookupEnv("PORT"); ok {
		port64, err := strconv.ParseUint(portStr, 10, 16)
		if err != nil {
			return nil, err
		}
		c.Port = uint16(port64)
	} else {
		c.Port = defaultPort
	}

	if mastodonHost, ok := os.LookupEnv("MASTODON_HOST"); ok {
		if _, err := url.ParseRequestURI("https://" + mastodonHost + "/"); err != nil {
			return nil, err
		}
		c.MastodonHost = mastodonHost
	} else {
		c.MastodonHost = defaultMastodonHost
	}

	if databaseURL, ok := os.LookupEnv("DATABASE_URL"); ok {
		dbURL, err := url.ParseRequestURI(databaseURL)
		if err != nil {
			return nil, err
		} else if !contains(sql.Drivers(), dbURL.Scheme) {
			return nil, fmt.Errorf("config: unknown database driver %q", dbURL.Scheme)
		}
		c.DatabaseURL = *dbURL
	} else {
		var dbURL url.URL

		if dbScheme, ok := os.LookupEnv("DB"); ok {
			dbURL.Scheme = dbScheme
		} else {
			dbURL.Scheme = defaultDBScheme
		}

		if dbUsername, ok := os.LookupEnv("DBUSER"); ok {
			if dbPassword, ok := os.LookupEnv("DBPASSWORD"); ok {
				dbURL.User = url.UserPassword(dbUsername, dbPassword)
			} else {
				dbURL.User = url.User(dbUsername)
			}
		}

		var dbHost string
		if v, ok := os.LookupEnv("DBHOST"); ok {
			dbHost = v
		} else {
			dbHost = defaultDBHost
		}

		if dbPortStr, ok := os.LookupEnv("DBPORT"); ok {
			_, err := strconv.ParseUint(dbPortStr, 10, 16)
			if err != nil {
				return nil, err
			}
			dbHost += ":" + dbPortStr
		}
		dbURL.Host = dbHost

		var dbName string
		if v, ok := os.LookupEnv("DBNAME"); ok {
			dbName = v
		} else {
			dbName = ""
		}
		dbURL.Path = "/" + dbName

		c.DatabaseURL = dbURL
	}

	if applicationName, ok := os.LookupEnv("APPLICATION_NAME"); ok {
		c.ApplicationName = applicationName
	} else {
		c.ApplicationName = defaultApplicationName
	}

	if highlightScoreThresholdStr, ok := os.LookupEnv("HIGHLIGHT_SCORE_THRESHOLD"); ok {
		highlightScoreThreshold, err := strconv.ParseInt(highlightScoreThresholdStr, 10, 32)
		if err != nil {
			return nil, err
		}
		c.HighlightScoreThreshold = int32(highlightScoreThreshold)
	} else {
		c.HighlightScoreThreshold = defaultHighlightScoreThreshold
	}

	if statusRemoveDaysStr, ok := os.LookupEnv("STATUS_REMOVE_DAYS"); ok {
		statusRemoveDays, err := strconv.Atoi(statusRemoveDaysStr)
		if err != nil {
			return nil, err
		}
		c.StatusRemoveDays = statusRemoveDays
	} else {
		c.StatusRemoveDays = defaultStatusRemoveDays
	}

	if accountRemoveDaysStr, ok := os.LookupEnv("ACCOUNT_REMOVE_DAYS"); ok {
		accountRemoveDays, err := strconv.Atoi(accountRemoveDaysStr)
		if err != nil {
			return nil, err
		}
		c.AccountRemoveDays = accountRemoveDays
	} else {
		c.AccountRemoveDays = defaultAccountRemoveDays
	}

	return c, nil
}
