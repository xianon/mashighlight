package presenters

import (
	"context"
	"log"

	"gitlab.com/xianon/mashighlight/interface/keys"
	"gitlab.com/xianon/mashighlight/interface/views"
	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type Authorized struct {
	view *views.Authorized
}

func NewAuthorized(view *views.Authorized) *Authorized {
	return &Authorized{
		view: view,
	}
}

func (a *Authorized) RespondIndex(ctx context.Context, response *responses.AuthorizedIndex) {
	w, ok := keys.GetResponseWriter(ctx)
	if !ok {
		return
	}

	err := a.view.Execute(w, response)
	if err == nil {
		return
	}

	respondError(w, err)

	log.Printf("Authorized Index Error: %s\n", err.Error())
}

func (*Authorized) RespondError(ctx context.Context, response *responses.AuthorizedError) {
	if w, ok := keys.GetResponseWriter(ctx); ok {
		respondError(w, response.Error)
	}

	log.Printf("Authorized Error: %s\n", response.Error.Error())
}
