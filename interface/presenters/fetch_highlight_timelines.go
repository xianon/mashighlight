package presenters

import (
	"context"
	"log"

	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type FetchHighlightTimelines struct{}

func NewFetchHighlightTimelines() *FetchHighlightTimelines {
	return &FetchHighlightTimelines{}
}

func (*FetchHighlightTimelines) Respond(ctx context.Context, response *responses.FetchHighlightTimelines) {
	log.Println("Succeeded fetching highlight timeline")
}

func (*FetchHighlightTimelines) RespondError(ctx context.Context, response *responses.FetchHighlightTimelinesError) {
	log.Printf("Failed fetching highlight timeline: %v\n", response.Error)
}
