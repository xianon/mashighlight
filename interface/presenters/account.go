package presenters

import (
	"context"
	"log"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/xianon/mashighlight/interface/keys"
	"gitlab.com/xianon/mashighlight/usecase/dto"
	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type Account struct{}

func NewAccount() *Account {
	return &Account{}
}

func convertMAccount(oAccount *dto.OAccount) map[string]interface{} {
	mAccount := map[string]interface{}{
		"id":           strconv.FormatInt(oAccount.ID, 10),
		"username":     oAccount.UserName,
		"acct":         oAccount.Acct,
		"display_name": oAccount.DisplayName,
		"created_at":   oAccount.CreatedAt.Format(time.RFC3339Nano),
	}
	for k, v := range oAccount.Other {
		mAccount[k] = v
	}
	return mAccount
}

func (a *Account) RespondRead(ctx context.Context, response *responses.AccountRead) {
	w, ok := keys.GetResponseWriter(ctx)
	if !ok {
		return
	}

	mAccount := convertMAccount(response.OAccount)

	if err := respondJSON(w, http.StatusOK, mAccount); err != nil {
		respondError(w, err)
	}
}

func (*Account) RespondError(ctx context.Context, response *responses.AccountError) {
	if w, ok := keys.GetResponseWriter(ctx); ok {
		respondError(w, response.Error)
	}

	log.Printf("Account Error: %s\n", response.Error.Error())
}
