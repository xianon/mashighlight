package presenters

import (
	"encoding/json"
	"net/http"

	"gitlab.com/xianon/mashighlight/usecase/description_errors"
)

type errorResponse struct {
	Error            string  `json:"error"`
	ErrorDescription *string `json:"error_description,omitempty"`
}

func newErrorResponse(err error) *errorResponse {
	var errorDescription *string
	if errDsc, ok := err.(description_errors.ErrorWithDescription); ok {
		errorDescription = errDsc.ErrorDescription()
	}
	return &errorResponse{
		Error:            err.Error(),
		ErrorDescription: errorDescription,
	}
}

func respondJSON(w http.ResponseWriter, statusCode int, v interface{}) error {
	body, err := json.Marshal(v)
	if err != nil {
		return err
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(statusCode)
	w.Write(body)
	return nil
}

func respondError(w http.ResponseWriter, err error) {
	var statusCode int
	if description_errors.IsBadRequest(err) {
		statusCode = http.StatusBadRequest
	} else if description_errors.IsUnauthenticated(err) {
		statusCode = http.StatusUnauthorized
	} else if description_errors.IsNotFound(err) {
		statusCode = http.StatusNotFound
	} else {
		statusCode = http.StatusInternalServerError
	}
	errResp := newErrorResponse(err)
	respondJSON(w, statusCode, errResp)
}
