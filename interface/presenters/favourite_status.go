package presenters

import (
	"context"
	"log"
	"net/http"

	"gitlab.com/xianon/mashighlight/interface/keys"
	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type FavouriteStatus struct{}

func NewFavouriteStatus() *FavouriteStatus {
	return &FavouriteStatus{}
}

func (f *FavouriteStatus) RespondCreate(ctx context.Context, response *responses.FavouriteStatusCreate) {
	w, ok := keys.GetResponseWriter(ctx)
	if !ok {
		return
	}

	mStatus := convertMStatus(response.Status)

	if err := respondJSON(w, http.StatusOK, mStatus); err != nil {
		respondError(w, err)
	}
}

func (*FavouriteStatus) RespondError(ctx context.Context, response *responses.FavouriteStatusError) {
	if w, ok := keys.GetResponseWriter(ctx); ok {
		respondError(w, response.Error)
	}

	log.Printf("FavouriteStatus Error: %s\n", response.Error.Error())
}
