package presenters

import (
	"context"
	"log"
	"net/http"

	"gitlab.com/xianon/mashighlight/interface/keys"
	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type UnfavouriteStatus struct{}

func NewUnfavouriteStatus() *UnfavouriteStatus {
	return &UnfavouriteStatus{}
}

func (u *UnfavouriteStatus) RespondCreate(ctx context.Context, response *responses.UnfavouriteStatusCreate) {
	w, ok := keys.GetResponseWriter(ctx)
	if !ok {
		return
	}

	mStatus := convertMStatus(response.Status)

	if err := respondJSON(w, http.StatusOK, mStatus); err != nil {
		respondError(w, err)
	}
}

func (*UnfavouriteStatus) RespondError(ctx context.Context, response *responses.UnfavouriteStatusError) {
	if w, ok := keys.GetResponseWriter(ctx); ok {
		respondError(w, response.Error)
	}

	log.Printf("UnfavouriteStatus Error: %s\n", response.Error.Error())
}
