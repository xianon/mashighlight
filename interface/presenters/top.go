package presenters

import (
	"context"
	"log"

	"gitlab.com/xianon/mashighlight/interface/keys"
	"gitlab.com/xianon/mashighlight/interface/views"
	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type Top struct {
	view *views.Top
}

func NewTop(view *views.Top) *Top {
	return &Top{
		view: view,
	}
}

func (t *Top) RespondIndex(ctx context.Context, response *responses.TopIndex) {
	w, ok := keys.GetResponseWriter(ctx)
	if !ok {
		return
	}

	err := t.view.Execute(w, response)
	if err == nil {
		return
	}

	respondError(w, err)

	log.Printf("Top Index Error: %s\n", err.Error())
}

func (*Top) RespondError(ctx context.Context, response *responses.TopError) {
	if w, ok := keys.GetResponseWriter(ctx); ok {
		respondError(w, response.Error)
	}

	log.Printf("Top Error: %s\n", response.Error.Error())
}
