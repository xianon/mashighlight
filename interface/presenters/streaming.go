package presenters

import (
	"context"
	"errors"
	"log"

	"gitlab.com/xianon/mashighlight/interface/keys"
	"gitlab.com/xianon/mashighlight/usecase/output_boundaries"
	"gitlab.com/xianon/mashighlight/usecase/responses"
	"nhooyr.io/websocket"
	"nhooyr.io/websocket/wsjson"
)

type Streaming struct {
	opts *websocket.AcceptOptions
}

func NewStreaming() *Streaming {
	// MEMO: "file://" の origin に対応するため空のホスト名を許可する
	opts := &websocket.AcceptOptions{
		OriginPatterns:  []string{""},
		CompressionMode: websocket.CompressionDisabled,
	}
	return &Streaming{
		opts: opts,
	}
}

func (s *Streaming) Streaming(ctx context.Context, response *responses.Streaming) (output_boundaries.StreamingCloser, error) {
	r, ok := keys.GetRequest(ctx)
	if !ok {
		return nil, errors.New("TODO: Stream GetRequest Error")
	}
	w, ok := keys.GetResponseWriter(ctx)
	if !ok {
		return nil, errors.New("TODO: Stream GetResponseWriter Error")
	}

	c, err := websocket.Accept(w, r, s.opts)
	if err != nil {
		return nil, err
	}

	streamingCloser := newStreamingCloser(c)
	return streamingCloser, nil
}

func (s *Streaming) RespondError(ctx context.Context, response *responses.StreamingError) {
	if w, ok := keys.GetResponseWriter(ctx); ok {
		respondError(w, response.Error)
	}

	log.Printf("Streaming Error: %s\n", response.Error.Error())
}

type streamingCloser struct {
	c *websocket.Conn
}

func newStreamingCloser(c *websocket.Conn) *streamingCloser {
	return &streamingCloser{
		c: c,
	}
}

func (s *streamingCloser) Close() error {
	return s.c.Close(websocket.StatusNormalClosure, "")
}

func (s *streamingCloser) ReadJSON(ctx context.Context, v interface{}) error {
	return wsjson.Read(ctx, s.c, v)
}

func (s *streamingCloser) WriteJSON(ctx context.Context, v interface{}) error {
	return wsjson.Write(ctx, s.c, v)
}
