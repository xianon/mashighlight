package presenters

import (
	"context"
	"log"
	"net/http"

	"gitlab.com/xianon/mashighlight/interface/keys"
	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type Register struct{}

func NewRegister() *Register {
	return &Register{}
}

func (*Register) RespondIndex(ctx context.Context, response *responses.RegisterIndex) {
	r, ok := keys.GetRequest(ctx)
	if !ok {
		return
	}
	w, ok := keys.GetResponseWriter(ctx)
	if !ok {
		return
	}

	http.Redirect(w, r, response.URL, http.StatusFound)
}

func (*Register) RespondError(ctx context.Context, response *responses.RegisterError) {
	if w, ok := keys.GetResponseWriter(ctx); ok {
		respondError(w, response.Error)
	}

	log.Printf("Register Error: %s\n", response.Error.Error())
}
