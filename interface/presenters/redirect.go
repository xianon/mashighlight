package presenters

import (
	"context"
	"net/http"

	"gitlab.com/xianon/mashighlight/interface/keys"
	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type Redirect struct{}

func NewRedirect() *Redirect {
	return &Redirect{}
}

func (*Redirect) RespondRead(ctx context.Context, response *responses.RedirectRead) {
	r, ok := keys.GetRequest(ctx)
	if !ok {
		return
	}
	w, ok := keys.GetResponseWriter(ctx)
	if !ok {
		return
	}

	http.Redirect(w, r, response.URL, http.StatusFound)
}
