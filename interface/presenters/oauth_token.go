package presenters

import (
	"context"
	"log"
	"net/http"
	"strings"

	"gitlab.com/xianon/mashighlight/domain/entities"
	"gitlab.com/xianon/mashighlight/interface/keys"
	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type OAuthToken struct{}

func NewOAuthToken() *OAuthToken {
	return &OAuthToken{}
}

func convertMAccessToken(accessToken *entities.AccessToken) map[string]interface{} {
	mAccessToken := map[string]interface{}{
		"access_token": accessToken.AccessToken,
	}
	if accessToken.TokenType != nil {
		mAccessToken["token_type"] = *accessToken.TokenType
	}
	if accessToken.ExpiresIn != nil {
		mAccessToken["expires_in"] = *accessToken.ExpiresIn
	}
	if accessToken.RefreshToken != nil {
		mAccessToken["refresh_token"] = *accessToken.RefreshToken
	}
	if len(accessToken.Scopes) > 0 {
		mAccessToken["scope"] = strings.Join(accessToken.Scopes, " ")
	}
	return mAccessToken
}

func (t *OAuthToken) RespondCreate(ctx context.Context, response *responses.OAuthTokenCreate) {
	w, ok := keys.GetResponseWriter(ctx)
	if !ok {
		return
	}

	log.Printf("%+v", response.AccessToken)
	mAccessToken := convertMAccessToken(response.AccessToken)
	log.Printf("%+v", mAccessToken)

	if err := respondJSON(w, http.StatusOK, mAccessToken); err != nil {
		respondError(w, err)
	}
}

func (*OAuthToken) RespondError(ctx context.Context, response *responses.OAuthTokenError) {
	if w, ok := keys.GetResponseWriter(ctx); ok {
		respondError(w, response.Error)
	}

	log.Printf("OAuthToken Error: %s\n", response.Error.Error())
}
