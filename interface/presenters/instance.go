package presenters

import (
	"context"
	"log"
	"net/http"

	"gitlab.com/xianon/mashighlight/domain/entities"
	"gitlab.com/xianon/mashighlight/interface/keys"
	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type Instance struct{}

func NewInstance() *Instance {
	return &Instance{}
}

func convertMInstance(instance *entities.Instance) map[string]interface{} {
	mInstance := map[string]interface{}{
		"uri":   instance.URI,
		"title": instance.Title,
		"urls": map[string]interface{}{
			"streaming_api": instance.URLs.StreamingAPI,
		},
		"registrations": false,
	}
	for k, v := range instance.Other {
		mInstance[k] = v
	}
	return mInstance
}

func (i *Instance) RespondRead(ctx context.Context, response *responses.InstanceRead) {
	w, ok := keys.GetResponseWriter(ctx)
	if !ok {
		return
	}

	mInstance := convertMInstance(response.Instance)

	if err := respondJSON(w, http.StatusOK, mInstance); err != nil {
		respondError(w, err)
	}
}

func (*Instance) RespondError(ctx context.Context, response *responses.InstanceError) {
	if w, ok := keys.GetResponseWriter(ctx); ok {
		respondError(w, response.Error)
	}

	log.Printf("Instance Error: %s\n", response.Error.Error())
}
