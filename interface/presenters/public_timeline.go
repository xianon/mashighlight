package presenters

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"gitlab.com/xianon/mashighlight/interface/keys"
	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type PublicTimeline struct {
	mastodonHost string
}

func NewPublicTimeline(mastodonHost string) *PublicTimeline {
	return &PublicTimeline{
		mastodonHost: mastodonHost,
	}
}

func (p *PublicTimeline) RespondRead(ctx context.Context, response *responses.PublicTimelineRead) {
	w, ok := keys.GetResponseWriter(ctx)
	if !ok {
		return
	}

	mStatuses := make([]map[string]interface{}, len(response.Statuses))
	for i, status := range response.Statuses {
		mStatuses[i] = convertMStatus(status)
	}

	if len(response.Statuses) > 0 {
		prevURI := &url.URL{
			Scheme: "https",
			Host:   p.mastodonHost,
			Path:   "/api/v1/timelines/home",
		}
		prevQuery := prevURI.Query()
		prevQuery.Set("min_id", strconv.FormatInt(response.Statuses[0].ID, 10))
		if response.Local {
			prevQuery.Set("local", "true")
		}
		if response.Remote {
			prevQuery.Set("remote", "true")
		}
		if response.OnlyMedia {
			prevQuery.Set("only_media", "true")
		}
		if response.Limit != nil {
			prevQuery.Set("limit", strconv.Itoa(*response.Limit))
		}
		prevURI.RawQuery = prevQuery.Encode()

		nextURI := &url.URL{
			Scheme: "https",
			Host:   p.mastodonHost,
			Path:   "/api/v1/timelines/home",
		}
		nextQuery := nextURI.Query()
		nextQuery.Set("max_id", strconv.FormatInt(response.Statuses[len(response.Statuses)-1].ID, 10))
		if response.Local {
			nextQuery.Set("local", "true")
		}
		if response.Remote {
			nextQuery.Set("remote", "true")
		}
		if response.OnlyMedia {
			nextQuery.Set("only_media", "true")
		}
		if response.Limit != nil {
			nextQuery.Set("limit", strconv.Itoa(*response.Limit))
		}
		nextURI.RawQuery = nextQuery.Encode()

		links := []string{
			fmt.Sprintf("<%s>; rel=\"prev\"", prevURI.String()),
			fmt.Sprintf("<%s>; rel=\"next\"", nextURI.String()),
		}
		w.Header().Set("Link", strings.Join(links, ", "))
	}

	if err := respondJSON(w, http.StatusOK, mStatuses); err != nil {
		respondError(w, err)
	}
}

func (*PublicTimeline) RespondError(ctx context.Context, response *responses.PublicTimelineError) {
	if w, ok := keys.GetResponseWriter(ctx); ok {
		respondError(w, response.Error)
	}

	log.Printf("PublicTimeline Error: %s\n", response.Error.Error())
}
