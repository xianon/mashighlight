package presenters

import (
	"context"
	"log"
	"net/http"

	"gitlab.com/xianon/mashighlight/interface/keys"
	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type EmptyList struct{}

func NewEmptyList() *EmptyList {
	return &EmptyList{}
}

func (e *EmptyList) RespondRead(ctx context.Context, response *responses.EmptyListRead) {
	w, ok := keys.GetResponseWriter(ctx)
	if !ok {
		return
	}

	emptyList := make([]struct{}, 0)

	if err := respondJSON(w, http.StatusOK, emptyList); err != nil {
		respondError(w, err)
	}
}

func (*EmptyList) RespondError(ctx context.Context, response *responses.EmptyListError) {
	if w, ok := keys.GetResponseWriter(ctx); ok {
		respondError(w, response.Error)
	}

	log.Printf("EmptyList Error: %s\n", response.Error.Error())
}
