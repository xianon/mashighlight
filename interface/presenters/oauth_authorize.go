package presenters

import (
	"context"
	"log"
	"net/http"
	"net/url"
	"strconv"

	"gitlab.com/xianon/mashighlight/interface/keys"
	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type OAuthAuthorize struct {
	mastodonHost string
}

func NewOAuthAuthorize(mastodonHost string) *OAuthAuthorize {
	return &OAuthAuthorize{
		mastodonHost: mastodonHost,
	}
}

func (a *OAuthAuthorize) RespondRead(ctx context.Context, response *responses.OAuthAuthorizeRead) {
	r, ok := keys.GetRequest(ctx)
	if !ok {
		return
	}
	w, ok := keys.GetResponseWriter(ctx)
	if !ok {
		return
	}

	url := &url.URL{
		Scheme: "https",
		Host:   a.mastodonHost,
		Path:   "/oauth/authorize",
	}
	query := url.Query()
	query.Set("response_type", response.ResponseType)
	query.Set("client_id", response.ClientID)
	query.Set("redirect_uri", response.RedirectURI)
	if response.Scope != nil {
		query.Set("scope", *response.Scope)
	}
	if response.ForceLogin != nil {
		query.Set("force_login", strconv.FormatBool(*response.ForceLogin))
	}
	for k, v := range response.Other {
		query.Set(k, v)
	}
	url.RawQuery = query.Encode()

	http.Redirect(w, r, url.String(), http.StatusFound)
}

func (*OAuthAuthorize) RespondError(ctx context.Context, response *responses.OAuthAuthorizeError) {
	if w, ok := keys.GetResponseWriter(ctx); ok {
		respondError(w, response.Error)
	}

	log.Printf("OAuthAuthorize Error: %s\n", response.Error.Error())
}
