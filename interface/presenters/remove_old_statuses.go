package presenters

import (
	"context"
	"log"

	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type RemoveOldStatuses struct{}

func NewRemoveOldStatuses() *RemoveOldStatuses {
	return &RemoveOldStatuses{}
}

func (*RemoveOldStatuses) Respond(ctx context.Context, response *responses.RemoveOldStatuses) {
	log.Println("Succeeded removing old statuses")
}

func (*RemoveOldStatuses) RespondError(ctx context.Context, response *responses.RemoveOldStatusesError) {
	log.Printf("Failed removing old statuses: %v\n", response.Error)
}
