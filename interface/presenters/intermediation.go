package presenters

import (
	"context"
	"log"
	"net/http"
	"strconv"

	"gitlab.com/xianon/mashighlight/interface/keys"
	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type Intermediation struct{}

func NewIntermediation() *Intermediation {
	return &Intermediation{}
}

func (i *Intermediation) RespondCreate(ctx context.Context, response *responses.IntermediationCreate) {
	w, ok := keys.GetResponseWriter(ctx)
	if !ok {
		return
	}

	i.respond(w, response.StatusCode, response.CacheControl, response.Age, response.Pragma, response.Vary, response.Etag, response.ContentType, response.Body)
}

func (i *Intermediation) RespondRead(ctx context.Context, response *responses.IntermediationRead) {
	w, ok := keys.GetResponseWriter(ctx)
	if !ok {
		return
	}

	i.respond(w, response.StatusCode, response.CacheControl, response.Age, response.Pragma, response.Vary, response.Etag, response.ContentType, response.Body)
}

func (*Intermediation) RespondError(ctx context.Context, response *responses.IntermediationError) {
	if w, ok := keys.GetResponseWriter(ctx); ok {
		respondError(w, response.Error)
	}

	log.Printf("Intermediation Error: %s\n", response.Error.Error())
}

func (*Intermediation) respond(w http.ResponseWriter, statusCode int, cacheControl *string, age *int, pragma *string, vary *string, etag *string, contentType *string, body []byte) {
	if cacheControl != nil {
		w.Header().Add("Cache-Control", *cacheControl)
	}
	if age != nil {
		w.Header().Add("Age", strconv.Itoa(*age))
	}
	if pragma != nil {
		w.Header().Add("Pragma", *pragma)
	}
	if vary != nil {
		w.Header().Add("Vary", *vary)
	}
	if etag != nil {
		w.Header().Add("Etag", *etag)
	}
	if contentType != nil {
		w.Header().Add("Content-Type", *contentType)
	}

	w.WriteHeader(statusCode)
	w.Write(body)
}
