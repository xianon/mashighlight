package presenters

import (
	"context"
	"log"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/xianon/mashighlight/domain/entities"
	"gitlab.com/xianon/mashighlight/interface/keys"
	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type Status struct{}

func NewStatus() *Status {
	return &Status{}
}

func convertMStatus(status *entities.Status) map[string]interface{} {
	mStatus := map[string]interface{}{
		"id":               strconv.FormatInt(status.ID, 10),
		"created_at":       status.CreatedAt.Format(time.RFC3339Nano),
		"visibility":       status.Visibility.String(),
		"reblogs_count":    status.ReblogsCount,
		"favourites_count": status.FavouritesCount,
		"replies_count":    status.RepliesCount,
		"in_reply_to_id":   nil,
		"reblog":           nil,
	}
	for k, v := range status.Other {
		mStatus[k] = v
	}
	return mStatus
}

func (s *Status) RespondRead(ctx context.Context, response *responses.StatusRead) {
	w, ok := keys.GetResponseWriter(ctx)
	if !ok {
		return
	}

	mStatus := convertMStatus(response.Status)

	if err := respondJSON(w, http.StatusOK, mStatus); err != nil {
		respondError(w, err)
	}
}

func (*Status) RespondError(ctx context.Context, response *responses.StatusError) {
	if w, ok := keys.GetResponseWriter(ctx); ok {
		respondError(w, response.Error)
	}

	log.Printf("Status Error: %s\n", response.Error.Error())
}
