package controllers

import (
	"net/http"

	"gitlab.com/xianon/mashighlight/interface/keys"
	"gitlab.com/xianon/mashighlight/usecase/input_boundaries"
	"gitlab.com/xianon/mashighlight/usecase/requests"
)

type Top struct {
	inputBoundary input_boundaries.Top
}

func NewTop(inputBoundary input_boundaries.Top) *Top {
	return &Top{
		inputBoundary: inputBoundary,
	}
}

func (t *Top) Index(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	ctx = keys.SetRequest(ctx, r)
	ctx = keys.SetResponseWriter(ctx, w)

	request := requests.NewTopIndex()
	t.inputBoundary.HandleIndex(ctx, request)
}
