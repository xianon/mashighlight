package controllers

import (
	"log"
	"net/http"
	"strings"

	"gitlab.com/xianon/mashighlight/interface/keys"
	"gitlab.com/xianon/mashighlight/usecase/input_boundaries"
	"gitlab.com/xianon/mashighlight/usecase/requests"
)

type OAuthAuthorize struct {
	inputBoundary input_boundaries.OAuthAuthorize
}

func NewOAuthAuthorize(inputBoundary input_boundaries.OAuthAuthorize) *OAuthAuthorize {
	return &OAuthAuthorize{
		inputBoundary: inputBoundary,
	}
}

func (a *OAuthAuthorize) Read(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	ctx = keys.SetRequest(ctx, r)
	ctx = keys.SetResponseWriter(ctx, w)

	var responseType *string
	var clientID *string
	var redirectURI *string
	var scope *string
	var forceLogin *bool
	forceLoginOK := true
	other := make(map[string]string)
	if err := r.ParseForm(); err == nil {
		responseType = getFormValue(r.Form, "response_type")
		clientID = getFormValue(r.Form, "client_id")
		redirectURI = getFormValue(r.Form, "redirect_uri")
		scope = getFormValue(r.Form, "scope")
		forceLogin, forceLoginOK = parseGetFormBoolValue(r.Form, "force_login")

		for k := range r.Form {
			if k == "response_type" || k == "client_id" || k == "redirect_uri" || k == "scope" || k == "force_login" {
				continue
			}
			v := getFormValue(r.Form, k)
			if v == nil {
				continue
			}
			other[k] = *v
		}
	}

	errMessages := make([]string, 0)

	if responseType == nil {
		errMessages = append(errMessages, "response_type が指定されていません")
	}

	if clientID == nil {
		errMessages = append(errMessages, "client_id が指定されていません")
	}

	if redirectURI == nil {
		errMessages = append(errMessages, "redirect_uri が指定されていません")
	}

	if !forceLoginOK {
		errMessages = append(errMessages, "force_login が有効な真偽値ではありません")
	}

	if len(errMessages) > 0 {
		errMsg := strings.Join(errMessages, "\n")
		request := requests.NewOAuthAuthorizeError(errMsg)
		a.inputBoundary.HandleError(ctx, request)
		return
	}

	// MEMO: glitch.com で "//" が "/" として渡されてしまうため ":/" を "://" とする
	if !strings.HasPrefix(*redirectURI, "com.tapbots.Ivory") {
		for i := 0; i+1 < len(*redirectURI); i++ {
			if (*redirectURI)[i] != ':' || (*redirectURI)[i+1] != '/' {
				continue
			} else if i+2 < len(*redirectURI) && (*redirectURI)[i+2] == '/' {
				i += 2
				continue
			}

			r := (*redirectURI)[:i+2] + "/" + (*redirectURI)[i+2:]
			redirectURI = &r
			i += 2
		}
	}
	log.Println("redirectURI")
	log.Print(*redirectURI)

	request := requests.NewOAuthAuthorizeRead(*responseType, *clientID, *redirectURI, scope, forceLogin, other)
	a.inputBoundary.HandleRead(ctx, request)
}
