package controllers

import (
	"mime"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

func getAccessToken(header http.Header) *string {
	const prefix = "Bearer "
	authorization := header.Get("Authorization")
	if !strings.HasPrefix(authorization, prefix) {
		return nil
	}
	accessToken := authorization[len(prefix):]
	return &accessToken
}

func getContentType(header http.Header) string {
	ct := header.Get("Content-Type")
	if contentType, _, err := mime.ParseMediaType(ct); err == nil {
		return contentType
	}
	return ""
}

func getFormValue(form url.Values, key string) *string {
	if vs := form[key]; len(vs) > 0 {
		value := vs[0]
		return &value
	}
	return nil
}

func getJsonFormValue(form map[string]interface{}, key string) *string {
	if value, ok := form[key].(string); ok {
		return &value
	}
	return nil
}

func parseGetFormBoolValue(form url.Values, key string) (*bool, bool) {
	value := getFormValue(form, key)
	if value == nil {
		return nil, true
	}
	if b, err := strconv.ParseBool(*value); err == nil {
		return &b, true
	}
	return nil, false
}

func parseGetFormInt64Value(form url.Values, key string) (*int64, bool) {
	value := getFormValue(form, key)
	if value == nil {
		return nil, true
	}
	if i, err := strconv.ParseInt(*value, 10, 64); err == nil {
		return &i, true
	}
	return nil, false
}

func parseGetFormIntValue(form url.Values, key string) (*int, bool) {
	value := getFormValue(form, key)
	if value == nil {
		return nil, true
	}
	if i, err := strconv.Atoi(*value); err == nil {
		return &i, true
	}
	return nil, false
}
