package controllers

import (
	"io"
	"net/http"

	"gitlab.com/xianon/mashighlight/interface/keys"
	"gitlab.com/xianon/mashighlight/usecase/input_boundaries"
	"gitlab.com/xianon/mashighlight/usecase/requests"
)

type Intermediation struct {
	inputBoundary input_boundaries.Intermediation
}

func NewIntermediation(inputBoundary input_boundaries.Intermediation) *Intermediation {
	return &Intermediation{
		inputBoundary: inputBoundary,
	}
}

func (i *Intermediation) Create(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	ctx = keys.SetRequest(ctx, r)
	ctx = keys.SetResponseWriter(ctx, w)

	ireq, err := i.getIntermediationRequest(r)
	if err != nil {
		request := requests.NewIntermediationError(err.Error())
		i.inputBoundary.HandleError(ctx, request)
		return
	}

	request := requests.NewIntermediationCreate(ireq.RequestURI, ireq.Token, ireq.ContentType, ireq.Body)
	i.inputBoundary.HandleCreate(ctx, request)
}

func (i *Intermediation) Read(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	ctx = keys.SetRequest(ctx, r)
	ctx = keys.SetResponseWriter(ctx, w)

	ireq, err := i.getIntermediationRequest(r)
	if err != nil {
		request := requests.NewIntermediationError(err.Error())
		i.inputBoundary.HandleError(ctx, request)
		return
	}

	request := requests.NewIntermediationRead(ireq.RequestURI, ireq.Token, ireq.ContentType, ireq.Body)
	i.inputBoundary.HandleRead(ctx, request)
}

type intermediationRequest struct {
	RequestURI  string
	Token       *string
	ContentType *string
	Body        []byte
}

func (*Intermediation) getIntermediationRequest(r *http.Request) (*intermediationRequest, error) {
	body, err := io.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}

	ireq := &intermediationRequest{
		RequestURI: r.RequestURI,
		Token:      getAccessToken(r.Header),
		Body:       body,
	}

	contentTypeValues := r.Header.Values("Content-Type")
	if len(contentTypeValues) > 0 {
		ireq.ContentType = &contentTypeValues[0]
	}

	return ireq, nil
}
