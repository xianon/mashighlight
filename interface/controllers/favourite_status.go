package controllers

import (
	"context"
	"net/http"
	"strings"

	"gitlab.com/xianon/mashighlight/interface/keys"
	"gitlab.com/xianon/mashighlight/usecase/input_boundaries"
	"gitlab.com/xianon/mashighlight/usecase/requests"
)

type favouriteStatusIDKey struct{}

type FavouriteStatus struct {
	inputBoundary input_boundaries.FavouriteStatus
}

func NewFavouriteStatus(inputBoundary input_boundaries.FavouriteStatus) *FavouriteStatus {
	return &FavouriteStatus{
		inputBoundary: inputBoundary,
	}
}

func (f *FavouriteStatus) Create(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	ctx = keys.SetRequest(ctx, r)
	ctx = keys.SetResponseWriter(ctx, w)

	token := getAccessToken(r.Header)

	errMessages := make([]string, 0)

	var id int64
	if i, ok := f.GetID(ctx); ok {
		id = i
	} else {
		errMessages = append(errMessages, "id が指定されていません")
	}

	if len(errMessages) > 0 {
		errMessage := strings.Join(errMessages, "\n")
		request := requests.NewFavouriteStatusError(errMessage)
		f.inputBoundary.HandleError(ctx, request)
		return
	}

	request := requests.NewFavouriteStatusCreate(token, id)
	f.inputBoundary.HandleCreate(ctx, request)
}

func (*FavouriteStatus) SetID(ctx context.Context, id int64) context.Context {
	return context.WithValue(ctx, favouriteStatusIDKey{}, id)
}

func (*FavouriteStatus) GetID(ctx context.Context) (int64, bool) {
	v, ok := ctx.Value(favouriteStatusIDKey{}).(int64)
	return v, ok
}
