package controllers

import (
	"net/http"
	"strings"

	"gitlab.com/xianon/mashighlight/interface/keys"
	"gitlab.com/xianon/mashighlight/usecase/input_boundaries"
	"gitlab.com/xianon/mashighlight/usecase/requests"
)

type PublicTimeline struct {
	inputBoundary input_boundaries.PublicTimeline
}

func NewPublicTimeline(inputBoundary input_boundaries.PublicTimeline) *PublicTimeline {
	return &PublicTimeline{
		inputBoundary: inputBoundary,
	}
}

func (p *PublicTimeline) Read(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	ctx = keys.SetRequest(ctx, r)
	ctx = keys.SetResponseWriter(ctx, w)

	token := getAccessToken(r.Header)

	var local bool
	var localOK bool
	var remote bool
	var remoteOK bool
	var onlyMedia bool
	var onlyMediaOK bool
	var maxID *int64
	var maxIDOK bool
	var sinceID *int64
	var sinceIDOK bool
	var minID *int64
	var minIDOK bool
	var limit *int
	var limitOK bool
	if err := r.ParseForm(); err == nil {
		l, ok := parseGetFormBoolValue(r.Form, "local")
		local = l != nil && *l
		localOK = ok

		rmt, ok := parseGetFormBoolValue(r.Form, "remote")
		remote = rmt != nil && *rmt
		remoteOK = ok

		o, ok := parseGetFormBoolValue(r.Form, "only_media")
		onlyMedia = o != nil && *o
		onlyMediaOK = ok

		maxID, maxIDOK = parseGetFormInt64Value(r.Form, "max_id")
		sinceID, sinceIDOK = parseGetFormInt64Value(r.Form, "since_id")
		minID, minIDOK = parseGetFormInt64Value(r.Form, "min_id")
		limit, limitOK = parseGetFormIntValue(r.Form, "limit")
	}

	errMessages := make([]string, 0)

	if !localOK {
		errMessages = append(errMessages, "local が有効な真偽値ではありません")
	}

	if !remoteOK {
		errMessages = append(errMessages, "remote が有効な真偽値ではありません")
	}

	if !onlyMediaOK {
		errMessages = append(errMessages, "only_media が有効な真偽値ではありません")
	}

	if !maxIDOK {
		errMessages = append(errMessages, "max_id が有効な数値ではありません")
	}

	if !sinceIDOK {
		errMessages = append(errMessages, "since_id が有効な数値ではありません")
	}

	if !minIDOK {
		errMessages = append(errMessages, "min_id が有効な数値ではありません")
	}

	if !limitOK {
		errMessages = append(errMessages, "limit が有効な数値ではありません")
	}

	if len(errMessages) > 0 {
		errMsg := strings.Join(errMessages, "\n")
		request := requests.NewPublicTimelineError(errMsg)
		p.inputBoundary.HandleError(ctx, request)
		return
	}

	request := requests.NewPublicTimelineRead(token, local, remote, onlyMedia, maxID, sinceID, minID, limit)
	p.inputBoundary.HandleRead(ctx, request)
}
