package controllers

import (
	"context"

	"gitlab.com/xianon/mashighlight/usecase/input_boundaries"
	"gitlab.com/xianon/mashighlight/usecase/requests"
)

type RemoveOldStatuses struct {
	inputBoundary input_boundaries.RemoveOldStatuses
}

func NewRemoveOldStatuses(inputBoundary input_boundaries.RemoveOldStatuses) *RemoveOldStatuses {
	return &RemoveOldStatuses{
		inputBoundary: inputBoundary,
	}
}

func (r *RemoveOldStatuses) Remove(ctx context.Context) {
	request := requests.NewRemoveOldStatuses()
	r.inputBoundary.Handle(ctx, request)
}
