package controllers

import (
	"net/http"

	"gitlab.com/xianon/mashighlight/interface/keys"
	"gitlab.com/xianon/mashighlight/usecase/input_boundaries"
	"gitlab.com/xianon/mashighlight/usecase/requests"
)

type Redirect struct {
	inputBoundary input_boundaries.Redirect
}

func NewRedirect(inputBoundary input_boundaries.Redirect) *Redirect {
	return &Redirect{
		inputBoundary: inputBoundary,
	}
}

func (d *Redirect) Read(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	ctx = keys.SetRequest(ctx, r)
	ctx = keys.SetResponseWriter(ctx, w)

	request := requests.NewRedirectRead(r.RequestURI)
	d.inputBoundary.HandleRead(ctx, request)
}
