package controllers

import (
	"log"
	"net/http"
	"strings"

	"gitlab.com/xianon/mashighlight/interface/keys"
	"gitlab.com/xianon/mashighlight/usecase/input_boundaries"
	"gitlab.com/xianon/mashighlight/usecase/requests"
)

type Streaming struct {
	inputBoundary input_boundaries.Streaming
}

func NewStreaming(inputBoundary input_boundaries.Streaming) *Streaming {
	return &Streaming{
		inputBoundary: inputBoundary,
	}
}

func (s *Streaming) Read(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	ctx = keys.SetRequest(ctx, r)
	ctx = keys.SetResponseWriter(ctx, w)

	var token *string
	var stream *string
	if err := r.ParseForm(); err == nil {
		token = getFormValue(r.Form, "access_token")
		stream = getFormValue(r.Form, "stream")
	}

	errMessages := make([]string, 0)

	// TODO:
	if stream != nil {
		switch *stream {
		case "user":
		case "public":
		case "public:local":
		case "hashtag":
		case "hashtag:local":
		case "list":
		case "direct":
			break
		default:
			errMessages = append(errMessages, "stream が有効な値ではありません")
		}
	}

	if len(errMessages) > 0 {
		errMessage := strings.Join(errMessages, "\n")
		request := requests.NewStreamingError(errMessage)
		s.inputBoundary.HandleError(ctx, request)
		return
	}

	log.Printf("Streaming Request Header: %#v", r.Header)
	request := requests.NewStreamingRead(token, stream)
	s.inputBoundary.HandleRead(ctx, request)
}
