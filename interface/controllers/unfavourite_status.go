package controllers

import (
	"context"
	"net/http"
	"strings"

	"gitlab.com/xianon/mashighlight/interface/keys"
	"gitlab.com/xianon/mashighlight/usecase/input_boundaries"
	"gitlab.com/xianon/mashighlight/usecase/requests"
)

type unfavouriteStatusIDKey struct{}

type UnfavouriteStatus struct {
	inputBoundary input_boundaries.UnfavouriteStatus
}

func NewUnfavouriteStatus(inputBoundary input_boundaries.UnfavouriteStatus) *UnfavouriteStatus {
	return &UnfavouriteStatus{
		inputBoundary: inputBoundary,
	}
}

func (u *UnfavouriteStatus) Create(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	ctx = keys.SetRequest(ctx, r)
	ctx = keys.SetResponseWriter(ctx, w)

	token := getAccessToken(r.Header)

	errMessages := make([]string, 0)

	var id int64
	if i, ok := u.GetID(ctx); ok {
		id = i
	} else {
		errMessages = append(errMessages, "id が指定されていません")
	}

	if len(errMessages) > 0 {
		errMessage := strings.Join(errMessages, "\n")
		request := requests.NewUnfavouriteStatusError(errMessage)
		u.inputBoundary.HandleError(ctx, request)
		return
	}

	request := requests.NewUnfavouriteStatusCreate(token, id)
	u.inputBoundary.HandleCreate(ctx, request)
}

func (*UnfavouriteStatus) SetID(ctx context.Context, id int64) context.Context {
	return context.WithValue(ctx, unfavouriteStatusIDKey{}, id)
}

func (*UnfavouriteStatus) GetID(ctx context.Context) (int64, bool) {
	v, ok := ctx.Value(unfavouriteStatusIDKey{}).(int64)
	return v, ok
}
