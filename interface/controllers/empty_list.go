package controllers

import (
	"net/http"

	"gitlab.com/xianon/mashighlight/interface/keys"
	"gitlab.com/xianon/mashighlight/usecase/input_boundaries"
	"gitlab.com/xianon/mashighlight/usecase/requests"
)

type EmptyList struct {
	inputBoundary input_boundaries.EmptyList
}

func NewEmptyList(inputBoundary input_boundaries.EmptyList) *EmptyList {
	return &EmptyList{
		inputBoundary: inputBoundary,
	}
}

func (e *EmptyList) Read(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	ctx = keys.SetRequest(ctx, r)
	ctx = keys.SetResponseWriter(ctx, w)

	request := requests.NewEmptyListRead(false, nil)
	e.inputBoundary.HandleRead(ctx, request)
}

func (e *EmptyList) AuthenticateRead(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	ctx = keys.SetRequest(ctx, r)
	ctx = keys.SetResponseWriter(ctx, w)

	token := getAccessToken(r.Header)

	request := requests.NewEmptyListRead(true, token)
	e.inputBoundary.HandleRead(ctx, request)
}
