package controllers

import (
	"net/http"

	"gitlab.com/xianon/mashighlight/interface/keys"
	"gitlab.com/xianon/mashighlight/usecase/input_boundaries"
	"gitlab.com/xianon/mashighlight/usecase/requests"
)

type Register struct {
	inputBoundary input_boundaries.Register
}

func NewRegister(inputBoundary input_boundaries.Register) *Register {
	return &Register{
		inputBoundary: inputBoundary,
	}
}

func (g *Register) Index(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	ctx = keys.SetRequest(ctx, r)
	ctx = keys.SetResponseWriter(ctx, w)

	request := requests.NewRegisterIndex()
	g.inputBoundary.HandleIndex(ctx, request)
}
