package controllers

import (
	"net/http"
	"strings"

	"gitlab.com/xianon/mashighlight/interface/keys"
	"gitlab.com/xianon/mashighlight/usecase/input_boundaries"
	"gitlab.com/xianon/mashighlight/usecase/requests"
)

type Authorized struct {
	inputBoundary input_boundaries.Authorized
}

func NewAuthorized(inputBoundary input_boundaries.Authorized) *Authorized {
	return &Authorized{
		inputBoundary: inputBoundary,
	}
}

func (a *Authorized) Index(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	ctx = keys.SetRequest(ctx, r)
	ctx = keys.SetResponseWriter(ctx, w)

	var code *string
	if err := r.ParseForm(); err == nil {
		code = getFormValue(r.Form, "code")
	}

	errMessages := make([]string, 0)

	if code == nil {
		errMessages = append(errMessages, "code が指定されていません")
	}

	if len(errMessages) > 0 {
		errMsg := strings.Join(errMessages, "\n")
		request := requests.NewAuthorizedError(errMsg)
		a.inputBoundary.HandleError(ctx, request)
		return
	}

	request := requests.NewAuthorizedIndex(*code)
	a.inputBoundary.HandleIndex(ctx, request)
}
