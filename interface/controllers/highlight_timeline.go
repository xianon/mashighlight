package controllers

import (
	"log"
	"net/http"
	"strings"

	"gitlab.com/xianon/mashighlight/interface/keys"
	"gitlab.com/xianon/mashighlight/usecase/input_boundaries"
	"gitlab.com/xianon/mashighlight/usecase/requests"
)

type HighlightTimeline struct {
	inputBoundary input_boundaries.HighlightTimeline
}

func NewHighlightTimeline(inputBoundary input_boundaries.HighlightTimeline) *HighlightTimeline {
	return &HighlightTimeline{
		inputBoundary: inputBoundary,
	}
}

func (h *HighlightTimeline) Read(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	ctx = keys.SetRequest(ctx, r)
	ctx = keys.SetResponseWriter(ctx, w)

	log.Printf("HighlightTimeline r.Header %+v", r.Header)

	token := getAccessToken(r.Header)

	var local bool
	var localOK bool
	var maxID *int64
	var maxIDOK bool
	var sinceID *int64
	var sinceIDOK bool
	var minID *int64
	var minIDOK bool
	var limit *int
	var limitOK bool
	if err := r.ParseForm(); err == nil {
		l, ok := parseGetFormBoolValue(r.Form, "local")
		local = l != nil && *l
		localOK = ok

		maxID, maxIDOK = parseGetFormInt64Value(r.Form, "max_id")
		sinceID, sinceIDOK = parseGetFormInt64Value(r.Form, "since_id")
		minID, minIDOK = parseGetFormInt64Value(r.Form, "min_id")
		limit, limitOK = parseGetFormIntValue(r.Form, "limit")
	}

	errMessages := make([]string, 0)

	if !localOK {
		errMessages = append(errMessages, "local が有効な真偽値ではありません")
	}

	if !maxIDOK {
		errMessages = append(errMessages, "max_id が有効な数値ではありません")
	}

	if !sinceIDOK {
		errMessages = append(errMessages, "since_id が有効な数値ではありません")
	}

	if !minIDOK {
		errMessages = append(errMessages, "min_id が有効な数値ではありません")
	}

	if !limitOK {
		errMessages = append(errMessages, "limit が有効な数値ではありません")
	}

	if len(errMessages) > 0 {
		errMsg := strings.Join(errMessages, "\n")
		request := requests.NewHighlightTimelineError(errMsg)
		h.inputBoundary.HandleError(ctx, request)
		return
	}

	request := requests.NewHighlightTimelineRead(token, local, maxID, sinceID, minID, limit)
	h.inputBoundary.HandleRead(ctx, request)
}
