package controllers

import (
	"net/http"

	"gitlab.com/xianon/mashighlight/interface/keys"
	"gitlab.com/xianon/mashighlight/usecase/input_boundaries"
	"gitlab.com/xianon/mashighlight/usecase/requests"
)

type Instance struct {
	inputBoundary input_boundaries.Instance
}

func NewInstance(inputBoundary input_boundaries.Instance) *Instance {
	return &Instance{
		inputBoundary: inputBoundary,
	}
}

func (i *Instance) Read(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	ctx = keys.SetRequest(ctx, r)
	ctx = keys.SetResponseWriter(ctx, w)

	request := requests.NewInstanceRead()
	i.inputBoundary.HandleRead(ctx, request)
}
