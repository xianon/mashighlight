package controllers

import (
	"context"
	"net/http"
	"strings"

	"gitlab.com/xianon/mashighlight/interface/keys"
	"gitlab.com/xianon/mashighlight/usecase/input_boundaries"
	"gitlab.com/xianon/mashighlight/usecase/requests"
)

type statusIDKey struct{}

type Status struct {
	inputBoundary input_boundaries.Status
}

func NewStatus(inputBoundary input_boundaries.Status) *Status {
	return &Status{
		inputBoundary: inputBoundary,
	}
}

func (s *Status) Read(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	ctx = keys.SetRequest(ctx, r)
	ctx = keys.SetResponseWriter(ctx, w)

	token := getAccessToken(r.Header)

	errMessages := make([]string, 0)

	var id int64
	if i, ok := s.GetID(ctx); ok {
		id = i
	} else {
		errMessages = append(errMessages, "id が指定されていません")
	}

	if len(errMessages) > 0 {
		errMessage := strings.Join(errMessages, "\n")
		request := requests.NewStatusError(errMessage)
		s.inputBoundary.HandleError(ctx, request)
		return
	}

	request := requests.NewStatusRead(token, id)
	s.inputBoundary.HandleRead(ctx, request)
}

func (*Status) SetID(ctx context.Context, id int64) context.Context {
	return context.WithValue(ctx, statusIDKey{}, id)
}

func (*Status) GetID(ctx context.Context) (int64, bool) {
	v, ok := ctx.Value(statusIDKey{}).(int64)
	return v, ok
}
