package controllers

import (
	"context"
	"net/http"
	"strings"

	"gitlab.com/xianon/mashighlight/interface/keys"
	"gitlab.com/xianon/mashighlight/usecase/input_boundaries"
	"gitlab.com/xianon/mashighlight/usecase/requests"
)

type accountIDKey struct{}

type Account struct {
	inputBoundary input_boundaries.Account
}

func NewAccount(inputBoundary input_boundaries.Account) *Account {
	return &Account{
		inputBoundary: inputBoundary,
	}
}

func (a *Account) Read(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	ctx = keys.SetRequest(ctx, r)
	ctx = keys.SetResponseWriter(ctx, w)

	token := getAccessToken(r.Header)

	errMessages := make([]string, 0)

	var id int64
	if i, ok := a.GetID(ctx); ok {
		id = i
	} else {
		errMessages = append(errMessages, "id が指定されていません")
	}

	if len(errMessages) > 0 {
		errMessage := strings.Join(errMessages, "\n")
		request := requests.NewAccountError(errMessage)
		a.inputBoundary.HandleError(ctx, request)
		return
	}

	request := requests.NewAccountRead(token, id)
	a.inputBoundary.HandleRead(ctx, request)
}

func (*Account) SetID(ctx context.Context, id int64) context.Context {
	return context.WithValue(ctx, accountIDKey{}, id)
}

func (*Account) GetID(ctx context.Context) (int64, bool) {
	v, ok := ctx.Value(accountIDKey{}).(int64)
	return v, ok
}
