package controllers

import (
	"encoding/json"
	"log"
	"net/http"
	"regexp"
	"strings"

	"gitlab.com/xianon/mashighlight/interface/keys"
	"gitlab.com/xianon/mashighlight/usecase/input_boundaries"
	"gitlab.com/xianon/mashighlight/usecase/requests"
)

type OAuthToken struct {
	inputBoundary input_boundaries.OAuthToken
}

func NewOAuthToken(inputBoundary input_boundaries.OAuthToken) *OAuthToken {
	return &OAuthToken{
		inputBoundary: inputBoundary,
	}
}

func (t *OAuthToken) Create(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	ctx = keys.SetRequest(ctx, r)
	ctx = keys.SetResponseWriter(ctx, w)

	var grantType *string
	var clientID *string
	var clientSecret *string
	var redirectURI *string
	var scope *string
	var code *string
	if err := r.ParseForm(); err == nil && len(r.Form) > 0 {
		grantType = getFormValue(r.Form, "grant_type")
		clientID = getFormValue(r.Form, "client_id")
		clientSecret = getFormValue(r.Form, "client_secret")
		redirectURI = getFormValue(r.Form, "redirect_uri")
		scope = getFormValue(r.Form, "scope")
		code = getFormValue(r.Form, "code")
		log.Println("r.Form")
		log.Print(r.Form)
	} else if err := r.ParseMultipartForm(32 << 20); err == nil && len(r.MultipartForm.Value) > 0 {
		grantType = getFormValue(r.MultipartForm.Value, "grant_type")
		clientID = getFormValue(r.MultipartForm.Value, "client_id")
		clientSecret = getFormValue(r.MultipartForm.Value, "client_secret")
		redirectURI = getFormValue(r.MultipartForm.Value, "redirect_uri")
		scope = getFormValue(r.MultipartForm.Value, "scope")
		code = getFormValue(r.MultipartForm.Value, "code")
		log.Println("r.MultipartForm.Value")
		log.Print(r.MultipartForm.Value)
	} else if getContentType(r.Header) == "application/json" {
		var form map[string]interface{}
		if err := json.NewDecoder(r.Body).Decode(&form); err == nil {
			grantType = getJsonFormValue(form, "grant_type")
			clientID = getJsonFormValue(form, "client_id")
			clientSecret = getJsonFormValue(form, "client_secret")
			redirectURI = getJsonFormValue(form, "redirect_uri")
			scope = getJsonFormValue(form, "scope")
			code = getJsonFormValue(form, "code")
			log.Println("json.NewDecoder")
			log.Print(form)
		}
	}

	errMessages := make([]string, 0)

	if grantType == nil {
		errMessages = append(errMessages, "grant_type が指定されていません")
	}

	if clientID == nil {
		errMessages = append(errMessages, "client_id が指定されていません")
	}

	if clientSecret == nil {
		errMessages = append(errMessages, "client_secret が指定されていません")
	}

	if len(errMessages) > 0 {
		errMsg := strings.Join(errMessages, "\n")
		request := requests.NewOAuthTokenError(errMsg)
		t.inputBoundary.HandleError(ctx, request)
		return
	}

	var scopes []string
	if scope != nil {
		scopes = regexp.MustCompile(" +").Split(*scope, -1)
	} else {
		scopes = make([]string, 0)
	}

	request := requests.NewOAuthTokenCreate(*grantType, *clientID, *clientSecret, redirectURI, scopes, code)
	t.inputBoundary.HandleCreate(ctx, request)
}
