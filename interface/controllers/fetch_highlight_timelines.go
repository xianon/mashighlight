package controllers

import (
	"context"

	"gitlab.com/xianon/mashighlight/usecase/input_boundaries"
	"gitlab.com/xianon/mashighlight/usecase/requests"
)

type FetchHighlightTimelines struct {
	inputBoundary input_boundaries.FetchHighlightTimelines
}

func NewFetchHighlightTimelines(inputBoundary input_boundaries.FetchHighlightTimelines) *FetchHighlightTimelines {
	return &FetchHighlightTimelines{
		inputBoundary: inputBoundary,
	}
}

func (f *FetchHighlightTimelines) Fetch(ctx context.Context) {
	request := requests.NewFetchHighlightTimelines()
	f.inputBoundary.Handle(ctx, request)
}
