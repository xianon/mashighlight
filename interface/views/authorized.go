package views

import (
	"embed"
	"html/template"
	"io"

	"gitlab.com/xianon/mashighlight/usecase/responses"
)

//go:embed authorized.html
var authorized embed.FS

type Authorized struct {
	template *template.Template
}

func NewAuthorized() (*Authorized, error) {
	template, err := template.ParseFS(authorized, "authorized.html")
	if err != nil {
		return nil, err
	}
	t := &Authorized{
		template: template,
	}
	return t, nil
}

func (a *Authorized) Execute(wr io.Writer, data *responses.AuthorizedIndex) error {
	return a.template.Execute(wr, data)
}
