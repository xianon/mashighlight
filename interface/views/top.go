package views

import (
	"embed"
	"html/template"
	"io"

	"gitlab.com/xianon/mashighlight/usecase/responses"
)

//go:embed top.html
var top embed.FS

type Top struct {
	template *template.Template
}

func NewTop() (*Top, error) {
	template, err := template.ParseFS(top, "top.html")
	if err != nil {
		return nil, err
	}
	t := &Top{
		template: template,
	}
	return t, nil
}

func (t *Top) Execute(wr io.Writer, data *responses.TopIndex) error {
	return t.template.Execute(wr, data)
}
