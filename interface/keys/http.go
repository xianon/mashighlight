package keys

import (
	"context"
	"net/http"
)

type requestKey struct{}

func GetRequest(ctx context.Context) (*http.Request, bool) {
	v := ctx.Value(requestKey{})
	if v == nil {
		return nil, false
	}
	if r, ok := v.(*http.Request); ok {
		return r, true
	}
	return nil, false
}

func SetRequest(ctx context.Context, r *http.Request) context.Context {
	return context.WithValue(ctx, requestKey{}, r)
}

type responseWriterKey struct{}

func GetResponseWriter(ctx context.Context) (http.ResponseWriter, bool) {
	v := ctx.Value(responseWriterKey{})
	if v == nil {
		return nil, false
	}
	if w, ok := v.(http.ResponseWriter); ok {
		return w, true
	}
	return nil, false
}

func SetResponseWriter(ctx context.Context, w http.ResponseWriter) context.Context {
	return context.WithValue(ctx, responseWriterKey{}, w)
}
