package schema

import (
	"math/rand"
	"time"

	"entgo.io/ent"
	"entgo.io/ent/dialect/entsql"
	"entgo.io/ent/schema"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
	"entgo.io/ent/schema/index"
)

// Status holds the schema definition for the Status entity.
type Status struct {
	ent.Schema
}

// Annotations of the Status.
func (Status) Annotations() []schema.Annotation {
	return []schema.Annotation{
		entsql.Annotation{
			Table: "statuses",
		},
	}
}

// Fields of the Status.
func (Status) Fields() []ent.Field {
	return []ent.Field{
		field.Int64("id").
			DefaultFunc(func() int64 {
				return time.Now().UnixMilli()<<16 | rand.Int63n(0x100)
			}),
		field.Int64("owner_id"),
		field.Int64("original_id"),
		field.Time("created_at"),
		field.Time("highlighted_at").
			Immutable().
			Default(time.Now),
		field.Bool("local").
			Default(false),
		field.String("visibility"),
		field.Int32("reblogs_count").
			Default(0),
		field.Int32("favourites_count").
			Default(0),
		field.Int32("replies_count").
			Default(0),
		field.Int64("in_reply_to_original_id").
			Optional().
			Nillable(),
		field.Bool("has_attached_media").
			Default(false),
		field.JSON("other", make(map[string]interface{})),
	}
}

// Edges of the Status.
func (Status) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("owner", Account.Type).
			Ref("statuses").
			Unique().
			Field("owner_id").
			Required(),
	}
}

// Indexes of the Status.
func (Status) Indexes() []ent.Index {
	return []ent.Index{
		index.Fields("owner_id", "original_id").
			Unique(),
	}
}
