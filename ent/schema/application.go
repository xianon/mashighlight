package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/field"
)

// Application holds the schema definition for the Application entity.
type Application struct {
	ent.Schema
}

// Fields of the Application.
func (Application) Fields() []ent.Field {
	return []ent.Field{
		field.Int32("id"),
		field.String("name"),
		field.String("website").
			Optional().
			Nillable(),
		field.String("vapid_key").
			Optional().
			Nillable(),
		field.String("client_id").
			Unique(),
		field.String("client_secret"),
		field.JSON("other", make(map[string]interface{})),
	}
}

// Edges of the Application.
func (Application) Edges() []ent.Edge {
	return nil
}

// Indexes of the Status.
func (Application) Indexes() []ent.Index {
	return nil
}
