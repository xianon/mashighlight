// Code generated by entc, DO NOT EDIT.

package migrate

import (
	"entgo.io/ent/dialect/entsql"
	"entgo.io/ent/dialect/sql/schema"
	"entgo.io/ent/schema/field"
)

var (
	// AccountsColumns holds the columns for the "accounts" table.
	AccountsColumns = []*schema.Column{
		{Name: "id", Type: field.TypeInt64, Increment: true},
		{Name: "token", Type: field.TypeString, Nullable: true},
		{Name: "last_accessed_at", Type: field.TypeTime},
	}
	// AccountsTable holds the schema information for the "accounts" table.
	AccountsTable = &schema.Table{
		Name:       "accounts",
		Columns:    AccountsColumns,
		PrimaryKey: []*schema.Column{AccountsColumns[0]},
	}
	// ApplicationsColumns holds the columns for the "applications" table.
	ApplicationsColumns = []*schema.Column{
		{Name: "id", Type: field.TypeInt32, Increment: true},
		{Name: "name", Type: field.TypeString},
		{Name: "website", Type: field.TypeString, Nullable: true},
		{Name: "vapid_key", Type: field.TypeString, Nullable: true},
		{Name: "client_id", Type: field.TypeString, Unique: true},
		{Name: "client_secret", Type: field.TypeString},
		{Name: "other", Type: field.TypeJSON},
	}
	// ApplicationsTable holds the schema information for the "applications" table.
	ApplicationsTable = &schema.Table{
		Name:       "applications",
		Columns:    ApplicationsColumns,
		PrimaryKey: []*schema.Column{ApplicationsColumns[0]},
	}
	// StatusesColumns holds the columns for the "statuses" table.
	StatusesColumns = []*schema.Column{
		{Name: "id", Type: field.TypeInt64, Increment: true},
		{Name: "original_id", Type: field.TypeInt64},
		{Name: "created_at", Type: field.TypeTime},
		{Name: "highlighted_at", Type: field.TypeTime},
		{Name: "local", Type: field.TypeBool, Default: false},
		{Name: "visibility", Type: field.TypeString},
		{Name: "reblogs_count", Type: field.TypeInt32, Default: 0},
		{Name: "favourites_count", Type: field.TypeInt32, Default: 0},
		{Name: "replies_count", Type: field.TypeInt32, Default: 0},
		{Name: "in_reply_to_original_id", Type: field.TypeInt64, Nullable: true},
		{Name: "has_attached_media", Type: field.TypeBool, Default: false},
		{Name: "other", Type: field.TypeJSON},
		{Name: "owner_id", Type: field.TypeInt64},
	}
	// StatusesTable holds the schema information for the "statuses" table.
	StatusesTable = &schema.Table{
		Name:       "statuses",
		Columns:    StatusesColumns,
		PrimaryKey: []*schema.Column{StatusesColumns[0]},
		ForeignKeys: []*schema.ForeignKey{
			{
				Symbol:     "statuses_accounts_statuses",
				Columns:    []*schema.Column{StatusesColumns[12]},
				RefColumns: []*schema.Column{AccountsColumns[0]},
				OnDelete:   schema.NoAction,
			},
		},
		Indexes: []*schema.Index{
			{
				Name:    "status_owner_id_original_id",
				Unique:  true,
				Columns: []*schema.Column{StatusesColumns[12], StatusesColumns[1]},
			},
		},
	}
	// Tables holds all the tables in the schema.
	Tables = []*schema.Table{
		AccountsTable,
		ApplicationsTable,
		StatusesTable,
	}
)

func init() {
	StatusesTable.ForeignKeys[0].RefTable = AccountsTable
	StatusesTable.Annotation = &entsql.Annotation{
		Table: "statuses",
	}
}
