// Code generated by entc, DO NOT EDIT.

package application

const (
	// Label holds the string label denoting the application type in the database.
	Label = "application"
	// FieldID holds the string denoting the id field in the database.
	FieldID = "id"
	// FieldName holds the string denoting the name field in the database.
	FieldName = "name"
	// FieldWebsite holds the string denoting the website field in the database.
	FieldWebsite = "website"
	// FieldVapidKey holds the string denoting the vapid_key field in the database.
	FieldVapidKey = "vapid_key"
	// FieldClientID holds the string denoting the client_id field in the database.
	FieldClientID = "client_id"
	// FieldClientSecret holds the string denoting the client_secret field in the database.
	FieldClientSecret = "client_secret"
	// FieldOther holds the string denoting the other field in the database.
	FieldOther = "other"
	// Table holds the table name of the application in the database.
	Table = "applications"
)

// Columns holds all SQL columns for application fields.
var Columns = []string{
	FieldID,
	FieldName,
	FieldWebsite,
	FieldVapidKey,
	FieldClientID,
	FieldClientSecret,
	FieldOther,
}

// ValidColumn reports if the column name is valid (part of the table columns).
func ValidColumn(column string) bool {
	for i := range Columns {
		if column == Columns[i] {
			return true
		}
	}
	return false
}
