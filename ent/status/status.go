// Code generated by entc, DO NOT EDIT.

package status

import (
	"time"
)

const (
	// Label holds the string label denoting the status type in the database.
	Label = "status"
	// FieldID holds the string denoting the id field in the database.
	FieldID = "id"
	// FieldOwnerID holds the string denoting the owner_id field in the database.
	FieldOwnerID = "owner_id"
	// FieldOriginalID holds the string denoting the original_id field in the database.
	FieldOriginalID = "original_id"
	// FieldCreatedAt holds the string denoting the created_at field in the database.
	FieldCreatedAt = "created_at"
	// FieldHighlightedAt holds the string denoting the highlighted_at field in the database.
	FieldHighlightedAt = "highlighted_at"
	// FieldLocal holds the string denoting the local field in the database.
	FieldLocal = "local"
	// FieldVisibility holds the string denoting the visibility field in the database.
	FieldVisibility = "visibility"
	// FieldReblogsCount holds the string denoting the reblogs_count field in the database.
	FieldReblogsCount = "reblogs_count"
	// FieldFavouritesCount holds the string denoting the favourites_count field in the database.
	FieldFavouritesCount = "favourites_count"
	// FieldRepliesCount holds the string denoting the replies_count field in the database.
	FieldRepliesCount = "replies_count"
	// FieldInReplyToOriginalID holds the string denoting the in_reply_to_original_id field in the database.
	FieldInReplyToOriginalID = "in_reply_to_original_id"
	// FieldHasAttachedMedia holds the string denoting the has_attached_media field in the database.
	FieldHasAttachedMedia = "has_attached_media"
	// FieldOther holds the string denoting the other field in the database.
	FieldOther = "other"
	// EdgeOwner holds the string denoting the owner edge name in mutations.
	EdgeOwner = "owner"
	// Table holds the table name of the status in the database.
	Table = "statuses"
	// OwnerTable is the table that holds the owner relation/edge.
	OwnerTable = "statuses"
	// OwnerInverseTable is the table name for the Account entity.
	// It exists in this package in order to avoid circular dependency with the "account" package.
	OwnerInverseTable = "accounts"
	// OwnerColumn is the table column denoting the owner relation/edge.
	OwnerColumn = "owner_id"
)

// Columns holds all SQL columns for status fields.
var Columns = []string{
	FieldID,
	FieldOwnerID,
	FieldOriginalID,
	FieldCreatedAt,
	FieldHighlightedAt,
	FieldLocal,
	FieldVisibility,
	FieldReblogsCount,
	FieldFavouritesCount,
	FieldRepliesCount,
	FieldInReplyToOriginalID,
	FieldHasAttachedMedia,
	FieldOther,
}

// ValidColumn reports if the column name is valid (part of the table columns).
func ValidColumn(column string) bool {
	for i := range Columns {
		if column == Columns[i] {
			return true
		}
	}
	return false
}

var (
	// DefaultHighlightedAt holds the default value on creation for the "highlighted_at" field.
	DefaultHighlightedAt func() time.Time
	// DefaultLocal holds the default value on creation for the "local" field.
	DefaultLocal bool
	// DefaultReblogsCount holds the default value on creation for the "reblogs_count" field.
	DefaultReblogsCount int32
	// DefaultFavouritesCount holds the default value on creation for the "favourites_count" field.
	DefaultFavouritesCount int32
	// DefaultRepliesCount holds the default value on creation for the "replies_count" field.
	DefaultRepliesCount int32
	// DefaultHasAttachedMedia holds the default value on creation for the "has_attached_media" field.
	DefaultHasAttachedMedia bool
	// DefaultID holds the default value on creation for the "id" field.
	DefaultID func() int64
)
