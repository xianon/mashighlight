// Code generated by entc, DO NOT EDIT.

package ent

import (
	"encoding/json"
	"fmt"
	"strings"

	"entgo.io/ent/dialect/sql"
	"gitlab.com/xianon/mashighlight/ent/application"
)

// Application is the model entity for the Application schema.
type Application struct {
	config `json:"-"`
	// ID of the ent.
	ID int32 `json:"id,omitempty"`
	// Name holds the value of the "name" field.
	Name string `json:"name,omitempty"`
	// Website holds the value of the "website" field.
	Website *string `json:"website,omitempty"`
	// VapidKey holds the value of the "vapid_key" field.
	VapidKey *string `json:"vapid_key,omitempty"`
	// ClientID holds the value of the "client_id" field.
	ClientID string `json:"client_id,omitempty"`
	// ClientSecret holds the value of the "client_secret" field.
	ClientSecret string `json:"client_secret,omitempty"`
	// Other holds the value of the "other" field.
	Other map[string]interface{} `json:"other,omitempty"`
}

// scanValues returns the types for scanning values from sql.Rows.
func (*Application) scanValues(columns []string) ([]interface{}, error) {
	values := make([]interface{}, len(columns))
	for i := range columns {
		switch columns[i] {
		case application.FieldOther:
			values[i] = new([]byte)
		case application.FieldID:
			values[i] = new(sql.NullInt64)
		case application.FieldName, application.FieldWebsite, application.FieldVapidKey, application.FieldClientID, application.FieldClientSecret:
			values[i] = new(sql.NullString)
		default:
			return nil, fmt.Errorf("unexpected column %q for type Application", columns[i])
		}
	}
	return values, nil
}

// assignValues assigns the values that were returned from sql.Rows (after scanning)
// to the Application fields.
func (a *Application) assignValues(columns []string, values []interface{}) error {
	if m, n := len(values), len(columns); m < n {
		return fmt.Errorf("mismatch number of scan values: %d != %d", m, n)
	}
	for i := range columns {
		switch columns[i] {
		case application.FieldID:
			value, ok := values[i].(*sql.NullInt64)
			if !ok {
				return fmt.Errorf("unexpected type %T for field id", value)
			}
			a.ID = int32(value.Int64)
		case application.FieldName:
			if value, ok := values[i].(*sql.NullString); !ok {
				return fmt.Errorf("unexpected type %T for field name", values[i])
			} else if value.Valid {
				a.Name = value.String
			}
		case application.FieldWebsite:
			if value, ok := values[i].(*sql.NullString); !ok {
				return fmt.Errorf("unexpected type %T for field website", values[i])
			} else if value.Valid {
				a.Website = new(string)
				*a.Website = value.String
			}
		case application.FieldVapidKey:
			if value, ok := values[i].(*sql.NullString); !ok {
				return fmt.Errorf("unexpected type %T for field vapid_key", values[i])
			} else if value.Valid {
				a.VapidKey = new(string)
				*a.VapidKey = value.String
			}
		case application.FieldClientID:
			if value, ok := values[i].(*sql.NullString); !ok {
				return fmt.Errorf("unexpected type %T for field client_id", values[i])
			} else if value.Valid {
				a.ClientID = value.String
			}
		case application.FieldClientSecret:
			if value, ok := values[i].(*sql.NullString); !ok {
				return fmt.Errorf("unexpected type %T for field client_secret", values[i])
			} else if value.Valid {
				a.ClientSecret = value.String
			}
		case application.FieldOther:
			if value, ok := values[i].(*[]byte); !ok {
				return fmt.Errorf("unexpected type %T for field other", values[i])
			} else if value != nil && len(*value) > 0 {
				if err := json.Unmarshal(*value, &a.Other); err != nil {
					return fmt.Errorf("unmarshal field other: %w", err)
				}
			}
		}
	}
	return nil
}

// Update returns a builder for updating this Application.
// Note that you need to call Application.Unwrap() before calling this method if this Application
// was returned from a transaction, and the transaction was committed or rolled back.
func (a *Application) Update() *ApplicationUpdateOne {
	return (&ApplicationClient{config: a.config}).UpdateOne(a)
}

// Unwrap unwraps the Application entity that was returned from a transaction after it was closed,
// so that all future queries will be executed through the driver which created the transaction.
func (a *Application) Unwrap() *Application {
	tx, ok := a.config.driver.(*txDriver)
	if !ok {
		panic("ent: Application is not a transactional entity")
	}
	a.config.driver = tx.drv
	return a
}

// String implements the fmt.Stringer.
func (a *Application) String() string {
	var builder strings.Builder
	builder.WriteString("Application(")
	builder.WriteString(fmt.Sprintf("id=%v", a.ID))
	builder.WriteString(", name=")
	builder.WriteString(a.Name)
	if v := a.Website; v != nil {
		builder.WriteString(", website=")
		builder.WriteString(*v)
	}
	if v := a.VapidKey; v != nil {
		builder.WriteString(", vapid_key=")
		builder.WriteString(*v)
	}
	builder.WriteString(", client_id=")
	builder.WriteString(a.ClientID)
	builder.WriteString(", client_secret=")
	builder.WriteString(a.ClientSecret)
	builder.WriteString(", other=")
	builder.WriteString(fmt.Sprintf("%v", a.Other))
	builder.WriteByte(')')
	return builder.String()
}

// Applications is a parsable slice of Application.
type Applications []*Application

func (a Applications) config(cfg config) {
	for _i := range a {
		a[_i].config = cfg
	}
}
