package input_boundaries

import (
	"context"

	"gitlab.com/xianon/mashighlight/usecase/requests"
)

type EmptyList interface {
	HandleRead(ctx context.Context, request *requests.EmptyListRead)
}
