package input_boundaries

import (
	"context"

	"gitlab.com/xianon/mashighlight/usecase/requests"
)

type Register interface {
	HandleIndex(ctx context.Context, request *requests.RegisterIndex)
}
