package input_boundaries

import (
	"context"

	"gitlab.com/xianon/mashighlight/usecase/requests"
)

type OAuthAuthorize interface {
	HandleRead(ctx context.Context, request *requests.OAuthAuthorizeRead)
	HandleError(ctx context.Context, request *requests.OAuthAuthorizeError)
}
