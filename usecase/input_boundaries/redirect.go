package input_boundaries

import (
	"context"

	"gitlab.com/xianon/mashighlight/usecase/requests"
)

type Redirect interface {
	HandleRead(ctx context.Context, request *requests.RedirectRead)
}
