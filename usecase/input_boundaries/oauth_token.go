package input_boundaries

import (
	"context"

	"gitlab.com/xianon/mashighlight/usecase/requests"
)

type OAuthToken interface {
	HandleCreate(ctx context.Context, request *requests.OAuthTokenCreate)
	HandleError(ctx context.Context, request *requests.OAuthTokenError)
}
