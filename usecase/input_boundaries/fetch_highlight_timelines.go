package input_boundaries

import (
	"context"

	"gitlab.com/xianon/mashighlight/usecase/requests"
)

type FetchHighlightTimelines interface {
	Handle(ctx context.Context, request *requests.FetchHighlightTimelines)
}
