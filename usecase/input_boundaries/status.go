package input_boundaries

import (
	"context"

	"gitlab.com/xianon/mashighlight/usecase/requests"
)

type Status interface {
	HandleRead(ctx context.Context, request *requests.StatusRead)
	HandleError(ctx context.Context, request *requests.StatusError)
}
