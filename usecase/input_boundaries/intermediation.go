package input_boundaries

import (
	"context"

	"gitlab.com/xianon/mashighlight/usecase/requests"
)

type Intermediation interface {
	HandleCreate(ctx context.Context, request *requests.IntermediationCreate)
	HandleRead(ctx context.Context, request *requests.IntermediationRead)
	HandleError(ctx context.Context, request *requests.IntermediationError)
}
