package input_boundaries

import (
	"context"

	"gitlab.com/xianon/mashighlight/usecase/requests"
)

type UnfavouriteStatus interface {
	HandleCreate(ctx context.Context, request *requests.UnfavouriteStatusCreate)
	HandleError(ctx context.Context, request *requests.UnfavouriteStatusError)
}
