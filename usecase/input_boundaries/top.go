package input_boundaries

import (
	"context"

	"gitlab.com/xianon/mashighlight/usecase/requests"
)

type Top interface {
	HandleIndex(ctx context.Context, request *requests.TopIndex)
}
