package input_boundaries

import (
	"context"

	"gitlab.com/xianon/mashighlight/usecase/requests"
)

type HighlightTimeline interface {
	HandleRead(ctx context.Context, request *requests.HighlightTimelineRead)
	HandleError(ctx context.Context, request *requests.HighlightTimelineError)
}
