package input_boundaries

import (
	"context"

	"gitlab.com/xianon/mashighlight/usecase/requests"
)

type Instance interface {
	HandleRead(ctx context.Context, request *requests.InstanceRead)
}
