package input_boundaries

import (
	"context"

	"gitlab.com/xianon/mashighlight/usecase/requests"
)

type Authorized interface {
	HandleIndex(ctx context.Context, request *requests.AuthorizedIndex)
	HandleError(ctx context.Context, request *requests.AuthorizedError)
}
