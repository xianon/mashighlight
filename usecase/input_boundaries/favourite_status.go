package input_boundaries

import (
	"context"

	"gitlab.com/xianon/mashighlight/usecase/requests"
)

type FavouriteStatus interface {
	HandleCreate(ctx context.Context, request *requests.FavouriteStatusCreate)
	HandleError(ctx context.Context, request *requests.FavouriteStatusError)
}
