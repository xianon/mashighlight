package input_boundaries

import (
	"context"

	"gitlab.com/xianon/mashighlight/usecase/requests"
)

type Account interface {
	HandleRead(ctx context.Context, request *requests.AccountRead)
	HandleError(ctx context.Context, request *requests.AccountError)
}
