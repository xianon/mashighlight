package input_boundaries

import (
	"context"

	"gitlab.com/xianon/mashighlight/usecase/requests"
)

type RemoveOldStatuses interface {
	Handle(ctx context.Context, request *requests.RemoveOldStatuses)
}
