package input_boundaries

import (
	"context"

	"gitlab.com/xianon/mashighlight/usecase/requests"
)

type PublicTimeline interface {
	HandleRead(ctx context.Context, request *requests.PublicTimelineRead)
	HandleError(ctx context.Context, request *requests.PublicTimelineError)
}
