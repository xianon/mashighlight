package input_boundaries

import (
	"context"

	"gitlab.com/xianon/mashighlight/usecase/requests"
)

type Streaming interface {
	HandleRead(ctx context.Context, request *requests.StreamingRead)
	HandleError(ctx context.Context, request *requests.StreamingError)
}
