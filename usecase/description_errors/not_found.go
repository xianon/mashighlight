package description_errors

import "errors"

type NotFoundError struct {
	msg string
	dsc *string
}

func NewNotFoundError(msg string) *NotFoundError {
	return &NotFoundError{
		msg: msg,
		dsc: nil,
	}
}

func NewNotFoundErrorWithDescription(msg string, dsc string) *NotFoundError {
	return &NotFoundError{
		msg: msg,
		dsc: &dsc,
	}
}

func (e *NotFoundError) Error() string {
	return e.msg
}

func (e *NotFoundError) ErrorDescription() *string {
	return e.dsc
}

func IsNotFound(err error) bool {
	if err == nil {
		return false
	}
	var e *NotFoundError
	return errors.As(err, &e)
}
