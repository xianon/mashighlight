package description_errors

import "errors"

type BadRequestError struct {
	msg string
	dsc *string
}

func NewBadRequestError(msg string) *BadRequestError {
	return &BadRequestError{
		msg: msg,
		dsc: nil,
	}
}

func NewBadRequestErrorWithDescription(msg string, dsc string) *BadRequestError {
	return &BadRequestError{
		msg: msg,
		dsc: &dsc,
	}
}

func (e *BadRequestError) Error() string {
	return e.msg
}

func (e *BadRequestError) ErrorDescription() *string {
	return e.dsc
}

func IsBadRequest(err error) bool {
	if err == nil {
		return false
	}
	var e *BadRequestError
	return errors.As(err, &e)
}
