package description_errors

import "errors"

type InteractionError struct {
	msg string
	dsc *string
}

func NewInteractionError(msg string) *InteractionError {
	return &InteractionError{
		msg: msg,
		dsc: nil,
	}
}

func NewInteractionErrorWithDescription(msg string, dsc string) *InteractionError {
	return &InteractionError{
		msg: msg,
		dsc: &dsc,
	}
}

func (e *InteractionError) Error() string {
	return e.msg
}

func (e *InteractionError) ErrorDescription() *string {
	return e.dsc
}

func IsInteractionError(err error) bool {
	if err == nil {
		return false
	}
	var e *InteractionError
	return errors.As(err, &e)
}
