package description_errors

type ErrorWithDescription interface {
	Error() string
	ErrorDescription() *string
}
