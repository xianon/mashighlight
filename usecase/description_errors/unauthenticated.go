package description_errors

import "errors"

type UnauthenticatedError struct {
	msg string
	dsc *string
}

func NewUnauthenticatedError(msg string) *UnauthenticatedError {
	return &UnauthenticatedError{
		msg: msg,
		dsc: nil,
	}
}

func NewUnauthenticatedErrorWithDescription(msg string, dsc string) *UnauthenticatedError {
	return &UnauthenticatedError{
		msg: msg,
		dsc: &dsc,
	}
}

func (e *UnauthenticatedError) Error() string {
	return e.msg
}

func (e *UnauthenticatedError) ErrorDescription() *string {
	return e.dsc
}

func IsUnauthenticated(err error) bool {
	if err == nil {
		return false
	}
	var e *UnauthenticatedError
	return errors.As(err, &e)
}
