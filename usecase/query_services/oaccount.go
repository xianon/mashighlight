package query_services

import (
	"context"

	"gitlab.com/xianon/mashighlight/usecase/dto"
)

type OAccount interface {
	VerifyCredentials(ctx context.Context, token string) (*dto.OAccount, error)
}
