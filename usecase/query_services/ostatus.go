package query_services

import (
	"context"

	"gitlab.com/xianon/mashighlight/usecase/dto"
)

type OStatus interface {
	Favourite(ctx context.Context, token string, id string, favourite bool) (*dto.OStatus, error)
}
