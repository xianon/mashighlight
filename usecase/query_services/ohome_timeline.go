package query_services

import (
	"context"

	"gitlab.com/xianon/mashighlight/usecase/dto"
)

type OHomeTimeline interface {
	FindAll(ctx context.Context, token string, maxID *string, sinceID *string, minID *string, limit *int, local *bool) ([]*dto.OStatus, error)
}
