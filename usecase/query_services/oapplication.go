package query_services

import (
	"context"

	"gitlab.com/xianon/mashighlight/usecase/dto"
)

type OApplication interface {
	Create(ctx context.Context, clientName string, redirectURIs string, scopes *string, website *string) (*dto.OApplication, error)
	VerifyCredentials(ctx context.Context, token string) (*dto.OApplication, error)
}
