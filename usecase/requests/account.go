package requests

type AccountRead struct {
	Token *string
	ID    int64
}

func NewAccountRead(token *string, id int64) *AccountRead {
	return &AccountRead{
		Token: token,
		ID:    id,
	}
}

type AccountError struct {
	Message string
}

func NewAccountError(message string) *AccountError {
	return &AccountError{
		Message: message,
	}
}
