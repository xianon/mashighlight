package requests

type StreamingRead struct {
	Token  *string
	Stream *string
}

func NewStreamingRead(token *string, stream *string) *StreamingRead {
	return &StreamingRead{
		Token:  token,
		Stream: stream,
	}
}

type StreamingError struct {
	Message string
}

func NewStreamingError(message string) *StreamingError {
	return &StreamingError{
		Message: message,
	}
}
