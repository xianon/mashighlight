package requests

type EmptyListRead struct {
	NeedsAuthentication bool
	Token               *string
}

func NewEmptyListRead(needsAuthentication bool, token *string) *EmptyListRead {
	return &EmptyListRead{
		NeedsAuthentication: needsAuthentication,
		Token:               token,
	}
}
