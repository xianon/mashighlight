package requests

type OAuthTokenCreate struct {
	GrantType    string
	ClientID     string
	ClientSecret string
	RedirectURI  *string
	Scopes       []string
	Code         *string
}

func NewOAuthTokenCreate(grantType string, clientID string, clientSecret string, redirectURI *string, scopes []string, code *string) *OAuthTokenCreate {
	return &OAuthTokenCreate{
		GrantType:    grantType,
		ClientID:     clientID,
		ClientSecret: clientSecret,
		RedirectURI:  redirectURI,
		Scopes:       scopes,
		Code:         code,
	}
}

type OAuthTokenError struct {
	Message string
}

func NewOAuthTokenError(message string) *OAuthTokenError {
	return &OAuthTokenError{
		Message: message,
	}
}
