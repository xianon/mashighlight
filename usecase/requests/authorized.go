package requests

type AuthorizedIndex struct {
	Code string
}

func NewAuthorizedIndex(code string) *AuthorizedIndex {
	return &AuthorizedIndex{
		Code: code,
	}
}

type AuthorizedError struct {
	Message string
}

func NewAuthorizedError(message string) *AuthorizedError {
	return &AuthorizedError{
		Message: message,
	}
}
