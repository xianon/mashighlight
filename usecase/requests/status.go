package requests

type StatusRead struct {
	Token *string
	ID    int64
}

func NewStatusRead(token *string, id int64) *StatusRead {
	return &StatusRead{
		Token: token,
		ID:    id,
	}
}

type StatusError struct {
	Message string
}

func NewStatusError(message string) *StatusError {
	return &StatusError{
		Message: message,
	}
}
