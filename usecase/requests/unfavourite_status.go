package requests

type UnfavouriteStatusCreate struct {
	Token *string
	ID    int64
}

func NewUnfavouriteStatusCreate(token *string, id int64) *UnfavouriteStatusCreate {
	return &UnfavouriteStatusCreate{
		Token: token,
		ID:    id,
	}
}

type UnfavouriteStatusError struct {
	Message string
}

func NewUnfavouriteStatusError(message string) *UnfavouriteStatusError {
	return &UnfavouriteStatusError{
		Message: message,
	}
}
