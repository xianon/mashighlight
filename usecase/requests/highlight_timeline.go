package requests

type HighlightTimelineRead struct {
	Token   *string
	Local   bool
	MaxID   *int64
	SinceID *int64
	MinID   *int64
	Limit   *int
}

func NewHighlightTimelineRead(token *string, local bool, maxID *int64, sinceID *int64, minID *int64, limit *int) *HighlightTimelineRead {
	return &HighlightTimelineRead{
		Token:   token,
		MaxID:   maxID,
		SinceID: sinceID,
		MinID:   minID,
		Limit:   limit,
		Local:   local,
	}
}

type HighlightTimelineError struct {
	Message string
}

func NewHighlightTimelineError(message string) *HighlightTimelineError {
	return &HighlightTimelineError{
		Message: message,
	}
}
