package requests

type IntermediationCreate struct {
	RequestURI  string
	Token       *string
	ContentType *string
	Body        []byte
}

func NewIntermediationCreate(requestURI string, token *string, contentType *string, body []byte) *IntermediationCreate {
	return &IntermediationCreate{
		Body:        body,
		ContentType: contentType,
		RequestURI:  requestURI,
		Token:       token,
	}
}

type IntermediationRead struct {
	RequestURI  string
	Token       *string
	ContentType *string
	Body        []byte
}

func NewIntermediationRead(requestURI string, token *string, contentType *string, body []byte) *IntermediationRead {
	return &IntermediationRead{
		Body:        body,
		ContentType: contentType,
		RequestURI:  requestURI,
		Token:       token,
	}
}

type IntermediationError struct {
	Message string
}

func NewIntermediationError(message string) *IntermediationError {
	return &IntermediationError{
		Message: message,
	}
}
