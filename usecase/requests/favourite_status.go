package requests

type FavouriteStatusCreate struct {
	Token *string
	ID    int64
}

func NewFavouriteStatusCreate(token *string, id int64) *FavouriteStatusCreate {
	return &FavouriteStatusCreate{
		Token: token,
		ID:    id,
	}
}

type FavouriteStatusError struct {
	Message string
}

func NewFavouriteStatusError(message string) *FavouriteStatusError {
	return &FavouriteStatusError{
		Message: message,
	}
}
