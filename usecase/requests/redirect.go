package requests

type RedirectRead struct {
	RequestURI string
}

func NewRedirectRead(requestURI string) *RedirectRead {
	return &RedirectRead{
		RequestURI: requestURI,
	}
}
