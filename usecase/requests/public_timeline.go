package requests

type PublicTimelineRead struct {
	Token     *string
	Local     bool
	Remote    bool
	OnlyMedia bool
	MaxID     *int64
	SinceID   *int64
	MinID     *int64
	Limit     *int
}

func NewPublicTimelineRead(token *string, local bool, remote bool, onlyMedia bool, maxID *int64, sinceID *int64, minID *int64, limit *int) *PublicTimelineRead {
	return &PublicTimelineRead{
		Token:     token,
		Local:     local,
		Remote:    remote,
		OnlyMedia: onlyMedia,
		MaxID:     maxID,
		SinceID:   sinceID,
		MinID:     minID,
		Limit:     limit,
	}
}

type PublicTimelineError struct {
	Message string
}

func NewPublicTimelineError(message string) *PublicTimelineError {
	return &PublicTimelineError{
		Message: message,
	}
}
