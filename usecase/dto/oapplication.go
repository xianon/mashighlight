package dto

type OApplication struct {
	// Required attributes
	Name string
	// Optional attributes
	Website  *string
	VapidKey *string
	// Client attributes
	ClientID     *string
	ClientSecret *string
	Other        map[string]interface{}
}

func NewOApplication(name string, website *string, vapidKey *string, clientID *string, clientSecret *string, other map[string]interface{}) *OApplication {
	return &OApplication{
		Name:         name,
		Website:      website,
		VapidKey:     vapidKey,
		ClientID:     clientID,
		ClientSecret: clientSecret,
		Other:        other,
	}
}
