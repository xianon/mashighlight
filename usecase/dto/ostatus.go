package dto

import (
	"time"

	"gitlab.com/xianon/mashighlight/domain/valueobject"
)

type OStatus struct {
	//Base attributes
	ID int64
	//URI string
	CreatedAt time.Time
	Account   OAccount
	//Content string
	Visibility valueobject.Visibility
	//Sensitive bool
	//SpoilerText string
	//MediaAttachments []OAttachment
	Application OApplication
	//Rendering attributes
	//Mentions []OMention
	//Tags []OTag
	//Emojis []OEmoji
	//Informational attributes
	ReblogsCount    int32
	FavouritesCount int32
	RepliesCount    int32
	//Nullable attributes
	//Url *string
	InReplyToID *int64
	//InReplyToAccountID *string
	Reblog *OStatus
	//Poll *OPoll
	//Card *OCard
	//Language *string
	//Text *string
	Other map[string]interface{}
}

func NewOStatus(id int64, createdAt time.Time, oAccount OAccount, visibility valueobject.Visibility, oApplication OApplication, reblogsCount int32, favouritesCount int32, repliesCount int32, inReplyToID *int64, oReblog *OStatus, other map[string]interface{}) *OStatus {
	return &OStatus{
		ID:              id,
		CreatedAt:       createdAt,
		Account:         oAccount,
		Visibility:      visibility,
		Application:     oApplication,
		ReblogsCount:    reblogsCount,
		FavouritesCount: favouritesCount,
		RepliesCount:    repliesCount,
		InReplyToID:     inReplyToID,
		Reblog:          oReblog,
		Other:           other,
	}
}
