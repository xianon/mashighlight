package dto

import "time"

type OAccount struct {
	// Base attributes
	ID       int64
	UserName string
	Acct     string
	//URL string
	// Display attributes
	DisplayName *string
	//Note string
	//Avatar string
	//AvatarStatic string
	//Header string
	//HeaderStatic string
	//Locked bool
	//Emojis []OEmoji
	//Discoverable bool
	// Statistical attributes
	CreatedAt time.Time
	//LastStatusAt time.Time
	//StatusesCount int64
	//FollowersCount int64
	//FollowingCount int64
	// Optional attributes
	//Moved *OAccount
	//Fields *[]OField
	//Bot *bool
	//Source *OSource
	//Suspended *bool
	//MuteExpiresAt *time.Time
	Other map[string]interface{}
}

func NewOAccount(id int64, userName string, acct string, displayName *string, createdAt time.Time, other map[string]interface{}) *OAccount {
	return &OAccount{
		ID:          id,
		UserName:    userName,
		Acct:        acct,
		DisplayName: displayName,
		CreatedAt:   createdAt,
		Other:       other,
	}
}
