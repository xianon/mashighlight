package responses

type AuthorizedIndex struct {
	Domain       string
	InstanceName string
}

func NewAuthorizedIndex(instanceName string, domain string) *AuthorizedIndex {
	return &AuthorizedIndex{
		Domain:       domain,
		InstanceName: instanceName,
	}
}

type AuthorizedError struct {
	Error error
}

func NewAuthorizedError(err error) *AuthorizedError {
	return &AuthorizedError{
		Error: err,
	}
}
