package responses

type RegisterIndex struct {
	URL string
}

func NewRegisterIndex(url string) *RegisterIndex {
	return &RegisterIndex{
		URL: url,
	}
}

type RegisterError struct {
	Error error
}

func NewRegisterError(err error) *RegisterError {
	return &RegisterError{
		Error: err,
	}
}
