package responses

import "gitlab.com/xianon/mashighlight/domain/entities"

type HighlightTimelineRead struct {
	Statuses []*entities.Status
	Local    bool
	Limit    *int
}

func NewHighlightTimelineRead(statuses []*entities.Status, local bool, limit *int) *HighlightTimelineRead {
	return &HighlightTimelineRead{
		Statuses: statuses,
		Limit:    limit,
		Local:    local,
	}
}

type HighlightTimelineError struct {
	Error error
}

func NewHighlightTimelineError(err error) *HighlightTimelineError {
	return &HighlightTimelineError{
		Error: err,
	}
}
