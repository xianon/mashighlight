package responses

import "gitlab.com/xianon/mashighlight/domain/entities"

type InstanceRead struct {
	Instance *entities.Instance
}

func NewInstanceRead(instance *entities.Instance) *InstanceRead {
	return &InstanceRead{
		Instance: instance,
	}
}

type InstanceError struct {
	Error error
}

func NewInstanceError(err error) *InstanceError {
	return &InstanceError{
		Error: err,
	}
}
