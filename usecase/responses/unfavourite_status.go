package responses

import "gitlab.com/xianon/mashighlight/domain/entities"

type UnfavouriteStatusCreate struct {
	Status *entities.Status
}

func NewUnfavouriteStatusCreate(status *entities.Status) *UnfavouriteStatusCreate {
	return &UnfavouriteStatusCreate{
		Status: status,
	}
}

type UnfavouriteStatusError struct {
	Error error
}

func NewUnfavouriteStatusError(err error) *UnfavouriteStatusError {
	return &UnfavouriteStatusError{
		Error: err,
	}
}
