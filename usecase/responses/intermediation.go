package responses

type IntermediationCreate struct {
	Age          *int
	Body         []byte
	CacheControl *string
	ContentType  *string
	Etag         *string
	Pragma       *string
	StatusCode   int
	Vary         *string
}

func NewIntermediationCreate(statusCode int, cacheControl *string, age *int, pragma *string, vary *string, etag *string, contentType *string, body []byte) *IntermediationCreate {
	return &IntermediationCreate{
		Age:          age,
		Body:         body,
		CacheControl: cacheControl,
		ContentType:  contentType,
		Etag:         etag,
		Pragma:       pragma,
		StatusCode:   statusCode,
		Vary:         vary,
	}
}

type IntermediationRead struct {
	Age          *int
	Body         []byte
	CacheControl *string
	ContentType  *string
	Etag         *string
	Pragma       *string
	StatusCode   int
	Vary         *string
}

func NewIntermediationRead(statusCode int, cacheControl *string, age *int, pragma *string, vary *string, etag *string, contentType *string, body []byte) *IntermediationRead {
	return &IntermediationRead{
		Age:          age,
		Body:         body,
		CacheControl: cacheControl,
		ContentType:  contentType,
		Etag:         etag,
		Pragma:       pragma,
		StatusCode:   statusCode,
		Vary:         vary,
	}
}

type IntermediationError struct {
	Error error
}

func NewIntermediationError(err error) *IntermediationError {
	return &IntermediationError{
		Error: err,
	}
}
