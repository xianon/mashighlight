package responses

import "gitlab.com/xianon/mashighlight/domain/entities"

type FavouriteStatusCreate struct {
	Status *entities.Status
}

func NewFavouriteStatusCreate(status *entities.Status) *FavouriteStatusCreate {
	return &FavouriteStatusCreate{
		Status: status,
	}
}

type FavouriteStatusError struct {
	Error error
}

func NewFavouriteStatusError(err error) *FavouriteStatusError {
	return &FavouriteStatusError{
		Error: err,
	}
}
