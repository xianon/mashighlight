package responses

type FetchHighlightTimelines struct{}

func NewFetchHighlightTimelines() *FetchHighlightTimelines {
	return &FetchHighlightTimelines{}
}

type FetchHighlightTimelinesError struct {
	Error error
}

func NewFetchHighlightTimelinesError(err error) *FetchHighlightTimelinesError {
	return &FetchHighlightTimelinesError{
		Error: err,
	}
}
