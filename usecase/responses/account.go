package responses

import (
	"gitlab.com/xianon/mashighlight/usecase/dto"
)

type AccountRead struct {
	OAccount *dto.OAccount
}

func NewAccountRead(oAccount *dto.OAccount) *AccountRead {
	return &AccountRead{
		OAccount: oAccount,
	}
}

type AccountError struct {
	Error error
}

func NewAccountError(err error) *AccountError {
	return &AccountError{
		Error: err,
	}
}
