package responses

type Streaming struct{}

func NewStreaming() *Streaming {
	return &Streaming{}
}

type StreamingError struct {
	Error error
}

func NewStreamingError(err error) *StreamingError {
	return &StreamingError{
		Error: err,
	}
}
