package responses

type RemoveOldStatuses struct{}

func NewRemoveOldStatuses() *RemoveOldStatuses {
	return &RemoveOldStatuses{}
}

type RemoveOldStatusesError struct {
	Error error
}

func NewRemoveOldStatusesError(err error) *RemoveOldStatusesError {
	return &RemoveOldStatusesError{
		Error: err,
	}
}
