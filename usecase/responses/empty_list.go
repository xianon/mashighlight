package responses

type EmptyListRead struct{}

func NewEmptyListRead() *EmptyListRead {
	return &EmptyListRead{}
}

type EmptyListError struct {
	Error error
}

func NewEmptyListError(err error) *EmptyListError {
	return &EmptyListError{
		Error: err,
	}
}
