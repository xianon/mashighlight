package responses

import "gitlab.com/xianon/mashighlight/domain/entities"

type OAuthTokenCreate struct {
	AccessToken *entities.AccessToken
}

func NewOAuthTokenCreate(accessToken *entities.AccessToken) *OAuthTokenCreate {
	return &OAuthTokenCreate{
		AccessToken: accessToken,
	}
}

type OAuthTokenError struct {
	Error error
}

func NewOAuthTokenError(err error) *OAuthTokenError {
	return &OAuthTokenError{
		Error: err,
	}
}
