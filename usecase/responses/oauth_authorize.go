package responses

type OAuthAuthorizeRead struct {
	ResponseType string
	ClientID     string
	RedirectURI  string
	Scope        *string
	ForceLogin   *bool
	Other        map[string]string
}

func NewOAuthAuthorizeRead(responseType string, clientID string, redirectURI string, scope *string, forceLogin *bool, other map[string]string) *OAuthAuthorizeRead {
	return &OAuthAuthorizeRead{
		ResponseType: responseType,
		ClientID:     clientID,
		RedirectURI:  redirectURI,
		Scope:        scope,
		ForceLogin:   forceLogin,
		Other:        other,
	}
}

type OAuthAuthorizeError struct {
	Error error
}

func NewOAuthAuthorizeError(err error) *OAuthAuthorizeError {
	return &OAuthAuthorizeError{
		Error: err,
	}
}
