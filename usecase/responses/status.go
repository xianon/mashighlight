package responses

import "gitlab.com/xianon/mashighlight/domain/entities"

type StatusRead struct {
	Status *entities.Status
}

func NewStatusRead(status *entities.Status) *StatusRead {
	return &StatusRead{
		Status: status,
	}
}

type StatusError struct {
	Error error
}

func NewStatusError(err error) *StatusError {
	return &StatusError{
		Error: err,
	}
}
