package responses

type RedirectRead struct {
	URL string
}

func NewRedirectRead(url string) *RedirectRead {
	return &RedirectRead{
		URL: url,
	}
}
