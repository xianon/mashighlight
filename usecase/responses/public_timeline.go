package responses

import "gitlab.com/xianon/mashighlight/domain/entities"

type PublicTimelineRead struct {
	Statuses  []*entities.Status
	Local     bool
	Remote    bool
	OnlyMedia bool
	Limit     *int
}

func NewPublicTimelineRead(statuses []*entities.Status, local bool, remote bool, onlyMedia bool, limit *int) *PublicTimelineRead {
	return &PublicTimelineRead{
		Statuses:  statuses,
		Local:     local,
		Remote:    remote,
		OnlyMedia: onlyMedia,
		Limit:     limit,
	}
}

type PublicTimelineError struct {
	Error error
}

func NewPublicTimelineError(err error) *PublicTimelineError {
	return &PublicTimelineError{
		Error: err,
	}
}
