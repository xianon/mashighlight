package responses

type TopIndex struct {
	InstanceName         string
	MastodonInstanceName string
	MastodonURL          string
	RegisterURL          string
}

func NewTopIndex(instanceName string, mastodonURL string, mastodonInstanceName string, registerURL string) *TopIndex {
	return &TopIndex{
		InstanceName:         instanceName,
		MastodonInstanceName: mastodonInstanceName,
		MastodonURL:          mastodonURL,
		RegisterURL:          registerURL,
	}
}

type TopError struct {
	Error error
}

func NewTopError(err error) *TopError {
	return &TopError{
		Error: err,
	}
}
