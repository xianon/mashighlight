package output_boundaries

import (
	"context"

	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type Redirect interface {
	RespondRead(ctx context.Context, response *responses.RedirectRead)
}
