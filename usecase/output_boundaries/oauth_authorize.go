package output_boundaries

import (
	"context"

	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type OAuthAuthorize interface {
	RespondRead(ctx context.Context, response *responses.OAuthAuthorizeRead)
	RespondError(ctx context.Context, response *responses.OAuthAuthorizeError)
}
