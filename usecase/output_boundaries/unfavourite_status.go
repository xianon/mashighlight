package output_boundaries

import (
	"context"

	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type UnfavouriteStatus interface {
	RespondCreate(ctx context.Context, response *responses.UnfavouriteStatusCreate)
	RespondError(ctx context.Context, response *responses.UnfavouriteStatusError)
}
