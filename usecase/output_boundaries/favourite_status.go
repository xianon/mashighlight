package output_boundaries

import (
	"context"

	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type FavouriteStatus interface {
	RespondCreate(ctx context.Context, response *responses.FavouriteStatusCreate)
	RespondError(ctx context.Context, response *responses.FavouriteStatusError)
}
