package output_boundaries

import (
	"context"

	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type FetchHighlightTimelines interface {
	Respond(ctx context.Context, response *responses.FetchHighlightTimelines)
	RespondError(ctx context.Context, response *responses.FetchHighlightTimelinesError)
}
