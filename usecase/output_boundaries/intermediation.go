package output_boundaries

import (
	"context"

	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type Intermediation interface {
	RespondCreate(ctx context.Context, response *responses.IntermediationCreate)
	RespondRead(ctx context.Context, response *responses.IntermediationRead)
	RespondError(ctx context.Context, response *responses.IntermediationError)
}
