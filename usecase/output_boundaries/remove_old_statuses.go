package output_boundaries

import (
	"context"

	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type RemoveOldStatuses interface {
	Respond(ctx context.Context, response *responses.RemoveOldStatuses)
	RespondError(ctx context.Context, response *responses.RemoveOldStatusesError)
}
