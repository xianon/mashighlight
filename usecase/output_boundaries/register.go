package output_boundaries

import (
	"context"

	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type Register interface {
	RespondIndex(ctx context.Context, response *responses.RegisterIndex)
	RespondError(ctx context.Context, response *responses.RegisterError)
}
