package output_boundaries

import (
	"context"

	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type HighlightTimeline interface {
	RespondRead(ctx context.Context, response *responses.HighlightTimelineRead)
	RespondError(ctx context.Context, response *responses.HighlightTimelineError)
}
