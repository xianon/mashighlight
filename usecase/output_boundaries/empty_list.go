package output_boundaries

import (
	"context"

	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type EmptyList interface {
	RespondRead(ctx context.Context, response *responses.EmptyListRead)
	RespondError(ctx context.Context, response *responses.EmptyListError)
}
