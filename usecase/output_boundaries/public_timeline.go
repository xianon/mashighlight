package output_boundaries

import (
	"context"

	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type PublicTimeline interface {
	RespondRead(ctx context.Context, response *responses.PublicTimelineRead)
	RespondError(ctx context.Context, response *responses.PublicTimelineError)
}
