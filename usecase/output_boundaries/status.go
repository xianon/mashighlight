package output_boundaries

import (
	"context"

	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type Status interface {
	RespondRead(ctx context.Context, response *responses.StatusRead)
	RespondError(ctx context.Context, response *responses.StatusError)
}
