package output_boundaries

import (
	"context"

	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type OAuthToken interface {
	RespondCreate(ctx context.Context, response *responses.OAuthTokenCreate)
	RespondError(ctx context.Context, response *responses.OAuthTokenError)
}
