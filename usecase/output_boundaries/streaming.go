package output_boundaries

import (
	"context"
	"io"

	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type Streaming interface {
	Streaming(ctx context.Context, response *responses.Streaming) (StreamingCloser, error)
	RespondError(ctx context.Context, response *responses.StreamingError)
}

type StreamingCloser interface {
	io.Closer
	ReadJSON(ctx context.Context, v interface{}) error
	WriteJSON(ctx context.Context, v interface{}) error
}
