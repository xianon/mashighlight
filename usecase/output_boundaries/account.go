package output_boundaries

import (
	"context"

	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type Account interface {
	RespondRead(ctx context.Context, response *responses.AccountRead)
	RespondError(ctx context.Context, response *responses.AccountError)
}
