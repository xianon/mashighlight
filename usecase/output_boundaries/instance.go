package output_boundaries

import (
	"context"

	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type Instance interface {
	RespondRead(ctx context.Context, response *responses.InstanceRead)
	RespondError(ctx context.Context, response *responses.InstanceError)
}
