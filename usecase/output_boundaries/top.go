package output_boundaries

import (
	"context"

	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type Top interface {
	RespondIndex(ctx context.Context, response *responses.TopIndex)
	RespondError(ctx context.Context, response *responses.TopError)
}
