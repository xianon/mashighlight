package output_boundaries

import (
	"context"

	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type Authorized interface {
	RespondIndex(ctx context.Context, response *responses.AuthorizedIndex)
	RespondError(ctx context.Context, response *responses.AuthorizedError)
}
