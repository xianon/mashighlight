package interactors

import (
	"context"
	"strconv"
	"time"

	"gitlab.com/xianon/mashighlight/domain/repositories"
	"gitlab.com/xianon/mashighlight/usecase/description_errors"
	"gitlab.com/xianon/mashighlight/usecase/dto"
	"gitlab.com/xianon/mashighlight/usecase/output_boundaries"
	"gitlab.com/xianon/mashighlight/usecase/query_services"
	"gitlab.com/xianon/mashighlight/usecase/requests"
	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type Account struct {
	accountRepository    repositories.Account
	oAccountQueryService query_services.OAccount
	outputBoundary       output_boundaries.Account
}

func NewAccount(outputBoundary output_boundaries.Account, accountRepository repositories.Account, oAccountQueryService query_services.OAccount) *Account {
	return &Account{
		accountRepository:    accountRepository,
		oAccountQueryService: oAccountQueryService,
		outputBoundary:       outputBoundary,
	}
}

func convertMAccount(oAccount *dto.OAccount) map[string]interface{} {
	mAccount := map[string]interface{}{
		"id":           strconv.FormatInt(oAccount.ID, 10),
		"username":     oAccount.UserName,
		"acct":         oAccount.Acct,
		"display_name": oAccount.DisplayName,
		"created_at":   oAccount.CreatedAt.Format(time.RFC3339Nano),
	}
	for k, v := range oAccount.Other {
		mAccount[k] = v
	}
	return mAccount
}

func (a *Account) HandleRead(ctx context.Context, request *requests.AccountRead) {
	if request.Token == nil {
		err := description_errors.NewUnauthenticatedError("アクセストークンが無効です")
		response := responses.NewAccountError(err)
		a.outputBoundary.RespondError(ctx, response)
		return
	}

	oAccount, err := a.oAccountQueryService.VerifyCredentials(ctx, *request.Token)
	if err != nil {
		response := responses.NewAccountError(err)
		a.outputBoundary.RespondError(ctx, response)
		return
	}

	a.accountRepository.UpdateLastAccessedAt(ctx, oAccount.ID)

	if request.ID != oAccount.ID {
		err := description_errors.NewNotFoundError("Record not found")
		response := responses.NewAccountError(err)
		a.outputBoundary.RespondError(ctx, response)
		return
	}

	response := responses.NewAccountRead(oAccount)
	a.outputBoundary.RespondRead(ctx, response)
}

func (a *Account) HandleError(ctx context.Context, request *requests.AccountError) {
	err := description_errors.NewNotFoundError(request.Message)
	response := responses.NewAccountError(err)
	a.outputBoundary.RespondError(ctx, response)
}
