package interactors

import (
	"context"

	"gitlab.com/xianon/mashighlight/domain/entities"
	"gitlab.com/xianon/mashighlight/domain/repositories"
	"gitlab.com/xianon/mashighlight/usecase/description_errors"
	"gitlab.com/xianon/mashighlight/usecase/output_boundaries"
	"gitlab.com/xianon/mashighlight/usecase/query_services"
	"gitlab.com/xianon/mashighlight/usecase/requests"
	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type PublicTimeline struct {
	accountRepository    repositories.Account
	oAccountQueryService query_services.OAccount
	outputBoundary       output_boundaries.PublicTimeline
	statusRepository     repositories.Status
}

func NewPublicTimeline(outputBoundary output_boundaries.PublicTimeline, accountRepository repositories.Account, oAccountQueryService query_services.OAccount, statusRepository repositories.Status) *PublicTimeline {
	return &PublicTimeline{
		accountRepository:    accountRepository,
		oAccountQueryService: oAccountQueryService,
		outputBoundary:       outputBoundary,
		statusRepository:     statusRepository,
	}
}

func (p *PublicTimeline) HandleRead(ctx context.Context, request *requests.PublicTimelineRead) {
	if request.Token == nil {
		response := responses.NewPublicTimelineRead(make([]*entities.Status, 0), request.Local, request.Remote, request.OnlyMedia, request.Limit)
		p.outputBoundary.RespondRead(ctx, response)
		return
	}

	oAccount, err := p.oAccountQueryService.VerifyCredentials(ctx, *request.Token)
	if err != nil {
		response := responses.NewPublicTimelineError(err)
		p.outputBoundary.RespondError(ctx, response)
		return
	}

	p.accountRepository.UpdateLastAccessedAt(ctx, oAccount.ID)

	statuses, err := p.statusRepository.FindPublicTimeline(ctx, oAccount.ID, request.Local, request.Remote, request.OnlyMedia, request.MaxID, request.SinceID, request.MinID, request.Limit)
	if err != nil {
		response := responses.NewPublicTimelineError(err)
		p.outputBoundary.RespondError(ctx, response)
		return
	}

	response := responses.NewPublicTimelineRead(statuses, request.Local, request.Remote, request.OnlyMedia, request.Limit)
	p.outputBoundary.RespondRead(ctx, response)
}

func (p *PublicTimeline) HandleError(ctx context.Context, request *requests.PublicTimelineError) {
	err := description_errors.NewBadRequestError(request.Message)
	response := responses.NewPublicTimelineError(err)
	p.outputBoundary.RespondError(ctx, response)
}
