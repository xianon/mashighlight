package interactors

import (
	"context"
	"log"
	"strings"
	"time"

	"gitlab.com/xianon/mashighlight/domain/repositories"
	"gitlab.com/xianon/mashighlight/usecase/dto"
	"gitlab.com/xianon/mashighlight/usecase/output_boundaries"
	"gitlab.com/xianon/mashighlight/usecase/query_services"
	"gitlab.com/xianon/mashighlight/usecase/requests"
	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type FetchHighlightTimelines struct {
	accountRepository         repositories.Account
	highlightScoreThreshold   int32
	oHomeTimelineQueryService query_services.OHomeTimeline
	outputBoundary            output_boundaries.FetchHighlightTimelines
	statusRepository          repositories.Status
}

func NewFetchHighlightTimelines(outputBoundary output_boundaries.FetchHighlightTimelines, accountRepository repositories.Account, oHomeTimelineQueryService query_services.OHomeTimeline, statusRepository repositories.Status, highlightScoreThreshold int32) *FetchHighlightTimelines {
	return &FetchHighlightTimelines{
		accountRepository:         accountRepository,
		highlightScoreThreshold:   highlightScoreThreshold,
		oHomeTimelineQueryService: oHomeTimelineQueryService,
		outputBoundary:            outputBoundary,
		statusRepository:          statusRepository,
	}
}

func (f *FetchHighlightTimelines) Handle(ctx context.Context, request *requests.FetchHighlightTimelines) {
	// 登録されている全てのアカウントを取得する
	accounts, err := f.accountRepository.FindAll(ctx)
	if err != nil {
		response := responses.NewFetchHighlightTimelinesError(err)
		f.outputBoundary.RespondError(ctx, response)
		return
	}

	for _, account := range accounts {
		if account.Token == nil {
			continue
		}

		// 元のホームタイムラインを取得する
		const maxOHomeTimelineLength = 400
		limit := maxOHomeTimelineLength
		oHomeTimeline, err := f.oHomeTimelineQueryService.FindAll(ctx, *account.Token, nil, nil, nil, &limit, nil)
		if err != nil {
			response := responses.NewFetchHighlightTimelinesError(err)
			f.outputBoundary.RespondError(ctx, response)
			return
		}

		if len(oHomeTimeline) > 0 {
			log.Printf("FetchHighlightTimelines accountID: %d, statusesCount: %d, firstCreatedAt: %s", account.ID, len(oHomeTimeline), oHomeTimeline[len(oHomeTimeline)-1].CreatedAt.Format(time.RFC3339Nano))
		} else {
			log.Printf("FetchHighlightTimelines accountID: %d, statusesCount: 0, firstCreatedAt: nil", account.ID)
		}

		// 元のホームタイムラインからハイライトとなる投稿を取得する
		highlightOStatuses := f.getHighlightOStatuses(oHomeTimeline)

		// 取得した投稿を登録または更新する
		for _, oStatus := range highlightOStatuses {
			local := !strings.Contains(oStatus.Account.Acct, "@")

			other := make(map[string]interface{})
			for k, v := range oStatus.Other {
				other[k] = v
			}
			other["account"] = convertMAccount(&oStatus.Account)
			other["application"] = convertMApplication(&oStatus.Application)

			var hasAttachedMedia bool
			if mediaAttachments, ok := other["media_attachments"].(map[string]interface{}); ok {
				hasAttachedMedia = len(mediaAttachments) > 0
			} else {
				hasAttachedMedia = false
			}

			_, err = f.statusRepository.MergeByOriginalID(ctx, account.ID, oStatus.ID, oStatus.CreatedAt, local, oStatus.Visibility, oStatus.ReblogsCount, oStatus.FavouritesCount, oStatus.RepliesCount, oStatus.InReplyToID, hasAttachedMedia, other)
			if err != nil {
				response := responses.NewFetchHighlightTimelinesError(err)
				f.outputBoundary.RespondError(ctx, response)
				return
			}
		}
	}

	response := responses.NewFetchHighlightTimelines()
	f.outputBoundary.Respond(ctx, response)
}

func (*FetchHighlightTimelines) calculateScore(oStatus *dto.OStatus) int32 {
	return oStatus.ReblogsCount + oStatus.FavouritesCount + oStatus.RepliesCount
}

func (f *FetchHighlightTimelines) getHighlightOStatuses(oStatuses []*dto.OStatus) []*dto.OStatus {
	highlightOStatuses := make([]*dto.OStatus, 0)
	for i := len(oStatuses) - 1; i >= 0; i-- {
		oStatus := oStatuses[i]
		if oStatus.Reblog != nil {
			oStatus = oStatus.Reblog
		}

		score := f.calculateScore(oStatus)
		if score < f.highlightScoreThreshold {
			continue
		}

		if f.hasOStatus(highlightOStatuses, oStatus.ID) {
			continue
		}

		highlightOStatuses = append(highlightOStatuses, oStatus)
	}
	return highlightOStatuses
}

func (*FetchHighlightTimelines) hasOStatus(oStatuses []*dto.OStatus, originalID int64) bool {
	for _, oStatus := range oStatuses {
		if oStatus.ID == originalID {
			return true
		}
	}
	return false
}
