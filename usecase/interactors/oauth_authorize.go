package interactors

import (
	"context"

	"gitlab.com/xianon/mashighlight/usecase/description_errors"
	"gitlab.com/xianon/mashighlight/usecase/output_boundaries"
	"gitlab.com/xianon/mashighlight/usecase/requests"
	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type OAuthAuthorize struct {
	outputBoundary output_boundaries.OAuthAuthorize
}

func NewOAuthAuthorize(outputBoundary output_boundaries.OAuthAuthorize) *OAuthAuthorize {
	return &OAuthAuthorize{
		outputBoundary: outputBoundary,
	}
}

func (a *OAuthAuthorize) HandleRead(ctx context.Context, request *requests.OAuthAuthorizeRead) {
	response := responses.NewOAuthAuthorizeRead(request.ResponseType, request.ClientID, request.RedirectURI, request.Scope, request.ForceLogin, request.Other)
	a.outputBoundary.RespondRead(ctx, response)
}

func (a *OAuthAuthorize) HandleError(ctx context.Context, request *requests.OAuthAuthorizeError) {
	err := description_errors.NewBadRequestError(request.Message)
	response := responses.NewOAuthAuthorizeError(err)
	a.outputBoundary.RespondError(ctx, response)
}
