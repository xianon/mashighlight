package interactors

import (
	"context"

	"gitlab.com/xianon/mashighlight/usecase/output_boundaries"
	"gitlab.com/xianon/mashighlight/usecase/requests"
	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type Redirect struct {
	mastodonHost   string
	outputBoundary output_boundaries.Redirect
}

func NewRedirect(outputBoundary output_boundaries.Redirect, mastodonHost string) *Redirect {
	return &Redirect{
		mastodonHost:   mastodonHost,
		outputBoundary: outputBoundary,
	}
}

func (d *Redirect) HandleRead(ctx context.Context, request *requests.RedirectRead) {
	url := "https://" + d.mastodonHost + request.RequestURI
	response := responses.NewRedirectRead(url)
	d.outputBoundary.RespondRead(ctx, response)
}
