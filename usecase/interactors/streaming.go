package interactors

import (
	"context"
	"errors"
	"io"
	"log"

	"gitlab.com/xianon/mashighlight/domain/repositories"
	"gitlab.com/xianon/mashighlight/usecase/description_errors"
	"gitlab.com/xianon/mashighlight/usecase/output_boundaries"
	"gitlab.com/xianon/mashighlight/usecase/query_services"
	"gitlab.com/xianon/mashighlight/usecase/requests"
	"gitlab.com/xianon/mashighlight/usecase/responses"
	"nhooyr.io/websocket"
)

type Streaming struct {
	accountRepository    repositories.Account
	oAccountQueryService query_services.OAccount
	outputBoundary       output_boundaries.Streaming
}

func NewStreaming(outputBoundary output_boundaries.Streaming, accountRepository repositories.Account, oAccountQueryService query_services.OAccount) *Streaming {
	return &Streaming{
		accountRepository:    accountRepository,
		oAccountQueryService: oAccountQueryService,
		outputBoundary:       outputBoundary,
	}
}

func (s *Streaming) HandleRead(ctx context.Context, request *requests.StreamingRead) {
	if request.Token != nil {
		oAccount, err := s.oAccountQueryService.VerifyCredentials(ctx, *request.Token)
		if err != nil {
			response := responses.NewStreamingError(err)
			s.outputBoundary.RespondError(ctx, response)
			return
		}

		s.accountRepository.UpdateLastAccessedAt(ctx, oAccount.ID)
	}

	response := responses.NewStreaming()
	streamingCloser, err := s.outputBoundary.Streaming(ctx, response)
	if err != nil {
		response := responses.NewStreamingError(err)
		s.outputBoundary.RespondError(ctx, response)
		return
	}
	defer streamingCloser.Close()
	log.Print("Streaming Upgrade")
	defer log.Print("Streaming Close")

	var cancel context.CancelFunc
	ctx, cancel = context.WithCancel(ctx)

	go func() {
		v := make(map[string]interface{})
		for {
			if err := streamingCloser.ReadJSON(ctx, &v); err != nil {
				if !errors.Is(err, io.EOF) && websocket.CloseStatus(err) < 0 {
					log.Printf("Streaming Read JSON Error: %s", err.Error())
				}
				cancel()
				break
			}
			log.Printf("Streaming Read JSON: %#v", v)
		}
	}()

	<-ctx.Done()
}

func (s *Streaming) HandleError(ctx context.Context, request *requests.StreamingError) {
	err := description_errors.NewBadRequestError(request.Message)
	response := responses.NewStreamingError(err)
	s.outputBoundary.RespondError(ctx, response)
}
