package interactors

import (
	"context"
	"strconv"
	"strings"

	"gitlab.com/xianon/mashighlight/domain/repositories"
	"gitlab.com/xianon/mashighlight/usecase/description_errors"
	"gitlab.com/xianon/mashighlight/usecase/output_boundaries"
	"gitlab.com/xianon/mashighlight/usecase/query_services"
	"gitlab.com/xianon/mashighlight/usecase/requests"
	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type UnfavouriteStatus struct {
	accountRepository    repositories.Account
	outputBoundary       output_boundaries.UnfavouriteStatus
	oAccountQueryService query_services.OAccount
	oStatusQueryService  query_services.OStatus
	statusRepository     repositories.Status
}

func NewUnfavouriteStatus(outputBoundary output_boundaries.UnfavouriteStatus, statusRepository repositories.Status, oStatusQueryService query_services.OStatus, oAccountQueryService query_services.OAccount, accountRepository repositories.Account) *UnfavouriteStatus {
	return &UnfavouriteStatus{
		accountRepository:    accountRepository,
		outputBoundary:       outputBoundary,
		oAccountQueryService: oAccountQueryService,
		oStatusQueryService:  oStatusQueryService,
		statusRepository:     statusRepository,
	}
}

func (u *UnfavouriteStatus) HandleCreate(ctx context.Context, request *requests.UnfavouriteStatusCreate) {
	if request.Token == nil {
		err := description_errors.NewUnauthenticatedError("アクセストークンが無効です")
		response := responses.NewUnfavouriteStatusError(err)
		u.outputBoundary.RespondError(ctx, response)
		return
	}

	oAccount, err := u.oAccountQueryService.VerifyCredentials(ctx, *request.Token)
	if err != nil {
		response := responses.NewUnfavouriteStatusError(err)
		u.outputBoundary.RespondError(ctx, response)
		return
	}

	u.accountRepository.UpdateLastAccessedAt(ctx, oAccount.ID)

	status, err := u.statusRepository.Find(ctx, request.ID)
	if err != nil {
		response := responses.NewUnfavouriteStatusError(err)
		u.outputBoundary.RespondError(ctx, response)
		return
	}

	oStatus, err := u.oStatusQueryService.Favourite(ctx, *request.Token, strconv.FormatInt(status.OriginalID, 10), false)
	if err != nil {
		response := responses.NewUnfavouriteStatusError(err)
		u.outputBoundary.RespondError(ctx, response)
		return
	}

	local := !strings.Contains(oStatus.Account.Acct, "@")

	other := make(map[string]interface{})
	for k, v := range oStatus.Other {
		other[k] = v
	}
	other["account"] = convertMAccount(&oStatus.Account)
	other["application"] = convertMApplication(&oStatus.Application)

	var hasAttachedMedia bool
	if mediaAttachments, ok := other["media_attachments"].(map[string]interface{}); ok {
		hasAttachedMedia = len(mediaAttachments) > 0
	} else {
		hasAttachedMedia = false
	}

	status, err = u.statusRepository.MergeByOriginalID(ctx, oAccount.ID, oStatus.ID, oStatus.CreatedAt, local, oStatus.Visibility, oStatus.ReblogsCount, oStatus.FavouritesCount, oStatus.RepliesCount, oStatus.InReplyToID, hasAttachedMedia, other)
	if err != nil {
		response := responses.NewUnfavouriteStatusError(err)
		u.outputBoundary.RespondError(ctx, response)
		return
	}

	response := responses.NewUnfavouriteStatusCreate(status)
	u.outputBoundary.RespondCreate(ctx, response)
}

func (u *UnfavouriteStatus) HandleError(ctx context.Context, request *requests.UnfavouriteStatusError) {
	err := description_errors.NewNotFoundError(request.Message)
	response := responses.NewUnfavouriteStatusError(err)
	u.outputBoundary.RespondError(ctx, response)
}
