package interactors

import (
	"context"

	"gitlab.com/xianon/mashighlight/domain/repositories"
	"gitlab.com/xianon/mashighlight/usecase/description_errors"
	"gitlab.com/xianon/mashighlight/usecase/output_boundaries"
	"gitlab.com/xianon/mashighlight/usecase/query_services"
	"gitlab.com/xianon/mashighlight/usecase/requests"
	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type HighlightTimeline struct {
	accountRepository    repositories.Account
	oAccountQueryService query_services.OAccount
	outputBoundary       output_boundaries.HighlightTimeline
	statusRepository     repositories.Status
}

func NewHighlightTimeline(outputBoundary output_boundaries.HighlightTimeline, accountRepository repositories.Account, oAccountQueryService query_services.OAccount, statusRepository repositories.Status) *HighlightTimeline {
	return &HighlightTimeline{
		accountRepository:    accountRepository,
		oAccountQueryService: oAccountQueryService,
		outputBoundary:       outputBoundary,
		statusRepository:     statusRepository,
	}
}

func (h *HighlightTimeline) HandleRead(ctx context.Context, request *requests.HighlightTimelineRead) {
	if request.Token == nil {
		err := description_errors.NewUnauthenticatedError("アクセストークンが無効です")
		response := responses.NewHighlightTimelineError(err)
		h.outputBoundary.RespondError(ctx, response)
		return
	}

	oAccount, err := h.oAccountQueryService.VerifyCredentials(ctx, *request.Token)
	if err != nil {
		response := responses.NewHighlightTimelineError(err)
		h.outputBoundary.RespondError(ctx, response)
		return
	}

	h.accountRepository.UpdateLastAccessedAt(ctx, oAccount.ID)

	statuses, err := h.statusRepository.FindHighlightTimeline(ctx, oAccount.ID, request.Local, request.MaxID, request.SinceID, request.MinID, request.Limit)
	if err != nil {
		response := responses.NewHighlightTimelineError(err)
		h.outputBoundary.RespondError(ctx, response)
		return
	}

	response := responses.NewHighlightTimelineRead(statuses, request.Local, request.Limit)
	h.outputBoundary.RespondRead(ctx, response)
}

func (h *HighlightTimeline) HandleError(ctx context.Context, request *requests.HighlightTimelineError) {
	err := description_errors.NewBadRequestError(request.Message)
	response := responses.NewHighlightTimelineError(err)
	h.outputBoundary.RespondError(ctx, response)
}
