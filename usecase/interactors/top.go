package interactors

import (
	"context"
	"net/url"

	"gitlab.com/xianon/mashighlight/domain/repositories"
	"gitlab.com/xianon/mashighlight/usecase/output_boundaries"
	"gitlab.com/xianon/mashighlight/usecase/requests"
	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type Top struct {
	domain             string
	instanceRepository repositories.Instance
	mastodonHost       string
	outputBoundary     output_boundaries.Top
}

func NewTop(outputBoundary output_boundaries.Top, instanceRepository repositories.Instance, domain string, mastodonHost string) *Top {
	return &Top{
		domain:             domain,
		instanceRepository: instanceRepository,
		mastodonHost:       mastodonHost,
		outputBoundary:     outputBoundary,
	}
}

func (t *Top) HandleIndex(ctx context.Context, request *requests.TopIndex) {
	var instanceName string
	var mastodonInstanceName string
	if instance, err := t.instanceRepository.Fetch(ctx); err == nil {
		instanceName = instance.Title + "ハイライト"
		mastodonInstanceName = instance.Title
	} else {
		instanceName = "ハイライト"
		mastodonInstanceName = t.mastodonHost
	}

	mastodonURL := &url.URL{
		Scheme: "https",
		Host:   t.mastodonHost,
		Path:   "/",
	}
	registerURL := &url.URL{
		Scheme: "https",
		Host:   t.domain,
		Path:   "/register",
	}

	response := responses.NewTopIndex(instanceName, mastodonURL.String(), mastodonInstanceName, registerURL.String())
	t.outputBoundary.RespondIndex(ctx, response)
}
