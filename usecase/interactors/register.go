package interactors

import (
	"context"
	"net/url"

	"gitlab.com/xianon/mashighlight/domain/repositories"
	"gitlab.com/xianon/mashighlight/usecase/description_errors"
	"gitlab.com/xianon/mashighlight/usecase/output_boundaries"
	"gitlab.com/xianon/mashighlight/usecase/query_services"
	"gitlab.com/xianon/mashighlight/usecase/requests"
	"gitlab.com/xianon/mashighlight/usecase/responses"
	"golang.org/x/oauth2"
)

type Register struct {
	applicationName          string
	applicationRepository    repositories.Application
	oApplicationQueryService query_services.OApplication
	domain                   string
	mastodonHost             string
	outputBoundary           output_boundaries.Register
}

func NewRegister(outputBoundary output_boundaries.Register, applicationRepository repositories.Application, oApplicationQueryService query_services.OApplication, domain string, mastodonHost string, applicationName string) *Register {
	return &Register{
		applicationName:          applicationName,
		applicationRepository:    applicationRepository,
		oApplicationQueryService: oApplicationQueryService,
		domain:                   domain,
		mastodonHost:             mastodonHost,
		outputBoundary:           outputBoundary,
	}
}

func (g *Register) HandleIndex(ctx context.Context, request *requests.RegisterIndex) {
	application, err := g.applicationRepository.Find(ctx)
	if err != nil {
		response := responses.NewRegisterError(err)
		g.outputBoundary.RespondError(ctx, response)
		return
	}

	if application == nil {
		redirectURI := &url.URL{
			Scheme: "https",
			Host:   g.domain,
			Path:   "/authorized",
		}
		scopes := "read:accounts read:statuses"
		websiteURL := &url.URL{
			Scheme: "https",
			Host:   g.domain,
			Path:   "/",
		}
		website := websiteURL.String()
		oApplication, err := g.oApplicationQueryService.Create(ctx, g.applicationName, redirectURI.String(), &scopes, &website)
		if err != nil {
			response := responses.NewRegisterError(err)
			g.outputBoundary.RespondError(ctx, response)
			return
		} else if oApplication.ClientID == nil || oApplication.ClientSecret == nil {
			err := description_errors.NewInteractionError("TODO")
			response := responses.NewRegisterError(err)
			g.outputBoundary.RespondError(ctx, response)
			return
		}

		application, err = g.applicationRepository.Merge(ctx, oApplication.Name, oApplication.Website, oApplication.VapidKey, *oApplication.ClientID, *oApplication.ClientSecret, oApplication.Other)
		if err != nil {
			response := responses.NewRegisterError(err)
			g.outputBoundary.RespondError(ctx, response)
			return
		}
	}

	authURL := &url.URL{
		Scheme: "https",
		Host:   g.mastodonHost,
		Path:   "/oauth/authorize",
	}
	redirectURL := &url.URL{
		Scheme: "https",
		Host:   g.domain,
		Path:   "/authorized",
	}
	conf := oauth2.Config{
		Endpoint: oauth2.Endpoint{
			AuthURL: authURL.String(),
		},
		ClientID:    application.ClientID,
		RedirectURL: redirectURL.String(),
		Scopes:      []string{"read:accounts", "read:statuses"},
	}

	authCodeURL := conf.AuthCodeURL("", oauth2.SetAuthURLParam("force_login", "true"))

	response := responses.NewRegisterIndex(authCodeURL)
	g.outputBoundary.RespondIndex(ctx, response)
}
