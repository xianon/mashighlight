package interactors

import (
	"context"

	"gitlab.com/xianon/mashighlight/domain/repositories"
	"gitlab.com/xianon/mashighlight/usecase/description_errors"
	"gitlab.com/xianon/mashighlight/usecase/output_boundaries"
	"gitlab.com/xianon/mashighlight/usecase/query_services"
	"gitlab.com/xianon/mashighlight/usecase/requests"
	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type Status struct {
	accountRepository    repositories.Account
	outputBoundary       output_boundaries.Status
	oAccountQueryService query_services.OAccount
	statusRepository     repositories.Status
}

func NewStatus(outputBoundary output_boundaries.Status, statusRepository repositories.Status, oAccountQueryService query_services.OAccount, accountRepository repositories.Account) *Status {
	return &Status{
		accountRepository:    accountRepository,
		outputBoundary:       outputBoundary,
		oAccountQueryService: oAccountQueryService,
		statusRepository:     statusRepository,
	}
}

func (s *Status) HandleRead(ctx context.Context, request *requests.StatusRead) {
	if request.Token == nil {
		err := description_errors.NewUnauthenticatedError("アクセストークンが無効です")
		response := responses.NewStatusError(err)
		s.outputBoundary.RespondError(ctx, response)
		return
	}

	oAccount, err := s.oAccountQueryService.VerifyCredentials(ctx, *request.Token)
	if err != nil {
		response := responses.NewStatusError(err)
		s.outputBoundary.RespondError(ctx, response)
		return
	}

	s.accountRepository.UpdateLastAccessedAt(ctx, oAccount.ID)

	status, err := s.statusRepository.Find(ctx, request.ID)
	if err != nil {
		response := responses.NewStatusError(err)
		s.outputBoundary.RespondError(ctx, response)
		return
	} else if status == nil {
		err := description_errors.NewNotFoundError("Record not found")
		response := responses.NewStatusError(err)
		s.outputBoundary.RespondError(ctx, response)
		return
	}

	response := responses.NewStatusRead(status)
	s.outputBoundary.RespondRead(ctx, response)
}

func (s *Status) HandleError(ctx context.Context, request *requests.StatusError) {
	err := description_errors.NewNotFoundError(request.Message)
	response := responses.NewStatusError(err)
	s.outputBoundary.RespondError(ctx, response)
}
