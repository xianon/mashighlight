package interactors

import "gitlab.com/xianon/mashighlight/usecase/dto"

func convertMApplication(oApplication *dto.OApplication) map[string]interface{} {
	mApplication := map[string]interface{}{
		"name":          oApplication.Name,
		"website":       oApplication.Website,
		"vapid_key":     oApplication.VapidKey,
		"client_id":     oApplication.ClientID,
		"client_secret": oApplication.ClientSecret,
	}
	for k, v := range oApplication.Other {
		mApplication[k] = v
	}
	return mApplication
}
