package interactors

import (
	"context"

	"gitlab.com/xianon/mashighlight/domain/repositories"
	"gitlab.com/xianon/mashighlight/usecase/description_errors"
	"gitlab.com/xianon/mashighlight/usecase/output_boundaries"
	"gitlab.com/xianon/mashighlight/usecase/requests"
	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type OAuthToken struct {
	accessTokenRepository repositories.AccessToken
	outputBoundary        output_boundaries.OAuthToken
}

func NewOAuthToken(outputBoundary output_boundaries.OAuthToken, accessTokenRepository repositories.AccessToken) *OAuthToken {
	return &OAuthToken{
		accessTokenRepository: accessTokenRepository,
		outputBoundary:        outputBoundary,
	}
}

func (t *OAuthToken) HandleCreate(ctx context.Context, request *requests.OAuthTokenCreate) {
	if request.GrantType != "client_credentials" && request.RedirectURI == nil {
		err := description_errors.NewBadRequestError("redirect_uri が指定されていません")
		response := responses.NewOAuthTokenError(err)
		t.outputBoundary.RespondError(ctx, response)
		return
	}

	accessToken, err := t.accessTokenRepository.Obtain(ctx, request.GrantType, request.ClientID, request.ClientSecret, request.RedirectURI, request.Scopes, request.Code)
	if err != nil {
		response := responses.NewOAuthTokenError(err)
		t.outputBoundary.RespondError(ctx, response)
		return
	}

	response := responses.NewOAuthTokenCreate(accessToken)
	t.outputBoundary.RespondCreate(ctx, response)
}

func (t *OAuthToken) HandleError(ctx context.Context, request *requests.OAuthTokenError) {
	err := description_errors.NewBadRequestError(request.Message)
	response := responses.NewOAuthTokenError(err)
	t.outputBoundary.RespondError(ctx, response)
}
