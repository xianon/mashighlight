package interactors

import (
	"context"
	"strconv"
	"strings"

	"gitlab.com/xianon/mashighlight/domain/repositories"
	"gitlab.com/xianon/mashighlight/usecase/description_errors"
	"gitlab.com/xianon/mashighlight/usecase/output_boundaries"
	"gitlab.com/xianon/mashighlight/usecase/query_services"
	"gitlab.com/xianon/mashighlight/usecase/requests"
	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type FavouriteStatus struct {
	accountRepository    repositories.Account
	outputBoundary       output_boundaries.FavouriteStatus
	oAccountQueryService query_services.OAccount
	oStatusQueryService  query_services.OStatus
	statusRepository     repositories.Status
}

func NewFavouriteStatus(outputBoundary output_boundaries.FavouriteStatus, statusRepository repositories.Status, oStatusQueryService query_services.OStatus, oAccountQueryService query_services.OAccount, accountRepository repositories.Account) *FavouriteStatus {
	return &FavouriteStatus{
		accountRepository:    accountRepository,
		outputBoundary:       outputBoundary,
		oAccountQueryService: oAccountQueryService,
		oStatusQueryService:  oStatusQueryService,
		statusRepository:     statusRepository,
	}
}

func (f *FavouriteStatus) HandleCreate(ctx context.Context, request *requests.FavouriteStatusCreate) {
	if request.Token == nil {
		err := description_errors.NewUnauthenticatedError("アクセストークンが無効です")
		response := responses.NewFavouriteStatusError(err)
		f.outputBoundary.RespondError(ctx, response)
		return
	}

	oAccount, err := f.oAccountQueryService.VerifyCredentials(ctx, *request.Token)
	if err != nil {
		response := responses.NewFavouriteStatusError(err)
		f.outputBoundary.RespondError(ctx, response)
		return
	}

	f.accountRepository.UpdateLastAccessedAt(ctx, oAccount.ID)

	status, err := f.statusRepository.Find(ctx, request.ID)
	if err != nil {
		response := responses.NewFavouriteStatusError(err)
		f.outputBoundary.RespondError(ctx, response)
		return
	}

	oStatus, err := f.oStatusQueryService.Favourite(ctx, *request.Token, strconv.FormatInt(status.OriginalID, 10), true)
	if err != nil {
		response := responses.NewFavouriteStatusError(err)
		f.outputBoundary.RespondError(ctx, response)
		return
	}

	local := !strings.Contains(oStatus.Account.Acct, "@")

	other := make(map[string]interface{})
	for k, v := range oStatus.Other {
		other[k] = v
	}
	other["account"] = convertMAccount(&oStatus.Account)
	other["application"] = convertMApplication(&oStatus.Application)

	var hasAttachedMedia bool
	if mediaAttachments, ok := other["media_attachments"].(map[string]interface{}); ok {
		hasAttachedMedia = len(mediaAttachments) > 0
	} else {
		hasAttachedMedia = false
	}

	status, err = f.statusRepository.MergeByOriginalID(ctx, oAccount.ID, oStatus.ID, oStatus.CreatedAt, local, oStatus.Visibility, oStatus.ReblogsCount, oStatus.FavouritesCount, oStatus.RepliesCount, oStatus.InReplyToID, hasAttachedMedia, other)
	if err != nil {
		response := responses.NewFavouriteStatusError(err)
		f.outputBoundary.RespondError(ctx, response)
		return
	}

	response := responses.NewFavouriteStatusCreate(status)
	f.outputBoundary.RespondCreate(ctx, response)
}

func (f *FavouriteStatus) HandleError(ctx context.Context, request *requests.FavouriteStatusError) {
	err := description_errors.NewNotFoundError(request.Message)
	response := responses.NewFavouriteStatusError(err)
	f.outputBoundary.RespondError(ctx, response)
}
