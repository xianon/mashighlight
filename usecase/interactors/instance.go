package interactors

import (
	"context"
	"net/url"

	"gitlab.com/xianon/mashighlight/domain/repositories"
	"gitlab.com/xianon/mashighlight/usecase/output_boundaries"
	"gitlab.com/xianon/mashighlight/usecase/requests"
	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type Instance struct {
	domain             string
	instanceRepository repositories.Instance
	outputBoundary     output_boundaries.Instance
}

func NewInstance(outputBoundary output_boundaries.Instance, instanceRepository repositories.Instance, domain string) *Instance {
	return &Instance{
		domain:             domain,
		instanceRepository: instanceRepository,
		outputBoundary:     outputBoundary,
	}
}

func (i *Instance) HandleRead(ctx context.Context, request *requests.InstanceRead) {
	instance, err := i.instanceRepository.Fetch(ctx)
	if err != nil {
		response := responses.NewInstanceError(err)
		i.outputBoundary.RespondError(ctx, response)
		return
	}

	instance.URI = i.domain
	instance.Title += "ハイライト"

	if streamingAPIURL, err := url.Parse(instance.URLs.StreamingAPI); err == nil {
		streamingAPIURL.Host = i.domain
		instance.URLs.StreamingAPI = streamingAPIURL.String()
	} else {
		instance.URLs.StreamingAPI = ""
	}

	response := responses.NewInstanceRead(instance)
	i.outputBoundary.RespondRead(ctx, response)
}
