package interactors

import (
	"context"

	"gitlab.com/xianon/mashighlight/domain/repositories"
	"gitlab.com/xianon/mashighlight/usecase/description_errors"
	"gitlab.com/xianon/mashighlight/usecase/output_boundaries"
	"gitlab.com/xianon/mashighlight/usecase/query_services"
	"gitlab.com/xianon/mashighlight/usecase/requests"
	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type EmptyList struct {
	accountRepository    repositories.Account
	oAccountQueryService query_services.OAccount
	outputBoundary       output_boundaries.EmptyList
}

func NewEmptyList(outputBoundary output_boundaries.EmptyList, accountRepository repositories.Account, oAccountQueryService query_services.OAccount) *EmptyList {
	return &EmptyList{
		accountRepository:    accountRepository,
		oAccountQueryService: oAccountQueryService,
		outputBoundary:       outputBoundary,
	}
}

func (e *EmptyList) HandleRead(ctx context.Context, request *requests.EmptyListRead) {
	if request.NeedsAuthentication {
		if request.Token == nil {
			err := description_errors.NewUnauthenticatedError("アクセストークンが無効です")
			response := responses.NewEmptyListError(err)
			e.outputBoundary.RespondError(ctx, response)
			return
		}

		oAccount, err := e.oAccountQueryService.VerifyCredentials(ctx, *request.Token)
		if err != nil {
			response := responses.NewEmptyListError(err)
			e.outputBoundary.RespondError(ctx, response)
			return
		}

		e.accountRepository.UpdateLastAccessedAt(ctx, oAccount.ID)
	}

	response := responses.NewEmptyListRead()
	e.outputBoundary.RespondRead(ctx, response)
}
