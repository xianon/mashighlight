package interactors

import (
	"context"

	"gitlab.com/xianon/mashighlight/domain/repositories"
	"gitlab.com/xianon/mashighlight/usecase/output_boundaries"
	"gitlab.com/xianon/mashighlight/usecase/requests"
	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type RemoveOldStatuses struct {
	days             int
	outputBoundary   output_boundaries.RemoveOldStatuses
	statusRepository repositories.Status
}

func NewRemoveOldStatuses(outputBoundary output_boundaries.RemoveOldStatuses, statusRepository repositories.Status, days int) *RemoveOldStatuses {
	return &RemoveOldStatuses{
		days:             days,
		outputBoundary:   outputBoundary,
		statusRepository: statusRepository,
	}
}

func (r *RemoveOldStatuses) Handle(ctx context.Context, request *requests.RemoveOldStatuses) {
	// 古い投稿を削除する
	err := r.statusRepository.DeleteOlder(ctx, r.days)
	if err != nil {
		response := responses.NewRemoveOldStatusesError(err)
		r.outputBoundary.RespondError(ctx, response)
		return
	}

	response := responses.NewRemoveOldStatuses()
	r.outputBoundary.Respond(ctx, response)
}
