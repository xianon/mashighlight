package interactors

import (
	"context"
	"net/url"

	"gitlab.com/xianon/mashighlight/domain/repositories"
	"gitlab.com/xianon/mashighlight/usecase/description_errors"
	"gitlab.com/xianon/mashighlight/usecase/output_boundaries"
	"gitlab.com/xianon/mashighlight/usecase/query_services"
	"gitlab.com/xianon/mashighlight/usecase/requests"
	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type Authorized struct {
	accessTokenRepository repositories.AccessToken
	accountRepository     repositories.Account
	applicationRepository repositories.Application
	domain                string
	instanceRepository    repositories.Instance
	oAccountQueryService  query_services.OAccount
	outputBoundary        output_boundaries.Authorized
}

func NewAuthorized(outputBoundary output_boundaries.Authorized, applicationRepository repositories.Application, accessTokenRepository repositories.AccessToken, oAccountQueryService query_services.OAccount, accountRepository repositories.Account, instanceRepository repositories.Instance, domain string) *Authorized {
	return &Authorized{
		accessTokenRepository: accessTokenRepository,
		accountRepository:     accountRepository,
		applicationRepository: applicationRepository,
		domain:                domain,
		instanceRepository:    instanceRepository,
		oAccountQueryService:  oAccountQueryService,
		outputBoundary:        outputBoundary,
	}
}

func (a *Authorized) HandleIndex(ctx context.Context, request *requests.AuthorizedIndex) {
	application, err := a.applicationRepository.Find(ctx)
	if err != nil {
		response := responses.NewAuthorizedError(err)
		a.outputBoundary.RespondError(ctx, response)
		return
	} else if application == nil {
		err := description_errors.NewInteractionError("We're sorry, but something went wrong on our end.")
		response := responses.NewAuthorizedError(err)
		a.outputBoundary.RespondError(ctx, response)
		return
	}

	const grantType = "authorization_code"

	redirectURI := &url.URL{
		Scheme: "https",
		Host:   a.domain,
		Path:   "/authorized",
	}
	redirectURIStr := redirectURI.String()

	scopes := []string{
		"read:accounts",
		"read:statuses",
	}

	accessToken, err := a.accessTokenRepository.Obtain(ctx, grantType, application.ClientID, application.ClientSecret, &redirectURIStr, scopes, &request.Code)
	if err != nil {
		response := responses.NewAuthorizedError(err)
		a.outputBoundary.RespondError(ctx, response)
		return
	}

	oAccount, err := a.oAccountQueryService.VerifyCredentials(ctx, accessToken.AccessToken)
	if err != nil {
		response := responses.NewAuthorizedError(err)
		a.outputBoundary.RespondError(ctx, response)
		return
	}

	_, err = a.accountRepository.Merge(ctx, oAccount.ID, &accessToken.AccessToken)
	if err != nil {
		response := responses.NewAuthorizedError(err)
		a.outputBoundary.RespondError(ctx, response)
		return
	}

	var instanceName string
	if instance, err := a.instanceRepository.Fetch(ctx); err == nil {
		instanceName = instance.Title + "ハイライト"
	} else {
		instanceName = "ハイライト"
	}

	response := responses.NewAuthorizedIndex(instanceName, a.domain)
	a.outputBoundary.RespondIndex(ctx, response)
}

func (a *Authorized) HandleError(ctx context.Context, request *requests.AuthorizedError) {
	err := description_errors.NewNotFoundError(request.Message)
	response := responses.NewAuthorizedError(err)
	a.outputBoundary.RespondError(ctx, response)
}
