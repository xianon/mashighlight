package interactors

import (
	"bytes"
	"context"
	"io"
	"net/http"
	"strconv"
	"strings"

	"gitlab.com/xianon/mashighlight/usecase/description_errors"
	"gitlab.com/xianon/mashighlight/usecase/output_boundaries"
	"gitlab.com/xianon/mashighlight/usecase/requests"
	"gitlab.com/xianon/mashighlight/usecase/responses"
)

type Intermediation struct {
	mastodonHost   string
	outputBoundary output_boundaries.Intermediation
}

func NewIntermediation(outputBoundary output_boundaries.Intermediation, mastodonHost string) *Intermediation {
	return &Intermediation{
		mastodonHost:   mastodonHost,
		outputBoundary: outputBoundary,
	}
}

func (i *Intermediation) HandleCreate(ctx context.Context, request *requests.IntermediationCreate) {
	url := "https://" + i.mastodonHost + request.RequestURI
	ireq, err := i.intermediate(ctx, "POST", url, request.Token, request.ContentType, request.Body)
	if err != nil {
		response := responses.NewIntermediationError(err)
		i.outputBoundary.RespondError(ctx, response)
		return
	}

	response := responses.NewIntermediationCreate(ireq.StatusCode, ireq.CacheControl, ireq.Age, ireq.Pragma, ireq.Vary, ireq.Etag, ireq.ContentType, ireq.Body)
	i.outputBoundary.RespondCreate(ctx, response)
}

func (i *Intermediation) HandleRead(ctx context.Context, request *requests.IntermediationRead) {
	url := "https://" + i.mastodonHost + request.RequestURI
	ireq, err := i.intermediate(ctx, "GET", url, request.Token, request.ContentType, request.Body)
	if err != nil {
		response := responses.NewIntermediationError(err)
		i.outputBoundary.RespondError(ctx, response)
		return
	}

	response := responses.NewIntermediationRead(ireq.StatusCode, ireq.CacheControl, ireq.Age, ireq.Pragma, ireq.Vary, ireq.Etag, ireq.ContentType, ireq.Body)
	i.outputBoundary.RespondRead(ctx, response)
}

func (i *Intermediation) HandleError(ctx context.Context, request *requests.IntermediationError) {
	err := description_errors.NewBadRequestError(request.Message)
	response := responses.NewIntermediationError(err)
	i.outputBoundary.RespondError(ctx, response)
}

type intermediationResponse struct {
	StatusCode   int
	CacheControl *string
	Age          *int
	Pragma       *string
	Vary         *string
	Etag         *string
	ContentType  *string
	Body         []byte
}

func (*Intermediation) intermediate(ctx context.Context, method string, url string, token *string, contentType *string, body []byte) (*intermediationResponse, error) {
	req, err := http.NewRequestWithContext(ctx, method, url, bytes.NewReader(body))
	if err != nil {
		// TODO: urlがおかしい場合
		return nil, err
	}

	if token != nil {
		req.Header.Set("Authorization", "Bearer "+*token)
	}
	if contentType != nil {
		req.Header.Set("Content-Type", *contentType)
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		// TODO:
		return nil, err
	}

	resBody, err := io.ReadAll(resp.Body)
	if err != nil {
		// TODO:
		return nil, err
	}
	defer resp.Body.Close()

	ires := &intermediationResponse{
		StatusCode: resp.StatusCode,
		Body:       resBody,
	}

	if cacheControlValues := resp.Header.Values("Cache-Control"); len(cacheControlValues) > 0 {
		cacheControl := strings.Join(cacheControlValues, ", ")
		ires.CacheControl = &cacheControl
	}
	if ageValues := resp.Header.Values("Age"); len(ageValues) > 0 {
		if age, err := strconv.Atoi(ageValues[0]); err == nil {
			ires.Age = &age
		}
	}
	if pragmaValues := resp.Header.Values("Pragma"); len(pragmaValues) > 0 {
		ires.Pragma = &pragmaValues[0]
	}
	if varyValues := resp.Header.Values("Vary"); len(varyValues) > 0 {
		vary := strings.Join(varyValues, ", ")
		ires.Vary = &vary
	}
	if etagValues := resp.Header.Values("Etag"); len(etagValues) > 0 {
		etag := strings.Join(etagValues, ", ")
		ires.Etag = &etag
	}
	if contentTypeValues := resp.Header.Values("Content-Type"); len(contentTypeValues) > 0 {
		contentType := strings.Join(contentTypeValues, ", ")
		ires.ContentType = &contentType
	}

	return ires, nil
}
