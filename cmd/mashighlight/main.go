package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"syscall"
	"time"

	"entgo.io/ent/dialect"
	"entgo.io/ent/dialect/sql/schema"
	"gitlab.com/xianon/mashighlight/config"
	"gitlab.com/xianon/mashighlight/domain/repositories"
	"gitlab.com/xianon/mashighlight/ent"
	"gitlab.com/xianon/mashighlight/ent/migrate"
	"gitlab.com/xianon/mashighlight/infrastructure/database"
	"gitlab.com/xianon/mashighlight/infrastructure/rest_api"
	"gitlab.com/xianon/mashighlight/infrastructure/router"
	"gitlab.com/xianon/mashighlight/interface/controllers"
	"gitlab.com/xianon/mashighlight/interface/presenters"
	"gitlab.com/xianon/mashighlight/interface/views"
	"gitlab.com/xianon/mashighlight/usecase/input_boundaries"
	"gitlab.com/xianon/mashighlight/usecase/interactors"
	"gitlab.com/xianon/mashighlight/usecase/output_boundaries"
	"gitlab.com/xianon/mashighlight/usecase/query_services"
	"golang.org/x/sync/errgroup"

	_ "github.com/lib/pq"
	_ "github.com/mattn/go-sqlite3"
)

func main() {
	conf, err := config.New()
	if err != nil {
		log.Fatal(err)
	}

	client, err := newEntClient(conf.DatabaseURL)
	if err != nil {
		log.Fatal(err)
	}
	defer client.Close()

	ctx := context.Background()
	if err := entMigrate(ctx, client); err != nil {
		log.Fatalf("failed creating schema resources: %v", err)
	}

	accountDatabase := newAccountDatabase(client)
	applicationDatabase := newApplicationDatabase(client)
	statusDatabase := newStatusDatabase(client)

	accessTokenRestAPI := newAccessTokenRestAPI(conf.MastodonHost)
	instanceRestAPI := newInstanceRestAPI(conf.MastodonHost)
	oAccountRestAPI := newOAccountRestAPI(conf.MastodonHost)
	oApplicationRestAPI := newOApplicationRestAPI(conf.MastodonHost)
	oHomeTimelineRestAPI := newOHomeTimelineRestAPI(conf.MastodonHost)
	oStatusRestAPI := newOStatusRestAPI(conf.MastodonHost)

	topView, err := newTopView()
	if err != nil {
		log.Fatal(err)
	}
	topPresenter := newTopPresenter(topView)
	topInteractor := newTopInteractor(topPresenter, instanceRestAPI, conf.Domain, conf.MastodonHost)
	topController := newTopController(topInteractor)

	registerPresenter := newRegisterPresenter()
	registerInteractor := newRegisterInteractor(registerPresenter, applicationDatabase, oApplicationRestAPI, conf.Domain, conf.MastodonHost, conf.ApplicationName)
	registerController := newRegisterController(registerInteractor)

	authorizedView, err := newAuthorizedView()
	if err != nil {
		log.Fatal(err)
	}
	authorizedPresenter := newAuthorizedPresenter(authorizedView)
	authorizedInteractor := newAuthorizedInteractor(authorizedPresenter, applicationDatabase, accessTokenRestAPI, oAccountRestAPI, accountDatabase, instanceRestAPI, conf.Domain)
	authorizedController := newAuthorizedController(authorizedInteractor)

	oauthAuthorizePresenter := newOAuthAuthorizePresenter(conf.MastodonHost)
	oauthAuthorizeInteractor := newOAuthAuthorizeInteractor(oauthAuthorizePresenter)
	oauthAuthorizeController := newOAuthAuthorizeController(oauthAuthorizeInteractor)

	intermediationPresenter := newIntermediationPresenter()
	intermediationInteractor := newIntermediationInteractor(intermediationPresenter, conf.MastodonHost)
	intermediationController := newIntermediationController(intermediationInteractor)

	oAuthTokenPresenter := newOAuthTokenPresenter()
	oAuthTokenInteractor := newOAuthTokenInteractor(oAuthTokenPresenter, accessTokenRestAPI)
	oAuthTokenController := newOAuthTokenController(oAuthTokenInteractor)

	highlightTimelinePresenter := newHighlightTimelinePresenter(conf.MastodonHost)
	highlightTimelineInteractor := newHighlightTimelineInteractor(highlightTimelinePresenter, accountDatabase, oAccountRestAPI, statusDatabase)
	highlightTimelineController := newHighlightTimelineController(highlightTimelineInteractor)

	publicTimelinePresenter := newPublicTimelinePresenter(conf.MastodonHost)
	publicTimelineInteractor := newPublicTimelineInteractor(publicTimelinePresenter, accountDatabase, oAccountRestAPI, statusDatabase)
	publicTimelineController := newPublicTimelineController(publicTimelineInteractor)

	accountPresenter := newAccountPresenter()
	accountInteractor := newAccountInteractor(accountPresenter, accountDatabase, oAccountRestAPI)
	accountController := newAccountController(accountInteractor)

	statusPresenter := newStatusPresenter()
	statusInteractor := newStatusInteractor(statusPresenter, statusDatabase, oAccountRestAPI, accountDatabase)
	statusController := newStatusController(statusInteractor)

	favouriteStatusPresenter := newFavouriteStatusPresenter()
	favouriteStatusInteractor := newFavouriteStatusInteractor(favouriteStatusPresenter, statusDatabase, oStatusRestAPI, oAccountRestAPI, accountDatabase)
	favouriteStatusController := newFavouriteStatusController(favouriteStatusInteractor)

	unfavouriteStatusPresenter := newUnfavouriteStatusPresenter()
	unfavouriteStatusInteractor := newUnfavouriteStatusInteractor(unfavouriteStatusPresenter, statusDatabase, oStatusRestAPI, oAccountRestAPI, accountDatabase)
	unfavouriteStatusController := newUnfavouriteStatusController(unfavouriteStatusInteractor)

	streamingPresenter := newStreamingPresenter()
	streamingInteractor := newStreamingInteractor(streamingPresenter, accountDatabase, oAccountRestAPI)
	streamingController := newStreamingController(streamingInteractor)

	instancePresenter := newInstancePresenter()
	instanceInteractor := newInstanceInteractor(instancePresenter, instanceRestAPI, conf.Domain)
	instanceController := newInstanceController(instanceInteractor)

	emptyListPresenter := newEmptyListPresenter()
	emptyListInteractor := newEmptyListInteractor(emptyListPresenter, accountDatabase, oAccountRestAPI)
	emptyListController := newEmptyListController(emptyListInteractor)

	rt := newRouter(topController, registerController, authorizedController, oauthAuthorizeController, intermediationController, oAuthTokenController, highlightTimelineController, publicTimelineController, accountController, statusController, favouriteStatusController, unfavouriteStatusController, streamingController, instanceController, emptyListController)

	fetchHighlightTimelinesPresenter := newFetchHighlightTimelinesPresenter()
	fetchHighlightTimelinesInteractor := newFetchHighlightTimelinesInteractor(fetchHighlightTimelinesPresenter, accountDatabase, oHomeTimelineRestAPI, statusDatabase, conf.HighlightScoreThreshold)
	fetchHighlightTimelinesController := newFetchHighlightTimelinesController(fetchHighlightTimelinesInteractor)

	removeOldStatusesPresenter := newRemoveOldStatusesPresenter()
	removeOldStatusesInteractor := newRemoveOldStatusesInteractor(removeOldStatusesPresenter, statusDatabase, conf.StatusRemoveDays)
	removeOldStatusesController := newRemoveOldStatusesController(removeOldStatusesInteractor)

	log.Println("Server start")

	baseCtx, stop := signal.NotifyContext(ctx, os.Interrupt, syscall.SIGTERM)
	defer stop()

	srv := &http.Server{
		Addr:    fmt.Sprintf(":%d", conf.Port),
		Handler: rt,
		BaseContext: func(_ net.Listener) context.Context {
			return baseCtx
		},
	}

	g, gCtx := errgroup.WithContext(baseCtx)
	g.Go(func() error {
		return listenAndServe(srv)
	})
	g.Go(func() error {
		<-gCtx.Done()
		return shutdownServer(srv)
	})
	g.Go(func() error {
		runTicker(gCtx, time.Hour, func(ctx context.Context) {
			fetchHighlightTimelinesController.Fetch(ctx)
			removeOldStatusesController.Remove(ctx)
		})
		return nil
	})
	if err := g.Wait(); err != nil {
		log.Fatal(err)
	}

	log.Println("Server shutdown")
}

func newEntClient(databaseURL url.URL) (*ent.Client, error) {
	driverName := databaseURL.Scheme
	var dataSourceName string
	switch driverName {
	case dialect.Postgres:
		query := databaseURL.Query()
		if !query.Has("sslmode") {
			query.Set("sslmode", "disable")
		}
		databaseURL.RawQuery = query.Encode()
		dataSourceName = databaseURL.String()
	case dialect.SQLite:
		databaseURL.Scheme = ""
		databaseURL.Opaque = ""
		databaseURL.User = nil
		databaseURL.Host = ""
		query := databaseURL.Query()
		if !query.Has("_fk") && !query.Has("_foreign_keys") {
			query.Set("_foreign_keys", "1")
		}
		databaseURL.RawQuery = query.Encode()
		fileQuery := databaseURL.String()
		if fileQuery != "" && fileQuery[0] == '/' {
			fileQuery = fileQuery[1:]
		}
		dataSourceName = "file:" + fileQuery
	default:
		dataSourceName = databaseURL.String()
	}
	return ent.Open(driverName, dataSourceName)
}

func entMigrate(ctx context.Context, client *ent.Client) error {
	migrateOptions := []schema.MigrateOption{
		migrate.WithDropIndex(true),
		migrate.WithDropColumn(true),
	}
	return client.Schema.Create(ctx, migrateOptions...)
}

func newApplicationDatabase(client *ent.Client) *database.Application {
	return database.NewApplication(client)
}

func newOApplicationRestAPI(mastodonHost string) *rest_api.OApplication {
	return rest_api.NewOApplication(mastodonHost)
}

func newTopView() (*views.Top, error) {
	return views.NewTop()
}

func newTopPresenter(view *views.Top) *presenters.Top {
	return presenters.NewTop(view)
}

func newTopInteractor(outputBoundary output_boundaries.Top, instanceRepository repositories.Instance, domain string, mastodonHost string) *interactors.Top {
	return interactors.NewTop(outputBoundary, instanceRepository, domain, mastodonHost)
}

func newTopController(inputBoundary input_boundaries.Top) *controllers.Top {
	return controllers.NewTop(inputBoundary)
}

func newRegisterPresenter() *presenters.Register {
	return presenters.NewRegister()
}

func newRegisterInteractor(outputBoundary output_boundaries.Register, applicationRepository repositories.Application, oApplicationQueryService query_services.OApplication, domain string, mastodonHost string, applicationName string) *interactors.Register {
	return interactors.NewRegister(outputBoundary, applicationRepository, oApplicationQueryService, domain, mastodonHost, applicationName)
}

func newRegisterController(inputBoundary input_boundaries.Register) *controllers.Register {
	return controllers.NewRegister(inputBoundary)
}

func newAuthorizedView() (*views.Authorized, error) {
	return views.NewAuthorized()
}

func newAuthorizedPresenter(view *views.Authorized) *presenters.Authorized {
	return presenters.NewAuthorized(view)
}

func newAuthorizedInteractor(outputBoundary output_boundaries.Authorized, applicationRepository repositories.Application, accessTokenRepository repositories.AccessToken, oAccountQueryService query_services.OAccount, accountRepository repositories.Account, instanceRepository repositories.Instance, domain string) *interactors.Authorized {
	return interactors.NewAuthorized(outputBoundary, applicationRepository, accessTokenRepository, oAccountQueryService, accountRepository, instanceRepository, domain)
}

func newAuthorizedController(inputBoundary input_boundaries.Authorized) *controllers.Authorized {
	return controllers.NewAuthorized(inputBoundary)
}

func newOAuthAuthorizePresenter(mastodonHost string) *presenters.OAuthAuthorize {
	return presenters.NewOAuthAuthorize(mastodonHost)
}

func newOAuthAuthorizeInteractor(outputBoundary output_boundaries.OAuthAuthorize) *interactors.OAuthAuthorize {
	return interactors.NewOAuthAuthorize(outputBoundary)
}

func newOAuthAuthorizeController(inputBoundary input_boundaries.OAuthAuthorize) *controllers.OAuthAuthorize {
	return controllers.NewOAuthAuthorize(inputBoundary)
}

func newIntermediationPresenter() *presenters.Intermediation {
	return presenters.NewIntermediation()
}

func newIntermediationInteractor(outputBoundary output_boundaries.Intermediation, mastodonHost string) *interactors.Intermediation {
	return interactors.NewIntermediation(outputBoundary, mastodonHost)
}

func newIntermediationController(inputBoundary input_boundaries.Intermediation) *controllers.Intermediation {
	return controllers.NewIntermediation(inputBoundary)
}

func newAccessTokenRestAPI(mastodonHost string) *rest_api.AccessToken {
	return rest_api.NewAccessToken(mastodonHost)
}

func newOAccountRestAPI(mastodonHost string) *rest_api.OAccount {
	return rest_api.NewOAccount(mastodonHost)
}

func newAccountDatabase(client *ent.Client) *database.Account {
	return database.NewAccount(client)
}

func newOAuthTokenPresenter() *presenters.OAuthToken {
	return presenters.NewOAuthToken()
}

func newOAuthTokenInteractor(outputBoundary output_boundaries.OAuthToken, accessTokenRepository repositories.AccessToken) *interactors.OAuthToken {
	return interactors.NewOAuthToken(outputBoundary, accessTokenRepository)
}

func newOAuthTokenController(inputBoundary input_boundaries.OAuthToken) *controllers.OAuthToken {
	return controllers.NewOAuthToken(inputBoundary)
}

func newStatusDatabase(client *ent.Client) *database.Status {
	return database.NewStatus(client)
}

func newHighlightTimelinePresenter(mastodonHost string) *presenters.HighlightTimeline {
	return presenters.NewHighlightTimeline(mastodonHost)
}

func newHighlightTimelineInteractor(outputBoundary output_boundaries.HighlightTimeline, accountRepository repositories.Account, oAccountQueryService query_services.OAccount, statusRepository repositories.Status) *interactors.HighlightTimeline {
	return interactors.NewHighlightTimeline(outputBoundary, accountRepository, oAccountQueryService, statusRepository)
}

func newHighlightTimelineController(inputBoundary input_boundaries.HighlightTimeline) *controllers.HighlightTimeline {
	return controllers.NewHighlightTimeline(inputBoundary)
}

func newPublicTimelinePresenter(mastodonHost string) *presenters.PublicTimeline {
	return presenters.NewPublicTimeline(mastodonHost)
}

func newPublicTimelineInteractor(outputBoundary output_boundaries.PublicTimeline, accountRepository repositories.Account, oAccountQueryService query_services.OAccount, statusRepository repositories.Status) *interactors.PublicTimeline {
	return interactors.NewPublicTimeline(outputBoundary, accountRepository, oAccountQueryService, statusRepository)
}

func newPublicTimelineController(inputBoundary input_boundaries.PublicTimeline) *controllers.PublicTimeline {
	return controllers.NewPublicTimeline(inputBoundary)
}

func newAccountPresenter() *presenters.Account {
	return presenters.NewAccount()
}

func newAccountInteractor(outputBoundary output_boundaries.Account, accountRepository repositories.Account, oAccountQueryService query_services.OAccount) *interactors.Account {
	return interactors.NewAccount(outputBoundary, accountRepository, oAccountQueryService)
}

func newAccountController(inputBoundary input_boundaries.Account) *controllers.Account {
	return controllers.NewAccount(inputBoundary)
}

func newStatusPresenter() *presenters.Status {
	return presenters.NewStatus()
}

func newStatusInteractor(outputBoundary output_boundaries.Status, statusRepository repositories.Status, oAccountQueryService query_services.OAccount, accountRepository repositories.Account) *interactors.Status {
	return interactors.NewStatus(outputBoundary, statusRepository, oAccountQueryService, accountRepository)
}

func newStatusController(inputBoundary input_boundaries.Status) *controllers.Status {
	return controllers.NewStatus(inputBoundary)
}

func newFavouriteStatusPresenter() *presenters.FavouriteStatus {
	return presenters.NewFavouriteStatus()
}

func newFavouriteStatusInteractor(outputBoundary output_boundaries.FavouriteStatus, statusRepository repositories.Status, oStatusQueryService query_services.OStatus, oAccountQueryService query_services.OAccount, accountRepository repositories.Account) *interactors.FavouriteStatus {
	return interactors.NewFavouriteStatus(outputBoundary, statusRepository, oStatusQueryService, oAccountQueryService, accountRepository)
}

func newFavouriteStatusController(inputBoundary input_boundaries.FavouriteStatus) *controllers.FavouriteStatus {
	return controllers.NewFavouriteStatus(inputBoundary)
}

func newUnfavouriteStatusPresenter() *presenters.UnfavouriteStatus {
	return presenters.NewUnfavouriteStatus()
}

func newUnfavouriteStatusInteractor(outputBoundary output_boundaries.UnfavouriteStatus, statusRepository repositories.Status, oStatusQueryService query_services.OStatus, oAccountQueryService query_services.OAccount, accountRepository repositories.Account) *interactors.UnfavouriteStatus {
	return interactors.NewUnfavouriteStatus(outputBoundary, statusRepository, oStatusQueryService, oAccountQueryService, accountRepository)
}

func newUnfavouriteStatusController(inputBoundary input_boundaries.UnfavouriteStatus) *controllers.UnfavouriteStatus {
	return controllers.NewUnfavouriteStatus(inputBoundary)
}

func newStreamingPresenter() *presenters.Streaming {
	return presenters.NewStreaming()
}

func newStreamingInteractor(outputBoundary output_boundaries.Streaming, accountRepository repositories.Account, oAccountQueryService query_services.OAccount) *interactors.Streaming {
	return interactors.NewStreaming(outputBoundary, accountRepository, oAccountQueryService)
}

func newStreamingController(inputBoundary input_boundaries.Streaming) *controllers.Streaming {
	return controllers.NewStreaming(inputBoundary)
}

func newInstanceRestAPI(mastodonHost string) *rest_api.Instance {
	return rest_api.NewInstance(mastodonHost)
}

func newInstancePresenter() *presenters.Instance {
	return presenters.NewInstance()
}

func newInstanceInteractor(outputBoundary output_boundaries.Instance, instanceRepository repositories.Instance, domain string) *interactors.Instance {
	return interactors.NewInstance(outputBoundary, instanceRepository, domain)
}

func newInstanceController(inputBoundary input_boundaries.Instance) *controllers.Instance {
	return controllers.NewInstance(inputBoundary)
}

func newEmptyListPresenter() *presenters.EmptyList {
	return presenters.NewEmptyList()
}

func newEmptyListInteractor(outputBoundary output_boundaries.EmptyList, accountRepository repositories.Account, oAccountQueryService query_services.OAccount) *interactors.EmptyList {
	return interactors.NewEmptyList(outputBoundary, accountRepository, oAccountQueryService)
}

func newEmptyListController(inputBoundary input_boundaries.EmptyList) *controllers.EmptyList {
	return controllers.NewEmptyList(inputBoundary)
}

func newRouter(topController *controllers.Top, registerController *controllers.Register, authorizedController *controllers.Authorized, oauthAuthorizeController *controllers.OAuthAuthorize, intermediationController *controllers.Intermediation, oAuthTokenController *controllers.OAuthToken, highlightTimelineController *controllers.HighlightTimeline, publicTimelineController *controllers.PublicTimeline, accountController *controllers.Account, statusController *controllers.Status, favouriteStatusController *controllers.FavouriteStatus, unfavouriteStatusController *controllers.UnfavouriteStatus, streamingController *controllers.Streaming, instanceController *controllers.Instance, emptyListController *controllers.EmptyList) *router.Router {
	return router.New(topController, registerController, authorizedController, oauthAuthorizeController, intermediationController, oAuthTokenController, highlightTimelineController, publicTimelineController, accountController, statusController, favouriteStatusController, unfavouriteStatusController, streamingController, instanceController, emptyListController)
}

func newOHomeTimelineRestAPI(mastodonHost string) *rest_api.OHomeTimeline {
	return rest_api.NewOHomeTimeline(mastodonHost)
}

func newOStatusRestAPI(mastodonHost string) *rest_api.OStatus {
	return rest_api.NewOStatus(mastodonHost)
}

func newFetchHighlightTimelinesPresenter() *presenters.FetchHighlightTimelines {
	return presenters.NewFetchHighlightTimelines()
}

func newFetchHighlightTimelinesInteractor(outputBoundary output_boundaries.FetchHighlightTimelines, accountRepository repositories.Account, oHomeTimelineQueryService query_services.OHomeTimeline, statusRepository repositories.Status, highlightScoreThreshold int32) *interactors.FetchHighlightTimelines {
	return interactors.NewFetchHighlightTimelines(outputBoundary, accountRepository, oHomeTimelineQueryService, statusRepository, highlightScoreThreshold)
}

func newFetchHighlightTimelinesController(inputBoundary input_boundaries.FetchHighlightTimelines) *controllers.FetchHighlightTimelines {
	return controllers.NewFetchHighlightTimelines(inputBoundary)
}

func newRemoveOldStatusesPresenter() *presenters.RemoveOldStatuses {
	return presenters.NewRemoveOldStatuses()
}

func newRemoveOldStatusesInteractor(outputBoundary output_boundaries.RemoveOldStatuses, statusRepository repositories.Status, days int) *interactors.RemoveOldStatuses {
	return interactors.NewRemoveOldStatuses(outputBoundary, statusRepository, days)
}

func newRemoveOldStatusesController(inputBoundary input_boundaries.RemoveOldStatuses) *controllers.RemoveOldStatuses {
	return controllers.NewRemoveOldStatuses(inputBoundary)
}

func listenAndServe(srv *http.Server) error {
	if err := srv.ListenAndServe(); err != http.ErrServerClosed {
		return fmt.Errorf("Server closed with error: %w", err)
	}
	return nil
}

func shutdownServer(srv *http.Server) error {
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		return fmt.Errorf("Failed to gracefully shutdown: %w", err)
	}
	return nil
}

func runTicker(ctx context.Context, d time.Duration, f func(context.Context)) {
	t := time.NewTicker(d)
	defer t.Stop()

	select {
	case <-ctx.Done():
		return
	default:
		f(ctx)
	}

	for {
		select {
		case <-ctx.Done():
			return
		case <-t.C:
			f(ctx)
		}
	}
}
