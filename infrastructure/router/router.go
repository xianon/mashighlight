package router

import (
	"net/http"
	"strconv"

	"gitlab.com/xianon/mashighlight/interface/controllers"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

type Router struct {
	r chi.Router
}

func New(topController *controllers.Top, registerController *controllers.Register, authorizedController *controllers.Authorized, oauthAuthorizeController *controllers.OAuthAuthorize, intermediationController *controllers.Intermediation, oAuthTokenController *controllers.OAuthToken, highlightTimelineController *controllers.HighlightTimeline, publicTimelineController *controllers.PublicTimeline, accountController *controllers.Account, statusController *controllers.Status, favouriteStatusController *controllers.FavouriteStatus, unfavouriteStatusController *controllers.UnfavouriteStatus, streamingController *controllers.Streaming, instanceController *controllers.Instance, emptyListController *controllers.EmptyList) *Router {
	rt := &Router{
		r: chi.NewRouter(),
	}

	rt.r.Use(middleware.Logger)
	rt.r.Use(middleware.StripSlashes)
	rt.r.Use(middleware.GetHead)

	rt.r.Get("/", topController.Index)
	rt.r.Get("/register", registerController.Index)
	rt.r.Get("/authorized", authorizedController.Index)

	rt.r.Route("/oauth", func(r chi.Router) {
		r.Get("/authorize", oauthAuthorizeController.Read)
		r.Post("/revoke", intermediationController.Create)
		r.Post("/token", oAuthTokenController.Create)
	})

	rt.r.Route("/api", func(r chi.Router) {
		r.Get("/v1/accounts/verify_credentials", intermediationController.Read)
		r.Get("/v1/accounts/{id:\\d+}", func(w http.ResponseWriter, r *http.Request) {
			id, _ := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
			ctx := accountController.SetID(r.Context(), id)
			accountController.Read(w, r.WithContext(ctx))
		})
		r.Post("/v1/apps", intermediationController.Create)
		r.Get("/v1/apps/verify_credentials", intermediationController.Read)
		r.Get("/v1/custom_emojis", intermediationController.Read)
		r.Get("/v1/filters", intermediationController.Read)
		r.Get("/v1/instance", instanceController.Read)
		r.Get("/v1/notifications", emptyListController.AuthenticateRead)
		r.Get("/v1/preferences", intermediationController.Read)
		r.Get("/v1/statuses/{id:\\d+}", func(w http.ResponseWriter, r *http.Request) {
			id, _ := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
			ctx := statusController.SetID(r.Context(), id)
			statusController.Read(w, r.WithContext(ctx))
		})
		r.Post("/v1/statuses/{id:\\d+}/favourite", func(w http.ResponseWriter, r *http.Request) {
			id, _ := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
			ctx := favouriteStatusController.SetID(r.Context(), id)
			favouriteStatusController.Create(w, r.WithContext(ctx))
		})
		r.Post("/v1/statuses/{id:\\d+}/unfavourite", func(w http.ResponseWriter, r *http.Request) {
			id, _ := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
			ctx := unfavouriteStatusController.SetID(r.Context(), id)
			unfavouriteStatusController.Create(w, r.WithContext(ctx))
		})
		r.Get("/v1/streaming", streamingController.Read)
		r.Get("/v1/timelines/home", highlightTimelineController.Read)
		r.Get("/v1/timelines/public", publicTimelineController.Read)

		/*
			r.Get("/*", intermediationController.Read)
			r.Post("/*", intermediationController.Create)
		*/
	})

	rt.r.Route("/", func(r chi.Router) {
		// TODO: Tootle が転送を無視するため intermediationController にしているがそもそも必要？
		//r.Get("/favicon.ico", intermediationController.Read)

		/*
			r.Get("/*", redirectController.Read)
		*/
	})

	return rt
}

func (rt *Router) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	rt.r.ServeHTTP(w, r)
}
