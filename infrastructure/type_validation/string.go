package type_validation

import (
	"fmt"
	"reflect"

	validation "github.com/go-ozzo/ozzo-validation/v4"
)

var ErrStringType = validation.NewError("validation_is_string_type", "must be a string type")

var String = StringTypeRule{err: ErrStringType}

type StringTypeRule struct {
	err validation.Error
}

func (r StringTypeRule) Error(message string) StringTypeRule {
	r.err = r.err.SetMessage(message)
	return r
}

func (r StringTypeRule) ErrorObject(err validation.Error) StringTypeRule {
	r.err = err
	return r
}

func (r StringTypeRule) Validate(value interface{}) error {
	value, isNil := validation.Indirect(value)
	if isNil {
		return nil
	}

	rv := reflect.ValueOf(value)
	if rv.Kind() == reflect.String {
		return nil
	}
	return fmt.Errorf("type not supported: %v", rv.Type())
}
