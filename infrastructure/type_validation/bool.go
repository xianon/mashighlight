package type_validation

import (
	"fmt"
	"reflect"

	validation "github.com/go-ozzo/ozzo-validation/v4"
)

var ErrBoolType = validation.NewError("validation_is_bool_type", "must be a bool type")

var Bool = BoolTypeRule{err: ErrBoolType}

type BoolTypeRule struct {
	err validation.Error
}

func (r BoolTypeRule) Error(message string) BoolTypeRule {
	r.err = r.err.SetMessage(message)
	return r
}

func (r BoolTypeRule) ErrorObject(err validation.Error) BoolTypeRule {
	r.err = err
	return r
}

func (r BoolTypeRule) Validate(value interface{}) error {
	value, isNil := validation.Indirect(value)
	if isNil {
		return nil
	}

	rv := reflect.ValueOf(value)
	switch rv.Kind() {
	case reflect.Bool:
		return nil
	default:
		return fmt.Errorf("type not supported: %v", rv.Type())
	}
}
