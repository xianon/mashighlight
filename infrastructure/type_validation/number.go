package type_validation

import (
	"fmt"
	"reflect"

	validation "github.com/go-ozzo/ozzo-validation/v4"
)

var ErrNumberType = validation.NewError("validation_is_number_type", "must be a number type")

var Number = NumberTypeRule{err: ErrNumberType}

type NumberTypeRule struct {
	err validation.Error
}

func (r NumberTypeRule) Error(message string) NumberTypeRule {
	r.err = r.err.SetMessage(message)
	return r
}

func (r NumberTypeRule) ErrorObject(err validation.Error) NumberTypeRule {
	r.err = err
	return r
}

func (r NumberTypeRule) Validate(value interface{}) error {
	value, isNil := validation.Indirect(value)
	if isNil {
		return nil
	}

	rv := reflect.ValueOf(value)
	switch rv.Kind() {
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return nil
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr:
		return nil
	case reflect.Float32, reflect.Float64:
		return nil
	default:
		return fmt.Errorf("type not supported: %v", rv.Type())
	}
}
