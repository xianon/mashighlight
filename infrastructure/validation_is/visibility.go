package validation_is

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/xianon/mashighlight/domain/valueobject"
)

var (
	Visibility = validation.NewStringRule(isVisibility, "must be a valid visibility")
)

func isVisibility(str string) bool {
	if _, err := valueobject.VisibilityFrom(str); err != nil {
		return false
	}
	return true
}
