package database

import (
	"context"
	"sort"
	"time"

	"gitlab.com/xianon/mashighlight/domain/entities"
	"gitlab.com/xianon/mashighlight/domain/repositories"
	"gitlab.com/xianon/mashighlight/domain/valueobject"
	"gitlab.com/xianon/mashighlight/ent"
	"gitlab.com/xianon/mashighlight/ent/status"
)

type Status struct {
	client *ent.Client
}

func NewStatus(client *ent.Client) *Status {
	return &Status{
		client: client,
	}
}

func (s *Status) Create(ctx context.Context, ownerID int64, originalID int64, createdAt time.Time, local bool, visibility valueobject.Visibility, reblogsCount int32, favouritesCount int32, repliesCount int32, inReplyToOriginalID *int64, hasAttachedMedia bool, other map[string]interface{}) (*entities.Status, error) {
	statusCreate := s.client.Status.Create().
		SetOwnerID(ownerID).
		SetOriginalID(originalID).
		SetCreatedAt(createdAt).
		SetLocal(local).
		SetVisibility(visibility.String()).
		SetReblogsCount(reblogsCount).
		SetFavouritesCount(favouritesCount).
		SetRepliesCount(repliesCount).
		SetNillableInReplyToOriginalID(inReplyToOriginalID).
		SetHasAttachedMedia(hasAttachedMedia).
		SetOther(other)
	eStatus, err := statusCreate.Save(ctx)
	if err != nil {
		return nil, err
	}

	return s.convert(eStatus)
}

func (s *Status) Delete(ctx context.Context, id int64) error {
	return s.client.Status.DeleteOneID(id).
		Exec(ctx)
}

func (s *Status) DeleteOlder(ctx context.Context, days int) error {
	_, err := s.client.Status.Delete().
		Where(status.CreatedAtLT(time.Now().AddDate(0, 0, -days))).
		Exec(ctx)
	return err
}

func (s *Status) Find(ctx context.Context, id int64) (*entities.Status, error) {
	eStatus, err := s.client.Status.Query().
		Where(status.IDEQ(id)).
		First(ctx)
	if ent.IsNotFound(err) {
		return nil, nil
	} else if err != nil {
		return nil, err
	}

	return s.convert(eStatus)
}

func (s *Status) FindHighlightTimeline(ctx context.Context, ownerID int64, local bool, maxID *int64, sinceID *int64, minID *int64, limit *int) ([]*entities.Status, error) {
	eStatusQuery := s.client.Status.Query().
		Where(status.OwnerIDEQ(ownerID))

	if local {
		eStatusQuery = eStatusQuery.
			Where(status.LocalEQ(true))
	}
	if maxID != nil {
		eStatusQuery = eStatusQuery.
			Where(status.IDLT(*maxID))
	}
	if minID != nil {
		eStatusQuery = eStatusQuery.
			Where(status.IDGT(*minID))
	} else if sinceID != nil {
		eStatusQuery = eStatusQuery.
			Where(status.IDGT(*sinceID))
	}

	var lmt int
	if limit != nil {
		lmt = *limit
	} else {
		lmt = repositories.DefaultTimelineLimit
	}
	if minID != nil {
		eStatusQuery = eStatusQuery.
			Order(ent.Asc(status.FieldID)).
			Limit(lmt)
	} else {
		eStatusQuery = eStatusQuery.
			Order(ent.Desc(status.FieldID)).
			Limit(lmt)
	}

	eStatuses, err := eStatusQuery.All(ctx)
	if ent.IsNotFound(err) {
		return make([]*entities.Status, 0), nil
	} else if err != nil {
		return nil, err
	}

	statuses := make([]*entities.Status, len(eStatuses))
	for i, eStatus := range eStatuses {
		status, err := s.convert(eStatus)
		if err != nil {
			return nil, err
		}
		statuses[i] = status
	}
	sort.Slice(statuses, func(i, j int) bool {
		return statuses[i].ID > statuses[j].ID
	})
	return statuses, nil
}

func (s *Status) FindPublicTimeline(ctx context.Context, ownerID int64, local bool, remote bool, onlyMedia bool, maxID *int64, sinceID *int64, minID *int64, limit *int) ([]*entities.Status, error) {
	if local && remote {
		return make([]*entities.Status, 0), nil
	}

	eStatusQuery := s.client.Status.Query().
		Where(status.OwnerIDEQ(ownerID))

	if local {
		eStatusQuery = eStatusQuery.
			Where(status.LocalEQ(true))
	} else if remote {
		eStatusQuery = eStatusQuery.
			Where(status.LocalEQ(false))
	}
	if onlyMedia {
		eStatusQuery = eStatusQuery.
			Where(status.HasAttachedMediaEQ(true))
	}
	if maxID != nil {
		eStatusQuery = eStatusQuery.
			Where(status.IDLT(*maxID))
	}
	if minID != nil {
		eStatusQuery = eStatusQuery.
			Where(status.IDGT(*minID))
	} else if sinceID != nil {
		eStatusQuery = eStatusQuery.
			Where(status.IDGT(*sinceID))
	}

	var lmt int
	if limit != nil {
		lmt = *limit
	} else {
		lmt = repositories.DefaultTimelineLimit
	}
	if minID != nil {
		eStatusQuery = eStatusQuery.
			Order(ent.Asc(status.FieldID)).
			Limit(lmt)
	} else {
		eStatusQuery = eStatusQuery.
			Order(ent.Desc(status.FieldID)).
			Limit(lmt)
	}

	eStatuses, err := eStatusQuery.All(ctx)
	if ent.IsNotFound(err) {
		return make([]*entities.Status, 0), nil
	} else if err != nil {
		return nil, err
	}

	statuses := make([]*entities.Status, len(eStatuses))
	for i, eStatus := range eStatuses {
		status, err := s.convert(eStatus)
		if err != nil {
			return nil, err
		}
		statuses[i] = status
	}
	sort.Slice(statuses, func(i, j int) bool {
		return statuses[i].ID > statuses[j].ID
	})
	return statuses, nil
}

func (s *Status) MergeByOriginalID(ctx context.Context, ownerID int64, originalID int64, createdAt time.Time, local bool, visibility valueobject.Visibility, reblogsCount int32, favouritesCount int32, repliesCount int32, inReplyToOriginalID *int64, hasAttachedMedia bool, other map[string]interface{}) (*entities.Status, error) {
	statusID, err := s.client.Status.Create().
		SetOwnerID(ownerID).
		SetOriginalID(originalID).
		SetCreatedAt(createdAt).
		SetLocal(local).
		SetVisibility(visibility.String()).
		SetReblogsCount(reblogsCount).
		SetFavouritesCount(favouritesCount).
		SetRepliesCount(repliesCount).
		SetNillableInReplyToOriginalID(inReplyToOriginalID).
		SetHasAttachedMedia(hasAttachedMedia).
		SetOther(other).
		OnConflictColumns(status.FieldOwnerID, status.FieldOriginalID).
		UpdateReblogsCount().
		UpdateFavouritesCount().
		UpdateRepliesCount().
		UpdateOther().
		ID(ctx)
	if err != nil {
		return nil, err
	}

	eStatus, err := s.client.Status.Get(ctx, statusID)
	if err != nil {
		return nil, err
	}

	return s.convert(eStatus)
}

func (s *Status) Update(ctx context.Context, id int64, reblogsCount int32, favouritesCount int32, repliesCount int32, other map[string]interface{}) (*entities.Status, error) {
	eStatus, err := s.client.Status.UpdateOneID(id).
		SetReblogsCount(reblogsCount).
		SetFavouritesCount(favouritesCount).
		SetRepliesCount(repliesCount).
		Save(ctx)
	if err != nil {
		return nil, err
	}

	return s.convert(eStatus)
}

func (s *Status) convert(eStatus *ent.Status) (*entities.Status, error) {
	visibility, err := valueobject.VisibilityFrom(eStatus.Visibility)
	if err != nil {
		return nil, err
	}

	other := make(map[string]interface{})
	for k, v := range eStatus.Other {
		other[k] = v
	}
	status := entities.NewStatus(eStatus.ID, eStatus.OwnerID, eStatus.OriginalID, eStatus.CreatedAt, eStatus.HighlightedAt, visibility, eStatus.ReblogsCount, eStatus.FavouritesCount, eStatus.RepliesCount, eStatus.InReplyToOriginalID, eStatus.HasAttachedMedia, other)
	return status, nil
}
