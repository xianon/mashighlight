package database

import (
	"context"

	"gitlab.com/xianon/mashighlight/domain/entities"
	"gitlab.com/xianon/mashighlight/ent"
	"gitlab.com/xianon/mashighlight/ent/application"
)

type Application struct {
	client *ent.Client
}

func NewApplication(client *ent.Client) *Application {
	return &Application{
		client: client,
	}
}

func (a *Application) Find(ctx context.Context) (*entities.Application, error) {
	eApplication, err := a.client.Application.Query().
		Order(ent.Desc(application.FieldID)).
		First(ctx)
	if ent.IsNotFound(err) {
		return nil, nil
	} else if err != nil {
		return nil, err
	}

	application := a.convert(eApplication)
	return application, nil
}

func (a *Application) Merge(ctx context.Context, name string, website *string, vapidKey *string, clientID string, clientSecret string, other map[string]interface{}) (*entities.Application, error) {
	applicationID, err := a.client.Application.Create().
		SetName(name).
		SetNillableWebsite(website).
		SetNillableVapidKey(vapidKey).
		SetClientID(clientID).
		SetClientSecret(clientSecret).
		SetOther(other).
		OnConflictColumns(application.FieldClientID).
		UpdateNewValues().
		ID(ctx)
	if err != nil {
		return nil, err
	}

	eApplication, err := a.client.Application.Get(ctx, applicationID)
	if err != nil {
		return nil, err
	}

	application := a.convert(eApplication)
	return application, nil
}

func (a *Application) convert(eApplication *ent.Application) *entities.Application {
	return entities.NewApplication(eApplication.Name, eApplication.Website, eApplication.VapidKey, eApplication.ClientID, eApplication.ClientSecret, eApplication.Other)
}
