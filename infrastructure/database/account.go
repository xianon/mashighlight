package database

import (
	"context"

	"gitlab.com/xianon/mashighlight/domain/entities"
	"gitlab.com/xianon/mashighlight/ent"
	"gitlab.com/xianon/mashighlight/ent/account"
)

type Account struct {
	client *ent.Client
}

func NewAccount(client *ent.Client) *Account {
	return &Account{
		client: client,
	}
}

func (a *Account) Delete(ctx context.Context, id int64) error {
	return a.client.Account.DeleteOneID(id).
		Exec(ctx)
}

func (a *Account) FindAll(ctx context.Context) ([]*entities.Account, error) {
	eAccounts, err := a.client.Account.Query().
		Order(ent.Asc(account.FieldID)).
		All(ctx)
	if ent.IsNotFound(err) {
		return make([]*entities.Account, 0), nil
	} else if err != nil {
		return nil, err
	}

	accounts := make([]*entities.Account, len(eAccounts))
	for i, eAccount := range eAccounts {
		accounts[i] = a.convert(eAccount)
	}
	return accounts, nil
}

func (a *Account) Merge(ctx context.Context, id int64, token *string) (*entities.Account, error) {
	accountID, err := a.client.Account.Create().
		SetID(id).
		SetNillableToken(token).
		OnConflictColumns(account.FieldID).
		UpdateNewValues().
		ID(ctx)
	if err != nil {
		return nil, err
	}

	eAccount, err := a.client.Account.Get(ctx, accountID)
	if err != nil {
		return nil, err
	}

	account := a.convert(eAccount)
	return account, nil
}

func (a *Account) UpdateLastAccessedAt(ctx context.Context, id int64) (*entities.Account, error) {
	eAccount, err := a.client.Account.UpdateOneID(id).Save(ctx)
	if err != nil {
		return nil, err
	}

	account := a.convert(eAccount)
	return account, nil
}

func (a *Account) convert(eAccount *ent.Account) *entities.Account {
	return entities.NewAccount(eAccount.ID, eAccount.Token, eAccount.LastAccessedAt)
}
