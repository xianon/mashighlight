package rest_api

import (
	"bytes"
	"context"
	"errors"
	"io"
	"log"
	"net/url"
	"regexp"
	"strings"

	"gitlab.com/xianon/mashighlight/domain/entities"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/clientcredentials"
)

type AccessToken struct {
	mastodonHost string
}

func NewAccessToken(mastodonHost string) *AccessToken {
	return &AccessToken{
		mastodonHost: mastodonHost,
	}
}

func (a *AccessToken) Obtain(ctx context.Context, grantType string, clientID string, clientSecret string, redirectURI *string, scopes []string, code *string) (*entities.AccessToken, error) {
	apiURL := &url.URL{
		Scheme: "https",
		Host:   a.mastodonHost,
		Path:   "/oauth/token",
	}

	var token *oauth2.Token
	var err error
	if grantType == "client_credentials" {
		conf := clientcredentials.Config{
			ClientID:     clientID,
			ClientSecret: clientSecret,
			TokenURL:     apiURL.String(),
			Scopes:       scopes,
			AuthStyle:    oauth2.AuthStyleInParams,
		}
		token, err = conf.Token(ctx)
	} else {
		conf := oauth2.Config{
			ClientID:     clientID,
			ClientSecret: clientSecret,
			Endpoint: oauth2.Endpoint{
				TokenURL:  apiURL.String(),
				AuthStyle: oauth2.AuthStyleInParams,
			},
		}
		if redirectURI != nil {
			conf.RedirectURL = *redirectURI
		}
		authCodeOptions := []oauth2.AuthCodeOption{
			oauth2.SetAuthURLParam("grant_type", grantType),
		}
		if len(scopes) > 0 {
			authCodeOptions = append(authCodeOptions, oauth2.SetAuthURLParam("scope", strings.Join(scopes, " ")))
		}
		var c string
		if code != nil {
			c = *code
		}
		token, err = conf.Exchange(ctx, c, authCodeOptions...)
	}
	log.Printf("%+v", token)

	if err != nil {
		var retrieveErr *oauth2.RetrieveError
		if errors.As(err, &retrieveErr) {
			resp := *retrieveErr.Response
			resp.Body = io.NopCloser(bytes.NewBuffer(retrieveErr.Body))
			err = getErrorWithDescription(&resp)
		}
		return nil, err
	}

	var tokenType *string
	if token.TokenType != "" {
		tokenType = &token.TokenType
	}
	var expiresIn *int
	expiresInValue := token.Extra("expires_in")
	if expiresInValue != nil {
		if v, ok := toInt(expiresInValue); ok {
			expiresIn = &v
		}
	}
	var refreshToken *string
	if token.RefreshToken != "" {
		refreshToken = &token.RefreshToken
	}
	tokenScopes := make([]string, 0)
	if scope, ok := toString(token.Extra("scope")); ok {
		tokenScopes = regexp.MustCompile(" +").Split(scope, -1)
	} else {
		tokenScopes = make([]string, 0)
	}
	accessToken := entities.NewAccessToken(token.AccessToken, tokenType, expiresIn, refreshToken, tokenScopes)

	return accessToken, nil
}
