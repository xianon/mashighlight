package rest_api

import (
	"context"
	"encoding/json"
	"net/http"
	"net/url"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/xianon/mashighlight/domain/entities"
	"gitlab.com/xianon/mashighlight/infrastructure/type_validation"
)

type Instance struct {
	mastodonHost string
}

func NewInstance(mastodonHost string) *Instance {
	return &Instance{
		mastodonHost: mastodonHost,
	}
}

func convertInstance(mInstance map[string]interface{}) (*entities.Instance, error) {
	err := validation.Validate(mInstance,
		validation.Map(
			validation.Key("uri",
				type_validation.String,
				validation.Required,
			),
			validation.Key("title",
				type_validation.String,
				validation.Required,
			),
			validation.Key("urls",
				validation.Required,
				validation.Map(
					validation.Key("streaming_api",
						type_validation.String,
						validation.Required,
					),
				).AllowExtraKeys(),
			),
			validation.Key("registrations",
				type_validation.Bool,
				validation.Required,
			),
		).AllowExtraKeys(),
	)

	var errs validation.Errors
	if err == nil {
		errs = validation.Errors{}
	} else if e, ok := err.(validation.Errors); ok {
		errs = e
	} else {
		return nil, err
	}

	instance := &entities.Instance{
		Other: make(map[string]interface{}),
	}
	for k, v := range mInstance {
		switch k {
		case "uri":
			instance.URI, _ = toString(v)
		case "title":
			instance.Title, _ = toString(v)
		case "urls":
			mURLs, _ := v.(map[string]interface{})
			instance.URLs.StreamingAPI, _ = toString(mURLs["streaming_api"])
		case "registrations":
			instance.Registrations, _ = toBool(v)
		default:
			instance.Other[k] = v
		}
	}

	if len(errs) > 0 {
		return nil, errs
	}
	return instance, nil
}

func (i *Instance) Fetch(ctx context.Context) (*entities.Instance, error) {
	apiURL := &url.URL{
		Scheme: "https",
		Host:   i.mastodonHost,
		Path:   "/api/v1/instance",
	}

	req, err := http.NewRequestWithContext(ctx, "GET", apiURL.String(), nil)
	if err != nil {
		return nil, err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		err = getErrorWithDescription(resp)
		return nil, err
	}

	var mInstance map[string]interface{}
	if err := json.NewDecoder(resp.Body).Decode(&mInstance); err != nil {
		return nil, err
	}

	instance, err := convertInstance(mInstance)
	if err != nil {
		return nil, err
	}

	return instance, nil
}
