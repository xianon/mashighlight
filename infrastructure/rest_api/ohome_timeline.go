package rest_api

import (
	"context"
	"encoding/json"
	"net/http"
	"net/url"
	"strconv"

	"github.com/peterhellberg/link"
	"gitlab.com/xianon/mashighlight/usecase/dto"
)

type OHomeTimeline struct {
	mastodonHost string
}

func NewOHomeTimeline(mastodonHost string) *OHomeTimeline {
	return &OHomeTimeline{
		mastodonHost: mastodonHost,
	}
}

func (h *OHomeTimeline) FindAll(ctx context.Context, token string, maxID *string, sinceID *string, minID *string, limit *int, local *bool) ([]*dto.OStatus, error) {
	apiURL := &url.URL{
		Scheme: "https",
		Host:   h.mastodonHost,
		Path:   "/api/v1/timelines/home",
	}

	query := apiURL.Query()
	if maxID != nil {
		query.Set("max_id", *maxID)
	}
	if sinceID != nil {
		query.Set("since_id", *sinceID)
	}
	if minID != nil {
		query.Set("min_id", *minID)
	}
	if local != nil {
		query.Set("local", strconv.FormatBool(*local))
	}
	apiURL.RawQuery = query.Encode()

	var oStatuses []*dto.OStatus
	if limit != nil {
		oStatuses = make([]*dto.OStatus, 0, *limit)
	} else {
		oStatuses = make([]*dto.OStatus, 0)
	}
	for limit == nil || len(oStatuses) < *limit {
		if limit != nil {
			query := apiURL.Query()
			query.Set("limit", strconv.Itoa(*limit-len(oStatuses)))
			apiURL.RawQuery = query.Encode()
		}

		req, err := http.NewRequestWithContext(ctx, "GET", apiURL.String(), nil)
		if err != nil {
			return nil, err
		}
		req.Header.Set("Authorization", "Bearer "+token)

		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			return nil, err
		}
		defer resp.Body.Close()

		if resp.StatusCode != http.StatusOK {
			err = getErrorWithDescription(resp)
			return nil, err
		}

		var mStatuses []map[string]interface{}
		if err := json.NewDecoder(resp.Body).Decode(&mStatuses); err != nil {
			return nil, err
		}
		if len(mStatuses) == 0 {
			break
		}

		for _, mStatus := range mStatuses {
			oStatus, err := convertOStatus(mStatus)
			if err != nil {
				return nil, err
			}
			oStatuses = append(oStatuses, oStatus)
		}

		if limit == nil {
			break
		}

		linkGroup := link.ParseResponse(resp)
		var linkGroupName string
		if minID != nil {
			linkGroupName = "prev"
		} else {
			linkGroupName = "next"
		}
		link, ok := linkGroup[linkGroupName]
		if !ok {
			break
		}

		apiURL, err = url.Parse(link.URI)
		if err != nil {
			return nil, err
		}
	}
	return oStatuses, nil
}
