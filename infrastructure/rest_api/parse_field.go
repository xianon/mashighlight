package rest_api

import (
	"reflect"

	validation "github.com/go-ozzo/ozzo-validation/v4"
)

func isFloat(value interface{}) bool {
	value, isNil := validation.Indirect(value)
	if isNil {
		return false
	}

	rv := reflect.ValueOf(value)
	switch rv.Kind() {
	case reflect.Float32, reflect.Float64:
		return true
	default:
		return false
	}
}

func isInt(value interface{}) bool {
	value, isNil := validation.Indirect(value)
	if isNil {
		return false
	}

	rv := reflect.ValueOf(value)
	switch rv.Kind() {
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return true
	default:
		return false
	}
}

func isUint(value interface{}) bool {
	value, isNil := validation.Indirect(value)
	if isNil {
		return false
	}

	rv := reflect.ValueOf(value)
	switch rv.Kind() {
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr:
		return true
	default:
		return false
	}
}

func toBool(value interface{}) (bool, bool) {
	value, isNil := validation.Indirect(value)
	if isNil {
		return false, false
	}

	rv := reflect.ValueOf(value)
	switch rv.Kind() {
	case reflect.Bool:
		return rv.Bool(), true
	default:
		return false, false
	}
}

func toInt(value interface{}) (int, bool) {
	value, isNil := validation.Indirect(value)
	if isNil {
		return 0, false
	}

	rv := reflect.ValueOf(value)
	switch rv.Kind() {
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return int(rv.Int()), true
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr:
		return int(rv.Uint()), true
	case reflect.Float32, reflect.Float64:
		return int(rv.Float()), true
	default:
		return 0, false
	}
}

func toInt32(value interface{}) (int32, bool) {
	value, isNil := validation.Indirect(value)
	if isNil {
		return 0, false
	}

	rv := reflect.ValueOf(value)
	switch rv.Kind() {
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return int32(rv.Int()), true
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr:
		return int32(rv.Uint()), true
	case reflect.Float32, reflect.Float64:
		return int32(rv.Float()), true
	default:
		return 0, false
	}
}

func toString(value interface{}) (string, bool) {
	value, isNil := validation.Indirect(value)
	if isNil {
		return "", false
	}

	rv := reflect.ValueOf(value)
	if rv.Kind() == reflect.String {
		return rv.String(), true
	}
	return "", false
}
