package rest_api

import (
	"context"
	"encoding/json"
	"net/http"
	"net/url"
	"strings"

	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/xianon/mashighlight/infrastructure/type_validation"
	"gitlab.com/xianon/mashighlight/usecase/dto"
)

type OApplication struct {
	mastodonHost string
}

func NewOApplication(mastodonHost string) *OApplication {
	return &OApplication{
		mastodonHost: mastodonHost,
	}
}

func convertOApplication(mApplication map[string]interface{}) (*dto.OApplication, error) {
	err := validation.Validate(mApplication,
		validation.Map(
			validation.Key("name",
				type_validation.String,
				validation.Required,
			),
			validation.Key("website",
				type_validation.String,
			).Optional(),
			validation.Key("vapid_key",
				type_validation.String,
			).Optional(),
			validation.Key("client_id",
				type_validation.String,
			).Optional(),
			validation.Key("client_secret",
				type_validation.String,
			).Optional(),
		).AllowExtraKeys(),
	)

	var errs validation.Errors
	if err == nil {
		errs = validation.Errors{}
	} else if e, ok := err.(validation.Errors); ok {
		errs = e
	} else {
		return nil, err
	}

	oApplication := &dto.OApplication{
		Other: make(map[string]interface{}),
	}
	for k, v := range mApplication {
		switch k {
		case "name":
			oApplication.Name, _ = toString(v)
		case "website":
			if website, ok := toString(v); ok {
				oApplication.Website = &website
			} else {
				oApplication.Website = nil
			}
		case "vapid_key":
			if vapidKey, ok := toString(v); ok {
				oApplication.VapidKey = &vapidKey
			} else {
				oApplication.VapidKey = nil
			}
		case "client_id":
			if clientID, ok := toString(v); ok {
				oApplication.ClientID = &clientID
			} else {
				oApplication.ClientID = nil
			}
		case "client_secret":
			if clientSecret, ok := toString(v); ok {
				oApplication.ClientSecret = &clientSecret
			} else {
				oApplication.ClientSecret = nil
			}
		default:
			oApplication.Other[k] = v
		}
	}

	if len(errs) > 0 {
		return nil, errs
	}
	return oApplication, nil
}

func (a *OApplication) Create(ctx context.Context, clientName string, redirectURIs string, scopes *string, website *string) (*dto.OApplication, error) {
	apiURL := &url.URL{
		Scheme: "https",
		Host:   a.mastodonHost,
		Path:   "/api/v1/apps",
	}

	data := url.Values{
		"client_name":   {clientName},
		"redirect_uris": {redirectURIs},
	}
	if scopes != nil {
		data.Add("scopes", *scopes)
	}
	if website != nil {
		data.Add("website", *website)
	}

	req, err := http.NewRequestWithContext(ctx, "POST", apiURL.String(), strings.NewReader(data.Encode()))
	if err != nil {
		return nil, err
	}

	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		err = getErrorWithDescription(resp)
		return nil, err
	}

	var mApplication map[string]interface{}
	if err := json.NewDecoder(resp.Body).Decode(&mApplication); err != nil {
		return nil, err
	}

	oApplication, err := convertOApplication(mApplication)
	if err != nil {
		return nil, err
	}

	return oApplication, nil
}

func (a *OApplication) VerifyCredentials(ctx context.Context, token string) (*dto.OApplication, error) {
	apiURL := &url.URL{
		Scheme: "https",
		Host:   a.mastodonHost,
		Path:   "/api/v1/apps/verify_credentials",
	}

	req, err := http.NewRequestWithContext(ctx, "GET", apiURL.String(), nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Authorization", "Bearer "+token)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		err = getErrorWithDescription(resp)
		return nil, err
	}

	var mApplication map[string]interface{}
	if err := json.NewDecoder(resp.Body).Decode(&mApplication); err != nil {
		return nil, err
	}

	oApplication, err := convertOApplication(mApplication)
	if err != nil {
		return nil, err
	}

	return oApplication, nil
}
