package rest_api

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"math"
	"net/http"
	"net/url"
	"strconv"
	"time"

	"github.com/go-ozzo/ozzo-validation/is"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/xianon/mashighlight/domain/valueobject"
	"gitlab.com/xianon/mashighlight/infrastructure/type_validation"
	"gitlab.com/xianon/mashighlight/infrastructure/validation_is"
	"gitlab.com/xianon/mashighlight/usecase/dto"
)

type OStatus struct {
	mastodonHost string
}

func NewOStatus(mastodonHost string) *OStatus {
	return &OStatus{
		mastodonHost: mastodonHost,
	}
}

func convertOStatus(mStatus map[string]interface{}) (*dto.OStatus, error) {
	err := validation.Validate(mStatus,
		validation.Map(
			validation.Key("id",
				type_validation.String,
				validation.Required,
				is.Int,
			),
			validation.Key("created_at",
				type_validation.String,
				validation.Required,
				validation.Date(time.RFC3339Nano),
			),
			validation.Key("visibility",
				type_validation.String,
				validation.Required,
				validation_is.Visibility,
			),
			validation.Key("reblogs_count",
				type_validation.Number,
				validation.NotNil,
				validation.When(isInt(mStatus["reblogs_count"]),
					validation.Min(int64(math.MinInt32)),
					validation.Max(int64(math.MaxInt32)),
				),
				validation.When(isUint(mStatus["reblogs_count"]),
					validation.Max(uint64(math.MaxInt32)),
				),
				validation.When(isFloat(mStatus["reblogs_count"]),
					validation.Min(float64(math.MinInt32)),
					validation.Max(float64(math.MaxInt32)),
				),
			),
			validation.Key("favourites_count",
				type_validation.Number,
				validation.NotNil,
				validation.When(isInt(mStatus["favourites_count"]),
					validation.Min(int64(math.MinInt32)),
					validation.Max(int64(math.MaxInt32)),
				),
				validation.When(isUint(mStatus["favourites_count"]),
					validation.Max(uint64(math.MaxInt32)),
				),
				validation.When(isFloat(mStatus["favourites_count"]),
					validation.Min(float64(math.MinInt32)),
					validation.Max(float64(math.MaxInt32)),
				),
			),
			validation.Key("replies_count",
				type_validation.Number,
				validation.NotNil,
				validation.When(isInt(mStatus["replies_count"]),
					validation.Min(int64(math.MinInt32)),
					validation.Max(int64(math.MaxInt32)),
				),
				validation.When(isUint(mStatus["replies_count"]),
					validation.Max(uint64(math.MaxInt32)),
				),
				validation.When(isFloat(mStatus["replies_count"]),
					validation.Min(float64(math.MinInt32)),
					validation.Max(float64(math.MaxInt32)),
				),
			),
			validation.Key("in_reply_to_id",
				type_validation.String,
				is.Int,
			),
		).AllowExtraKeys(),
	)

	var errs validation.Errors
	if err == nil {
		errs = validation.Errors{}
	} else if e, ok := err.(validation.Errors); ok {
		errs = e
	} else {
		return nil, err
	}

	oStatus := &dto.OStatus{
		Other: make(map[string]interface{}),
	}
	for k, v := range mStatus {
		switch k {
		case "id":
			idStr, _ := toString(v)
			oStatus.ID, _ = strconv.ParseInt(idStr, 10, 64)
		case "created_at":
			createdAtStr, _ := toString(v)
			oStatus.CreatedAt, _ = time.Parse(time.RFC3339Nano, createdAtStr)
		case "account":
			if v == nil {
				continue
			}
			mAccount, ok := v.(map[string]interface{})
			if !ok {
				return nil, validation.NewInternalError(errors.New("only a map[string]interface{} can be validated"))
			}
			oAccount, err := convertOAccount(mAccount)
			if err != nil {
				if _, ok := err.(validation.InternalError); ok {
					return nil, err
				}
				errs["account"] = err
				continue
			}
			oStatus.Account = *oAccount
		case "visibility":
			visibilityStr, _ := toString(v)
			oStatus.Visibility, _ = valueobject.VisibilityFrom(visibilityStr)
		case "application":
			if v == nil {
				continue
			}
			mApplication, ok := v.(map[string]interface{})
			if !ok {
				return nil, validation.NewInternalError(errors.New("only a map[string]interface{} can be validated"))
			}
			oApplication, err := convertOApplication(mApplication)
			if err != nil {
				if _, ok := err.(validation.InternalError); ok {
					return nil, err
				}
				errs["application"] = err
				continue
			}
			oStatus.Application = *oApplication
		case "reblogs_count":
			oStatus.ReblogsCount, _ = toInt32(v)
		case "favourites_count":
			oStatus.FavouritesCount, _ = toInt32(v)
		case "replies_count":
			oStatus.RepliesCount, _ = toInt32(v)
		case "in_reply_to_id":
			if inReplyToIDStr, ok := toString(v); ok {
				inReplyToID, _ := strconv.ParseInt(inReplyToIDStr, 10, 64)
				oStatus.InReplyToID = &inReplyToID
			} else {
				oStatus.InReplyToID = nil
			}
		case "reblog":
			if v == nil {
				continue
			}
			mReblog, ok := v.(map[string]interface{})
			if !ok {
				return nil, validation.NewInternalError(errors.New("only a map[string]interface{} can be validated"))
			}
			oStatus.Reblog, err = convertOStatus(mReblog)
			if err != nil {
				if _, ok := err.(validation.InternalError); ok {
					return nil, err
				}
				errs["reblog"] = err
			}
		default:
			oStatus.Other[k] = v
		}
	}

	if len(errs) > 0 {
		return nil, errs
	}
	return oStatus, nil
}

func (s *OStatus) Favourite(ctx context.Context, token string, id string, favourite bool) (*dto.OStatus, error) {
	apiURL := &url.URL{
		Scheme: "https",
		Host:   s.mastodonHost,
	}
	if favourite {
		apiURL.Path = fmt.Sprintf("/api/v1/statuses/%s/favourite", id)
	} else {
		apiURL.Path = fmt.Sprintf("/api/v1/statuses/%s/unfavourite", id)
	}

	req, err := http.NewRequestWithContext(ctx, "POST", apiURL.String(), nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Authorization", "Bearer "+token)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		err = getErrorWithDescription(resp)
		return nil, err
	}

	var mStatus map[string]interface{}
	if err := json.NewDecoder(resp.Body).Decode(&mStatus); err != nil {
		return nil, err
	}

	oStatus, err := convertOStatus(mStatus)
	if err != nil {
		return nil, err
	}

	return oStatus, nil
}
