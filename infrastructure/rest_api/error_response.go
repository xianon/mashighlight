package rest_api

import (
	"encoding/json"
	"io"
	"mime"
	"net/http"

	"gitlab.com/xianon/mashighlight/usecase/description_errors"
)

type errorResponse struct {
	Error            string  `json:"error"`
	ErrorDescription *string `json:"error_description,omitempty"`
}

func getContentType(header http.Header) string {
	ct := header.Get("Content-Type")
	if contentType, _, err := mime.ParseMediaType(ct); err == nil {
		return contentType
	}
	return ""
}

func getErrorResponse(header http.Header, body []byte) *errorResponse {
	if getContentType(header) != "application/json" {
		return nil
	}
	var errorResponse errorResponse
	if err := json.Unmarshal(body, &errorResponse); err != nil {
		return nil
	}
	return &errorResponse
}

func getErrorWithDescription(resp *http.Response) description_errors.ErrorWithDescription {
	var msg string
	var dsc *string
	if body, err := io.ReadAll(resp.Body); err != nil {
		msg = err.Error()
	} else {
		errorResponse := getErrorResponse(resp.Header, body)
		if errorResponse != nil {
			msg = errorResponse.Error
			dsc = errorResponse.ErrorDescription
		} else if len(body) > 0 {
			msg = string(body)
		} else {
			msg = resp.Status
		}
	}

	switch resp.StatusCode {
	case http.StatusBadRequest:
		if dsc == nil {
			return description_errors.NewBadRequestError(msg)
		}
		return description_errors.NewBadRequestErrorWithDescription(msg, *dsc)
	case http.StatusUnauthorized:
		if dsc == nil {
			return description_errors.NewUnauthenticatedError(msg)
		}
		return description_errors.NewUnauthenticatedErrorWithDescription(msg, *dsc)
	case http.StatusNotFound:
		if dsc == nil {
			return description_errors.NewNotFoundError(msg)
		}
		return description_errors.NewNotFoundErrorWithDescription(msg, *dsc)
	default:
		if dsc == nil {
			return description_errors.NewInteractionError(msg)
		}
		return description_errors.NewInteractionErrorWithDescription(msg, *dsc)
	}
}
