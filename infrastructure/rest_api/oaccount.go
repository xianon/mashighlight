package rest_api

import (
	"context"
	"encoding/json"
	"net/http"
	"net/url"
	"strconv"
	"time"

	"github.com/go-ozzo/ozzo-validation/is"
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/xianon/mashighlight/infrastructure/type_validation"
	"gitlab.com/xianon/mashighlight/usecase/dto"
)

type OAccount struct {
	mastodonHost string
}

func NewOAccount(mastodonHost string) *OAccount {
	return &OAccount{
		mastodonHost: mastodonHost,
	}
}

func convertOAccount(mAccount map[string]interface{}) (*dto.OAccount, error) {
	err := validation.Validate(mAccount,
		validation.Map(
			validation.Key("id",
				type_validation.String,
				validation.Required,
				is.Int,
			),
			validation.Key("username",
				type_validation.String,
				validation.Required,
			),
			validation.Key("acct",
				type_validation.String,
				validation.Required,
			),
			validation.Key("display_name",
				type_validation.String,
			),
			validation.Key("created_at",
				type_validation.String,
				validation.Required,
				validation.Date(time.RFC3339Nano),
			),
		).AllowExtraKeys(),
	)

	var errs validation.Errors
	if err == nil {
		errs = validation.Errors{}
	} else if e, ok := err.(validation.Errors); ok {
		errs = e
	} else {
		return nil, err
	}

	oAccount := &dto.OAccount{
		Other: make(map[string]interface{}),
	}
	for k, v := range mAccount {
		switch k {
		case "id":
			idStr, _ := toString(v)
			oAccount.ID, _ = strconv.ParseInt(idStr, 10, 64)
		case "username":
			oAccount.UserName, _ = toString(v)
		case "acct":
			oAccount.Acct, _ = toString(v)
		case "display_name":
			if displayName, ok := toString(v); ok {
				oAccount.DisplayName = &displayName
			} else {
				oAccount.DisplayName = nil
			}
		case "created_at":
			createdAtStr, _ := toString(v)
			oAccount.CreatedAt, _ = time.Parse(time.RFC3339Nano, createdAtStr)
		default:
			oAccount.Other[k] = v
		}
	}

	if len(errs) > 0 {
		return nil, errs
	}
	return oAccount, nil
}

func (a *OAccount) VerifyCredentials(ctx context.Context, token string) (*dto.OAccount, error) {
	apiURL := &url.URL{
		Scheme: "https",
		Host:   a.mastodonHost,
		Path:   "/api/v1/accounts/verify_credentials",
	}

	req, err := http.NewRequestWithContext(ctx, "GET", apiURL.String(), nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Authorization", "Bearer "+token)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		err = getErrorWithDescription(resp)
		return nil, err
	}

	var mAccount map[string]interface{}
	if err := json.NewDecoder(resp.Body).Decode(&mAccount); err != nil {
		return nil, err
	}

	oAccount, err := convertOAccount(mAccount)
	if err != nil {
		return nil, err
	}

	return oAccount, nil
}
