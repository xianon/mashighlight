package repositories

import (
	"context"

	"gitlab.com/xianon/mashighlight/domain/entities"
)

type AccessToken interface {
	Obtain(ctx context.Context, grantType string, clientID string, clientSecret string, redirectURI *string, scopes []string, code *string) (*entities.AccessToken, error)
}
