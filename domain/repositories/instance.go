package repositories

import (
	"context"

	"gitlab.com/xianon/mashighlight/domain/entities"
)

type Instance interface {
	Fetch(ctx context.Context) (*entities.Instance, error)
}
