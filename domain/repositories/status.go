package repositories

import (
	"context"
	"time"

	"gitlab.com/xianon/mashighlight/domain/entities"
	"gitlab.com/xianon/mashighlight/domain/valueobject"
)

const DefaultTimelineLimit = 20

type Status interface {
	Create(ctx context.Context, ownerID int64, originalID int64, createdAt time.Time, local bool, visibility valueobject.Visibility, reblogsCount int32, favouritesCount int32, repliesCount int32, inReplyToOriginalID *int64, hasAttachedMedia bool, other map[string]interface{}) (*entities.Status, error)
	Delete(ctx context.Context, id int64) error
	DeleteOlder(ctx context.Context, days int) error
	Find(ctx context.Context, id int64) (*entities.Status, error)
	FindHighlightTimeline(ctx context.Context, ownerID int64, local bool, maxID *int64, sinceID *int64, minID *int64, limit *int) ([]*entities.Status, error)
	FindPublicTimeline(ctx context.Context, ownerID int64, local bool, remote bool, onlyMedia bool, maxID *int64, sinceID *int64, minID *int64, limit *int) ([]*entities.Status, error)
	MergeByOriginalID(ctx context.Context, ownerID int64, originalID int64, createdAt time.Time, local bool, visibility valueobject.Visibility, reblogsCount int32, favouritesCount int32, repliesCount int32, inReplyToOriginalID *int64, hasAttachedMedia bool, other map[string]interface{}) (*entities.Status, error)
	Update(ctx context.Context, id int64, reblogsCount int32, favouritesCount int32, repliesCount int32, other map[string]interface{}) (*entities.Status, error)
}
