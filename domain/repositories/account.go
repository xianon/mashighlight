package repositories

import (
	"context"

	"gitlab.com/xianon/mashighlight/domain/entities"
)

type Account interface {
	Delete(ctx context.Context, id int64) error
	FindAll(ctx context.Context) ([]*entities.Account, error)
	Merge(ctx context.Context, id int64, token *string) (*entities.Account, error)
	UpdateLastAccessedAt(ctx context.Context, id int64) (*entities.Account, error)
}
