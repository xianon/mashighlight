package repositories

import (
	"context"

	"gitlab.com/xianon/mashighlight/domain/entities"
)

type Application interface {
	Find(ctx context.Context) (*entities.Application, error)
	Merge(ctx context.Context, name string, website *string, vapidKey *string, clientID string, clientSecret string, other map[string]interface{}) (*entities.Application, error)
}
