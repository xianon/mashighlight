package valueobject

import (
	"errors"
)

type Visibility struct {
	value string
}

var (
	VisibilityPublic   = Visibility{value: "public"}
	VisibilityUnlisted = Visibility{value: "unlisted"}
	VisibilityPrivate  = Visibility{value: "private"}
	VisibilityDirect   = Visibility{value: "direct"}
)

func VisibilityFrom(v string) (Visibility, error) {
	switch v {
	case VisibilityPublic.value:
		return VisibilityPublic, nil
	case VisibilityUnlisted.value:
		return VisibilityUnlisted, nil
	case VisibilityPrivate.value:
		return VisibilityPrivate, nil
	case VisibilityDirect.value:
		return VisibilityDirect, nil
	default:
		return Visibility{value: v}, errors.New("visibility invalid")
	}
}

func (v Visibility) MarshalJSON() ([]byte, error) {
	return []byte(v.value), nil
}

func (v Visibility) String() string {
	return v.value
}

func (v Visibility) UnmarshalJSON(data []byte) error {
	visibility, err := VisibilityFrom(string(data))
	if err != nil {
		return err
	}

	v.value = visibility.value
	return nil
}
