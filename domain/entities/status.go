package entities

import (
	"time"

	"gitlab.com/xianon/mashighlight/domain/valueobject"
)

type Status struct {
	ID                  int64
	OwnerID             int64
	OriginalID          int64
	CreatedAt           time.Time
	HighlightedAt       time.Time
	Visibility          valueobject.Visibility
	ReblogsCount        int32
	FavouritesCount     int32
	RepliesCount        int32
	InReplyToOriginalID *int64
	HasAttachedMedia    bool
	Other               map[string]interface{}
}

func NewStatus(id int64, ownerID int64, originalID int64, createdAt time.Time, highlightedAt time.Time, visibility valueobject.Visibility, reblogsCount int32, favouritesCount int32, repliesCount int32, inReplyToOriginalID *int64, hasAttachedMedia bool, other map[string]interface{}) *Status {
	return &Status{
		ID:                  id,
		OwnerID:             ownerID,
		OriginalID:          originalID,
		CreatedAt:           createdAt,
		HighlightedAt:       highlightedAt,
		Visibility:          visibility,
		ReblogsCount:        reblogsCount,
		FavouritesCount:     favouritesCount,
		RepliesCount:        repliesCount,
		InReplyToOriginalID: inReplyToOriginalID,
		HasAttachedMedia:    hasAttachedMedia,
		Other:               other,
	}
}
