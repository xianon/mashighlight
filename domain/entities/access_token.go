package entities

type AccessToken struct {
	AccessToken  string
	TokenType    *string
	ExpiresIn    *int
	RefreshToken *string
	Scopes       []string
}

func NewAccessToken(accessToken string, tokenType *string, expiresIn *int, refreshToken *string, scopes []string) *AccessToken {
	return &AccessToken{
		AccessToken:  accessToken,
		TokenType:    tokenType,
		ExpiresIn:    expiresIn,
		RefreshToken: refreshToken,
		Scopes:       scopes,
	}
}
