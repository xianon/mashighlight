package entities

type Application struct {
	Name         string
	Website      *string
	VapidKey     *string
	ClientID     string
	ClientSecret string
	Other        map[string]interface{}
}

func NewApplication(name string, website *string, vapidKey *string, clientID string, clientSecret string, other map[string]interface{}) *Application {
	return &Application{
		Name:         name,
		Website:      website,
		VapidKey:     vapidKey,
		ClientID:     clientID,
		ClientSecret: clientSecret,
		Other:        other,
	}
}
