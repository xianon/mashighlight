package entities

import "time"

type Account struct {
	ID             int64
	Token          *string
	LastAccessedAt time.Time
}

func NewAccount(id int64, token *string, lastAccessedAt time.Time) *Account {
	return &Account{
		ID:             id,
		Token:          token,
		LastAccessedAt: lastAccessedAt,
	}
}
