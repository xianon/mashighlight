package entities

type InstanceURLs struct {
	StreamingAPI string
}

type Instance struct {
	URI           string
	Title         string
	URLs          InstanceURLs
	Registrations bool
	Other         map[string]interface{}
}

func NewInstance(uri string, title string, streamingAPI string, registrations bool, other map[string]interface{}) *Instance {
	return &Instance{
		URI:   uri,
		Title: title,
		URLs: InstanceURLs{
			StreamingAPI: streamingAPI,
		},
		Registrations: registrations,
		Other:         other,
	}
}
